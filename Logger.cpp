/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */


#include "Logger.h"
#include "TerminalColors.h"
#include "util.h"
#include <iomanip>
#include <sstream>

Logger::Logger()
    : printLog(true), printStdout(true), verbose(false), useTerminalColors(true), contextWidth(10) {
  std::ostringstream stream;
  stream << std::string(config::LOG_FOLDER) << "/" << std::string(config::LOG_FILENAME);
  logFilePath = stream.str();
}

void Logger::operator()(const std::string &context, const std::string &message) {
  std::lock_guard<std::mutex> guard(logMutex);

  std::istringstream stream(message);
  std::string line;

  while (std::getline(stream, line)) {
    log(context, line);
  }
}

void Logger::log(const std::string &context, const std::string &message) {
  Color color{};
  stringToRgb(context, color);
  if (printStdout) {
    printLogLine(std::cout, color, context, message);
  }
  if (printLog) {
    std::ofstream logFile;
    logFile.open(logFilePath, std::ios_base::app);
    printLogLine(logFile, color, context, message);
  }
}

void Logger::setPrintStdout(bool printStdout) {
  Logger::printStdout = printStdout;
}

void Logger::setPrintLog(bool printLog) {
  Logger::printLog = printLog;
}

void Logger::setVerbose(bool verbose) {
  Logger::verbose = verbose;
}

bool Logger::getVerbose() const {
  return verbose;
}

const std::string &Logger::getLogFilePath() const {
  return logFilePath;
}

Logger &GetLogger() {
  static Logger logger;
  return logger;
}

void Logger::adjustBrightness(Color &color, int minBrightness) {
  double brightness = (0.299 * color.r + 0.587 * color.g + 0.114 * color.b) / 255.0;

  if (brightness < (minBrightness / 255.0)) {
    double factor = (minBrightness / 255.0) / brightness;
    color.r = std::min((unsigned int) (color.r * factor), 255u);
    color.g = std::min((unsigned int) (color.g * factor), 255u);
    color.b = std::min((unsigned int) (color.b * factor), 255u);
  }
}

void Logger::stringToRgb(const std::string &str, Color &color) {
  std::hash<std::string> hasher;
  auto hash = hasher(str);

  color.r = (hash & 0xFF0000) >> 16;
  color.g = (hash & 0x00FF00) >> 8;
  color.b = hash & 0x0000FF;

  adjustBrightness(color, config::LOG_COLOR_MIN_BRIGHTNESS);
}

template<typename T>
void Logger::printLogLine(T &&stream, const Color &color, const std::string &context, const std::string &message) {
  std::string logMessage = message;

  if (useTerminalColors) {
    // in case the log message contains color codes already, this avoids that any "ANSI_NO_COLOR" occurrence removes the context color afterwards
    util::stringReplace(logMessage, std::string(ANSI_NO_COLOR), beginANSIColor(color));

    stream << beginANSIColor(color);
  }

  stream << std::setw((int) contextWidth) << std::left << context
         << ";" << logMessage;
  if (useTerminalColors) {
    stream << ANSI_NO_COLOR;
  }
  stream << std::endl;
}

void Logger::setContextWidth(size_t width) {
  contextWidth = width;
}

void Logger::setUseTerminalColors(bool useColors) {
  useTerminalColors = useColors;
}
