/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once

#include <regex>
#include <string_view>

namespace config {
  // name of the file to store the workflow status in (will be created in the execution directory)
  extern const std::string_view STATUS_FILENAME;
  extern const std::string_view TREE_FILENAME;

  // output options
  extern const int JSON_INDENT;

  // logging options
  extern const std::string_view LOG_FOLDER;
  extern const std::string_view LOG_FILENAME;
  extern const std::string_view COMMAND_LOG_FILENAME;
  extern const char LOG_SEPARATOR;
  extern const int LOG_SEPARATOR_NUM_SHORT;
  extern const int LOG_SEPARATOR_NUM_LONG;
  extern const int LOG_COLOR_MIN_BRIGHTNESS;
  extern const std::string_view MAIN_LOG_CONTEXT;

  // shortcuts feature
  extern const std::string_view SHORTCUT_FILENAME;

  // interaction feature
  extern const std::string_view INTERACTION_FILENAME;
  extern const std::string_view INTERACTION_ARRAY_KEY;

  // status persistence
  extern const std::string_view VARIABLES_KEY;
  extern const std::string_view PID_KEY;

  // variable feature
  extern const std::string_view VARIABLE_REGEX;
  extern const std::string_view VARIABLE_VALUE_UNDEFINED;
  extern const int MAX_VAR_REPLACEMENTS;
  extern const std::string_view VARIABLE_JSON_DEFAULT_KEY;

  // temporary folder
  extern const std::string_view TEMPORARY_FOLDER;

  // workflow analysis
  extern const int MAX_BRANCH_RECURSION_DEPTH;
  extern const int MAX_DEPENDENCY_SCAN_ROUNDS;

  // loops
  extern const std::string_view DEFAULT_LOOP_VARIABLE;

  // data and converting
  extern const std::string_view TRUE_STRING_REPR;  // string used for bool value true internally
  extern const std::string_view FALSE_STRING_REPR; // string used for bool value false internally

  extern const std::initializer_list<const std::string_view> VALID_TRUE_STRING_REPRESENTATIONS;
  extern const std::initializer_list<const std::string_view> VALID_FALSE_STRING_REPRESENTATIONS;

  // timeouts
  extern const unsigned int CANCEL_TIMEOUT_DEFAULT;
} // namespace config
