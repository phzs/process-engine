stages:
  - analysis
  - build
  - test
  - pack
  - release

workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event' || '$CI_PIPELINE_SOURCE == "push"'

default:
  image: debian:bullseye

before_script:
  - apt-get update
  - apt-get install --yes cmake
variables:
  GIT_SUBMODULE_STRATEGY: recursive

clang-format:
  stage: analysis
  tags:
    - docker
  before_script:
    - cat /etc/debian_version
    - apt-get update
    - apt-get install --yes clang-format git
  script:
    - clang-format --version
    - clang-format -i $(find . \( -path ./lib -o -path ./cmake-build-debug \) -prune -false -o -name "*.cpp" -or -iname "*.h" -or -iname "*.hpp")
    - git diff --exit-code | tee clang-format.am
  artifacts:
    when: on_failure
    paths:
      - clang-format.am
    expire_in: 3 days

build:
  stage: build
  tags:
    - docker
  before_script:
    - apt-get update
    - apt-get install --yes g++ cmake
  script:
    - mkdir -p build/
    - cmake -S `pwd` -B `pwd`/build -Dbuild_tests=ON
    - make -C build/ -j8
  artifacts:
    paths:
      - build/process_engine
      - build/process_engine_test
    expire_in: 1 day

pack_deb:
  stage: pack
  tags:
    - docker
  before_script:
    - apt-get update
    - apt-get install --yes g++ cmake file
  script:
    - ./pack-deb.sh
  artifacts:
    name: "Process-Engine"
    paths:
      - build/process-engine-*.deb

release_deb:
  stage: release
  tags:
    - docker
  dependencies:
    - pack_deb
  script:
    - echo "Releasing .deb package"
  rules:
    - if: '$CI_COMMIT_TAG =~ /^v[0-9]+\.[0-9]+\.[0-9]$/'
  artifacts:
    name: "Process-Engine-$CI_COMMIT_TAG"
    paths:
      - build/process-engine-*.deb
    expire_in: never

unit_tests:
  stage: test
  tags:
    - docker
  script:
    - build/process_engine_test -r junit --out result.xml
  timeout: 2min
  artifacts:
    when: always
    reports:
      junit: result.xml

xmlhelp_test:
  stage: test
  tags:
    - docker
  before_script:
    - apt-get update
    - apt-get install --yes libxml2-utils
  script:
    - build/process_engine run --xmlhelp | xmllint --noout -
    - >
      build/process_engine --commands | while read command; do
        echo -n "Testing \"$command\"..."
        $command --xmlhelp | xmllint --noout -
        echo "OK"
      done
