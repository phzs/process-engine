# process engine
This is a workflow execution engine to execute kadi workflows.

## Download and install latest .deb package
```
wget https://gitlab.com/iam-cms/workflows/process-engine/-/jobs/artifacts/master/download?job=pack_deb -O process_engine.zip
unzip process_engine.zip
sudo apt install ./build/process_engine-[VERSION]_[SYSTEM ARCHITECTURE].deb
```

## How to build
```
git submodule update --init
mkdir build
cmake -B build/ -S .
cmake --build build/
```


## Usage
Running a workflow:
```
process_engine start -p /tmp/my-execution-folder my-workflow.flow
```
More info can be obtained with `process_engine --help` and `process_engine [sub-command] --help`.

## Configuration for the process manager
The API which the [process manager](https://gitlab.com/iam-cms/workflows/process-manager) expects was configurable in the past, but now the only configuration is "name" and "path" for each process engine.
Assuming the process engine is installed as `process_engine` and available via `$PATH`, the [default configuration of the process manager](https://gitlab.com/iam-cms/workflows/process-manager/-/blob/master/res/engines.json) will work.

# Development

To also build the unit tests, set the cmake variable `build_test` to `ON` when generating the cmake cache (default is `OFF`):
```
cmake .. -Dbuild_tests=ON
```
