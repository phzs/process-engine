/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "util.h"
#include "Logger.h"
#include <experimental/filesystem>
#include <iomanip>
#include <string>
#include <unistd.h>
#include <vector>
#include <wordexp.h>


namespace util {
  namespace fs = std::experimental::filesystem;
  std::string getCurrentDateTime() {
    std::ostringstream oss;
    auto tm = std::time(nullptr);
    oss << std::put_time(std::localtime(&tm), "%H:%M:%S--%d.%m.%Y");
    std::string result = oss.str();
    result.replace(result.find("--"), 2, " ");
    return result;
  }

  bool createDirectory(const std::string &path) {
    bool result = true;

    if (!fs::is_directory(path) || !fs::exists(path)) {
      result = fs::create_directory(path);
    }
    return result;
  }

  std::string getDirectoryPath(const std::string &path) {
    std::size_t sepPos = path.rfind('/');
    if (sepPos != std::string::npos) {
      return std::move(path.substr(0, sepPos));
    }
    return path;
  }

  bool fileExists(const std::string &fileName) {
    return fs::exists(fileName);
  }

  std::string getAbsoluteFilePath(const std::string &relativePath, bool expand) {
    std::string path = relativePath;
    if (expand) {
      path = expandString(relativePath);
    }
    return fs::absolute(path);
  }

  void deleteFile(const std::string &basicString) {
    if (fileExists(basicString))
      std::experimental::filesystem::remove(basicString);
    // no error if file does not exist
  }

  bool deleteFolder(const std::string &path) {
    bool result = true;
    if (fileExists(path)) {
      auto returnCode = std::experimental::filesystem::remove_all(path);
      result = (returnCode != -1);
    }
    return result;
  }

  std::string readFile(const std::string &path) {
    if (!fileExists(path)) {
      throw std::invalid_argument("Error reading file: \"" + path + "\" does not exist");
    }
    if (fs::is_directory(path)) {
      throw std::invalid_argument("Error reading file: \"" + path + "\" is a directory");
    }
    std::ifstream inputFile(path);

    // see https://stackoverflow.com/a/2602060/3997725 for more info
    std::string result((std::istreambuf_iterator<char>(inputFile)),
                       std::istreambuf_iterator<char>());
    inputFile.close();
    return result;
  }

  std::string expandString(const std::string &string) {
    if (string.empty()) {
      // warning: the code below runs into SIGSEGV when string is empty
      return string;
    }
    std::string result = string;

    wordexp_t exp_result;
    int ret = wordexp(string.c_str(), &exp_result, 0) != 0;
    if (ret == 0) {
      result = exp_result.we_wordv[0];
      wordfree(&exp_result);
    }

    return result;
  }

  bool isAbsolutePath(const std::string &path) {
    fs::path toCheck = path;
    return toCheck.is_absolute();
  }

  std::string getAbsolutePath(const std::string &fileName) {
    return fs::absolute(fileName);
  }

  std::string ltrim(const std::string &string) {
    return std::regex_replace(string, std::regex("^\\s+"), std::string(""));
  }

  std::string rtrim(const std::string &string) {
    return std::regex_replace(string, std::regex("\\s+$"), std::string(""));
  }

  std::string trim(const std::string &string) {
    return ltrim(rtrim(string));
  }

  std::string formatString(const std::string &format, const std::vector<std::string> &values) {
    std::regex tokenRegex("%([0-9]+)"); // to match %0, %1, ... and extract the number
    std::ostringstream result;
    std::string searchString = format;
    std::smatch currentMatch;
    while (std::regex_search(searchString, currentMatch, tokenRegex)) {
      unsigned int valueIndex = std::stoul(currentMatch[1]);
      if (valueIndex > std::numeric_limits<unsigned int>::max()) {
        throw std::out_of_range(currentMatch[1]);
      }
      if (valueIndex >= values.size()) {
        std::ostringstream errorMessage;
        errorMessage
          << "Token with index out of range: %" << valueIndex << " (there are only "
          << values.size() << " possible values available, addressable using %0 to %"
          << values.size() - 1 << ")";
        throw std::out_of_range(errorMessage.str());
      }
      result
        << currentMatch.prefix().str()
        << values.at(valueIndex);
      searchString = currentMatch.suffix().str();
    }
    result << searchString;
    return result.str();
  }

  std::vector<std::string> splitString(const std::string &string, const std::string &delimiter) {
    std::vector<std::string> result;

    auto start = 0U;
    auto end = string.find(delimiter);
    while (end != std::string::npos) {
      auto token = util::trim(string.substr(start, end - start));
      if (!token.empty()) {
        result.push_back(token);
      }
      start = end + delimiter.length();
      end = string.find(delimiter, start);
    }

    // substr(start, -1) returns everything from start to remainder
    auto token = util::trim(string.substr(start, end));
    if (!token.empty()) {
      result.push_back(token);
    }

    return result;
  }

  bool processRunning(pid_t pid) {
    return (getpgid(pid) >= 0);
  }

  void stringReplace(std::string &target, const std::string &toReplace, const std::string &replaceWith) {
    if (toReplace.empty()) {
      throw std::invalid_argument("String to replace must not be empty");
    }
    size_t pos = 0;
    while ((pos = target.find(toReplace, pos)) != std::string::npos) {
      target.replace(pos, toReplace.size(), replaceWith);
      pos += replaceWith.length();
    }
  }

} // namespace util
