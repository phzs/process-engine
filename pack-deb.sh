#!/bin/bash

mkdir -p build
cmake -B build/ -S .
cmake --build build/
( cd build && cpack )

echo
echo "DEB content:"
echo "------------------------------"
# shellcheck disable=SC2012
DEB=$(ls -t build/*.deb | head -1)
dpkg -I "$DEB"
dpkg -c "$DEB"
