/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "WorkflowBranch.h"
#include "nodes/Node.h"
#include "ports/BranchingPort.h"

WorkflowBranch::WorkflowBranch(AbstractWorkflow *workflow, BranchingNode *originNode, const BranchingPort *rootPort)
    : originNode(originNode), originPort(rootPort), parentWorkflow(workflow) {
  if (!parentWorkflow) {
    throw std::invalid_argument("WorkflowBranch must be initialized with a non-null parent");
  }
  if (!rootPort) {
    throw std::invalid_argument("WorkflowBranch must be initialized with a non-null root port");
  }
}

Node *WorkflowBranch::getNode(const std::string &nodeId) const {
  assumeInBranch(nodeId);
  return parentWorkflow->getNode(nodeId);
}

const Node *WorkflowBranch::getNodeConst(const std::string &nodeId) const {
  assumeInBranch(nodeId);
  if (branchNodeIds.find(nodeId) == branchNodeIds.end()) {
    throw std::invalid_argument("Node " + nodeId + " is node in the branch");
  }
  return parentWorkflow->getNodeConst(nodeId);
}

std::unordered_map<std::string, Node *> WorkflowBranch::getNodes() {
  std::unordered_map<std::string, Node *> result;
  for (const auto &branchNodeId : branchNodeIds) {
    result[branchNodeId] = parentWorkflow->getNode(branchNodeId);
  }
  return result;
}

std::unordered_map<std::string, const Node *> WorkflowBranch::getNodesConst() const {
  std::unordered_map<std::string, const Node *> result;
  for (const auto &branchNodeId : branchNodeIds) {
    result[branchNodeId] = parentWorkflow->getNodeConst(branchNodeId);
  }
  return result;
}

bool WorkflowBranch::isInBranch(const std::string &nodeId) const {
  return branchNodeIds.find(nodeId) != branchNodeIds.end();
}

void WorkflowBranch::assumeInBranch(const std::string &nodeId) const {
  if (!isInBranch(nodeId)) {
    throw std::invalid_argument("Node " + nodeId + " is node in the branch");
  }
}

void WorkflowBranch::addNode(const std::string &nodeId) {
  branchNodeIds.insert(nodeId);
}

bool WorkflowBranch::areCoherentGroupsInitialized() const {
  return parentWorkflow->areCoherentGroupsInitialized();
}

void WorkflowBranch::initCoherentGroups() {
  parentWorkflow->initCoherentGroups();
}

std::string WorkflowBranch::getIdentifier() const {
  return branchIdentifier(originPort->getNodeId(), originPort->getIndex());
}

std::unordered_set<std::string> WorkflowBranch::getNodeIds() const {
  return branchNodeIds;
}

std::string WorkflowBranch::branchIdentifier(const std::string &rootNodeId, unsigned int portIndex) {
  return rootNodeId + "," + std::to_string(portIndex);
}

bool WorkflowBranch::isEmpty() const {
  return branchNodeIds.empty();
}

const BranchingPort *WorkflowBranch::getOriginPort() const {
  return originPort;
}

int WorkflowBranch::getBranchTokens() const {
  int branchTokens = 0;
  for (const std::string &branchNodeId : branchNodeIds) {
    Node *node = getNode(branchNodeId);
    if (node->getBranchParent() == originNode && node->getDirectDependentsWithBranchParent(originNode).empty()) {
      branchTokens++;
    }
  }
  return branchTokens;
}

void WorkflowBranch::skip() {
  for (const auto &entry : getNodes()) {
    Node *node = entry.second;
    node->skip();
  }
}
