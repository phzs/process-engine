/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once

#include "AbstractWorkflow.h"
#include <unordered_set>

class Workflow;
class BranchingPort;
class BranchingNode;

class WorkflowBranch : public AbstractWorkflow {

public:
  explicit WorkflowBranch(AbstractWorkflow *parent, BranchingNode *originNode, const BranchingPort *rootPort);
  ~WorkflowBranch() override = default;

  // interface implementations
  Node *getNode(const std::string &nodeId) const override;
  const Node *getNodeConst(const std::string &nodeId) const override;
  std::unordered_map<std::string, Node *> getNodes() override;
  std::unordered_map<std::string, const Node *> getNodesConst() const override;
  bool isEmpty() const override;
  bool areCoherentGroupsInitialized() const override;
  void initCoherentGroups() override;
  std::unordered_set<std::string> getNodeIds() const override;
  std::string getIdentifier() const override;

  // specific to this type of AbstractWorkflow
  void addNode(const std::string &nodeId);
  bool isInBranch(const std::string &nodeId) const;

  // return a unique string to identify a branch
  static std::string branchIdentifier(const std::string &rootNodeId, unsigned int portIndex);

  const BranchingPort *getOriginPort() const;
  Node *getOriginNode() const;

  int getBranchTokens() const;

  void skip();

private:
  BranchingNode *originNode;
  const BranchingPort *originPort;

  // this is just to know which nodes belong to this branch
  // the ownership still is in the Workflow itself
  std::unordered_set<std::string> branchNodeIds;

  AbstractWorkflow *parentWorkflow;

  void assumeInBranch(const std::string &nodeId) const;
};
