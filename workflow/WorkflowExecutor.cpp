/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "WorkflowExecutor.h"
#include "../Logger.h"
#include "WorkflowAnalyzer.h"
#include "nodes/SourceNode.h"
#include "nodes/control/LoopNode.h"
#include "ports/BranchingPort.h"
#include <future>

using namespace std::chrono_literals;


WorkflowExecutor::WorkflowExecutor(int nThreads)
    : threadPool(nThreads) { // TODO add CLI param for number of threads
}

WorkflowExecutor::~WorkflowExecutor() {
  threadPool.stop(true);
}

bool WorkflowExecutor::execute(AbstractWorkflow *workflow, WorkflowExecution *execution) {
  bool success = true;
  execution->getInteractionManager()->setExecutor(this);
  execution->getInteractionManager()->setExecution(execution);
  execution->setState(ExecutionState::RUNNING);
  auto nodes = workflow->getNodes();

  printExecutionStartMessage(workflow, execution->isDryRun());
  execution->persistInfo();

  std::vector<std::string> startNodes = WorkflowAnalyzer::getStartNodes(workflow->getNodesConst());
  if (startNodes.empty()) {
    LOG("Error: Can not start workflow. No starting node found (all nodes have dependencies)");
    execution->setState(ExecutionState::ERROR);
    execution->persistInfo();
    return false;
  }

  for (const std::string &nodeId : startNodes) {
    Node *node = workflow->getNode(nodeId);
    nodesToExecute.push_back(node);
  }

  do {
    std::vector<Node *> nodesToStart = nodesToExecute;
    nodesToExecute.clear();

    lock();
    for (Node *node : nodesToStart) {
      if (!node->toBeExecuted()) {
        continue;
      }
      threadPool.push([execution, node, this](int n) {
        try {
          if (!node->execute(execution)) {
            execution->handleError(node);
            threadPool.stop(false);
            nodesToExecute.clear();
          } else {
            lock();
            util::joinVector(nodesToExecute, node->nodeFinished(execution));
            unlock();
          }
        } catch (...) {
          execution->handleError(node, std::current_exception());
        }
        notify(); // always notify when a worker exits, otherwise the executor might wait infinitely without ever checking his loop condition again
      });
    }
    unlock();
    std::unique_lock<std::mutex> waitLock(waitMutex);
    executorCV.wait_for(waitLock, 50ms);
    execution->persistTree();
  } while ((!nodesToExecute.empty() || !isThreadPoolIdling() || execution->getInteractionManager()->hasUnresolvedInteractions()) && execution->getState() != ExecutionState::ERROR && !execution->isCancelling());
  unlock();
  LOG_VERBOSE("executor", "unlock, pool: " << threadPool.n_idle() << "/" << threadPool.size() << ", jobs: " << nodesToExecute.size());
  threadPool.stop(false);
  workflow->skipUnterminatedNodes(); // this is only to indicate that nodes won't get processed anymore

  if (execution->getState() != ExecutionState::ERROR && !execution->isCancelling()) {
    execution->setState(ExecutionState::FINISHED);
    execution->createShortcuts();
  }
  execution->createShortcuts();
  execution->persistInfo();
  printExecutionResult(workflow, execution);
  return success;
}

void WorkflowExecutor::printExecutionStartMessage(const AbstractWorkflow *workflow, bool dryRun) {
  bool currentlyInBranch = isBranch(workflow);
  std::string workflowType = currentlyInBranch ? "BRANCH" : "WORKFLOW";
  LOG(workflowType << " EXECUTION starts");
  if (dryRun) {
    LOG(" (dry run, only printing commands)");
  }
}

bool WorkflowExecutor::isBranch(const AbstractWorkflow *workflow) {
  auto branch = dynamic_cast<const WorkflowBranch *>(workflow);
  return branch != nullptr;
}

void WorkflowExecutor::printCancelMessage(AbstractWorkflow *workflow) {
  if (!isBranch(workflow)) {
    LOG("Workflow execution successfully cancelled.");
  }
}

void WorkflowExecutor::printExecutionResult(AbstractWorkflow *workflow, const WorkflowExecution *execution) {
  std::string workflowType = isBranch(workflow) ? "Branch" : "Workflow";
}

void WorkflowExecutor::lock() {
  executorMutex.lock();
}

void WorkflowExecutor::unlock() {
  executorMutex.unlock();
}

void WorkflowExecutor::notify() {
  executorCV.notify_one();
}

bool WorkflowExecutor::isThreadPoolIdling() {
  return threadPool.size() == threadPool.n_idle();
}

void WorkflowExecutor::addNodesToExecute(const std::vector<Node *> &nodes) {
  if (nodes.empty()) return;
  lock();
  util::joinVector(nodesToExecute, nodes);
  unlock();
  notify();
}
