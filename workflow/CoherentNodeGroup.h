/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once

#include "StaticData.h"
#include <memory>
#include <unordered_set>
#include <vector>

class SystemCommandNode;
class PipePort;

enum class CoherentConnectionType {
  PIPE,
  PIPE_TO_OTHER, // PipePort to any non-coherent port type
  INVALID,
  NONE
};

struct CoherentNodePtr {
  const SystemCommandNode *connectedNode;
  CoherentConnectionType connectionType;

  bool operator==(const CoherentNodePtr &rhs) const;
  bool operator!=(const CoherentNodePtr &rhs) const;
};

// the same as CoherentNodePtr, but contains only the Id of the connected Node and the type
struct CoherentConnectionInfo {
  std::string connectedNodeId;
  CoherentConnectionType connectionType;
};

class CoherentNodeGroup {
public:
  void addStartNode(const SystemCommandNode *node);

  bool operator==(const CoherentNodeGroup &rhs) const;

  bool operator!=(const CoherentNodeGroup &rhs) const;

  /**
   * Adds a member to this group where the type of coherent connection is specified via the optional @param type.
   * @param node Pointer to a SystemCommandNode which will be added as new group member
   * @param type Type of connection to the previous group member, NONE if this is the first member in the group
   */
  void addCoherentNode(const SystemCommandNode *node, CoherentConnectionType type = CoherentConnectionType::NONE);
  unsigned int size() const;
  bool empty() const;

  /**
   * Returns a list of Pointers to the group members together with the type of the connection between them.
   * @return vector of CoherentNodePtr
   */
  std::vector<CoherentNodePtr> getCoherentNodes() const;

  /**
   * Returns a set of the node ids of all members of this group in no particular order.
   * @return unordered set of node ids
   */
  std::unordered_set<std::string> getNodeIds() const;

  /**
   * Calculates the dependencies of all group members as if they were one node.
   * (Excludes the group members themselves)
   * @return Set of Ids of the nodes the group depends on
   */
  std::unordered_set<std::string> getGroupDependencies() const;

  /**
   * Gives a string representation of this group including the node ids and type of connections between them
   * @return string representation of this group
   */
  std::string toString() const;

  /**
   * Set an output data port, so that output data for a connection to a non-PipePort can be set when the group gets
   * executed.
   * @param portPtr
   */
  void setPipeOutputPort(PipePort *portPtr);

  /**
   * If there was an output pipe port set using setPipeOutputPort(PipePort *portPtr),
   * propagate data to it, to allow connected non-PipePorts to read it.
   * @param data
   */
  void setPipeOutputData(std::shared_ptr<StaticData> data);

  std::string getCommand(const WorkflowExecution *execution) const;

private:
  std::vector<CoherentNodePtr> group;
  PipePort *pipeOutputPort;
};
