/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once

#include <string>

class WorkflowExecution;

class StaticData {
public:
  StaticData();
  explicit StaticData(bool data);
  explicit StaticData(double data);
  explicit StaticData(int data);
  explicit StaticData(float data);
  explicit StaticData(const char *data); // to make it work for string literals
  explicit StaticData(std::string data);

  [[nodiscard]] bool isEmpty() const;
  [[nodiscard]] bool toBoolean() const;
  [[nodiscard]] float toFloat() const;
  [[nodiscard]] int toInt() const;
  int expandInt(WorkflowExecution *execution) const;
  bool expandBool(WorkflowExecution *execution) const;
  [[nodiscard]] std::string toString() const;

private:
  static int stringToInt(const std::string &value);
  static float stringToFloat(const std::string &value);
  static bool stringToBool(const std::string &value);
  bool empty;
  std::string raw_data;
};
