/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "../Logger.h"
#include "../config.h"
#include "../util.h"
#include "WorkflowAnalyzer.h"
#include "nodes/FileOutputNode.h"
#include "nodes/Node.h"
#include "nodes/UserInteractionNode.h"
#include "nodes/control/BranchingNode.h"
#include "state/WorkflowExecution.h"
#include <iomanip>
#include <iostream>
#include <regex>
#include <set>

#include "Workflow.h"

Workflow::Workflow()
    : coherentGroupsInitialized(false), longestNodeContextSize(0) {
}

Node *Workflow::addNode(std::unique_ptr<Node> node) {
  if (node) {
    node->setWorkflow(this);
    std::string id = node->getId();
    updateLongestContextSize(node.get());
    nodes[id] = std::move(node);
    return nodes[id].get();
  } else {
    throw std::invalid_argument("Can not add node which is null");
  }
}

void Workflow::connectPorts(std::string const &nodeLeftId, int portLeftIndex, std::string const &nodeRightId, int portRightIndex, bool throwOnFailure) {
  auto left = nodes.find(nodeLeftId);
  auto right = nodes.find(nodeRightId);
  bool foundLeft = (left != nodes.end());
  bool foundRight = (right != nodes.end());
  if ((!foundLeft) || (!foundRight)) {
    std::ostringstream logStream;
    logStream << "Unable to connect nodes:\n";
    if (!foundLeft) logStream << "- Left node (" << nodeLeftId << ") not found\n";
    if (!foundRight) logStream << "- Right node (" << nodeRightId << ") not found\n";
    LOG_VERBOSE("workflow", logStream.str());
    if (throwOnFailure) {
      throw std::invalid_argument(logStream.str());
    }
    return;
  }
  auto portLeft = left->second->getPort(PortDirection::Out, portLeftIndex);
  auto portRight = right->second->getPort(PortDirection::In, portRightIndex);
  if ((!portLeft) || (!portRight)) {
    std::ostringstream logStream;
    logStream << "Unable to connect ports:\n";
    if (!portLeft) logStream << "- Left port (" << nodeLeftId << ", " << portLeftIndex << ") not found\n";
    if (!portRight) logStream << "- Right port (" << nodeRightId << ", " << portRightIndex << ") not found\n";
    LOG_VERBOSE("workflow", logStream.str());
    if (throwOnFailure) {
      throw std::invalid_argument(logStream.str());
    }
    return;
  }

  for (auto connectedPort : portLeft->getConnectedPorts()) {
    if (connectedPort->getNodeId() == portRight->getNodeId() && connectedPort->getIndex() == portRight->getIndex()) {
      std::ostringstream error;
      error << "Connecting ports " + portLeft->toString() << " and "
            << " would result in a duplicate connection! \nPlease check your .flow file for consistency!";
      throw std::invalid_argument(error.str());
    }
  }

  /* The only way to invalidate the computed coherent groups of SystemCommandNodes,
   * is to add a new connection between CoherencyPorts (since deleting connections is not supported).
   * Force re-initialization in case such a connection is created. */
  auto isCoherencyPort = [](const Port *port) {
    auto coherencyPort = dynamic_cast<const CoherencyPort *>(port);
    return (coherencyPort != nullptr);
  };
  if (isCoherencyPort(portLeft) || isCoherencyPort(portRight)) {
    coherentGroupsInitialized = false;
  }
  portLeft->addConnectedPort(portRight);
  portRight->addConnectedPort(portLeft);
}

std::unordered_map<std::string, Node *> Workflow::getNodes() {
  std::unordered_map<std::string, Node *> result;
  for (const auto &node : nodes) {
    result[node.first] = node.second.get();
  }
  return result;
}

std::unordered_map<std::string, const Node *> Workflow::getNodesConst() const {
  std::unordered_map<std::string, const Node *> result;
  for (const auto &node : nodes) {
    result[node.first] = node.second.get();
  }
  return result;
}

const Node *Workflow::getNodeConst(const std::string &nodeId) const {
  if (nodes.find(nodeId) == nodes.end()) {
    throw std::invalid_argument("Requested invalid node id: " + nodeId);
  }
  return nodes.at(nodeId).get();
}

Node *Workflow::getNode(const std::string &nodeId) const {
  if (nodes.find(nodeId) == nodes.end()) {
    throw std::invalid_argument("Requested invalid node id: " + nodeId);
  }
  return nodes.at(nodeId).get();
}

void Workflow::initCoherentGroups() {
  if (coherentGroupsInitialized) {
    // groups already initialized
    return;
  }
  WorkflowAnalyzer workflowAnalyzer(this);

  for (const auto &element : nodes) {

    auto &nodeId = element.first;
    auto &nodePtr = element.second;

    auto systemCommandNode = dynamic_cast<SystemCommandNode *>(nodePtr.get());
    if (systemCommandNode) {
      // avoid re-initializing
      if (!systemCommandNode->isInCoherentGroup()) {
        auto nodeGroup = workflowAnalyzer.findCoherentNodeGroup(nodeId);
        if (nodeGroup && !nodeGroup->empty()) {
          systemCommandNode->setCoherentGroup(nodeGroup);
          for (auto &pipeNode : nodeGroup->getCoherentNodes()) {
            if (systemCommandNode->getId() == pipeNode.connectedNode->getId()) {
              continue;
            }
            auto groupMember = dynamic_cast<SystemCommandNode *>(nodes[pipeNode.connectedNode->getId()].get());
            groupMember->setCoherentGroup(nodeGroup);
          }
        }
      }
    }
  }
  coherentGroupsInitialized = true;
}

bool Workflow::areCoherentGroupsInitialized() const {
  return coherentGroupsInitialized;
}

bool Workflow::isEmpty() const {
  return nodes.empty();
}

std::unordered_set<std::string> Workflow::getNodeIds() const {
  return util::keys(nodes);
}

void Workflow::updateLongestContextSize(Node *node) {
  size_t contextLength = node->getContext().size();
  if (contextLength > longestNodeContextSize) {
    longestNodeContextSize = contextLength;
  }
}

unsigned int Workflow::getLongestNodeContextSize() const {
  return longestNodeContextSize;
}

unsigned int Workflow::getNodesProcessed() const {
  unsigned int result = 0;
  for (const auto &node : getNodesConst()) {
    if (node.second->getBranchParent()) continue;
    result += node.second->getNodesProcessed();
  }
  return result;
}

unsigned int Workflow::getNodesProcessedInLoop() const {
  unsigned int result = 0;
  for (const auto &node : getNodesConst()) {
    if (node.second->getBranchParent()) continue;
    result += node.second->getNodesProcessedInLoop();
  }
  return result;
}

unsigned int Workflow::getNodesTotal() const {
  unsigned int result = 0;
  for (const auto &node : getNodesConst()) {
    if (node.second->getBranchParent()) continue;
    result += node.second->getNodesTotal();
  }
  return result;
}

json Workflow::getTree() const {
  json result = json::object();
  for (const auto &node : getNodesConst()) {
    const json &node_object = node.second->toJson();
    if (!node_object.is_null()) {
      result[node.second->getId()] = node_object;
    }
  }
  return result;
}
