/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "NodeState.h"
#include "../AbstractWorkflow.h"

NodeState::NodeState(NodeExecutionState state, int tokens)
    : tokens(tokens), branchTokens(-1), currentState(state), activeBranch(nullptr) {
}

NodeState::NodeState(const json &jsonObject)
    : tokens(-1), branchTokens(-1), activeBranch(nullptr) {
  fromJson(jsonObject);
}

json NodeState::toJson() const {
  if (stdoutData.empty()) {
    return json::array({currentState, loopIteration});
  }
  return json::array({currentState, loopIteration, stdoutData});
}

void NodeState::fromJson(const json &obj) {
  currentState = obj[0];
  loopIteration = obj[1];
  if (obj.size() > 2) {
    stdoutData = obj[2];
  }
}

bool NodeState::dependencyResolved() const {
  return (currentState == NodeExecutionState::EXECUTED) || (currentState == NodeExecutionState::NOOP);
}

bool NodeState::stillToProcess() const {
  return (currentState == NodeExecutionState::TO_BE_EXECUTED) || (currentState == NodeExecutionState::NEEDS_INTERACTION);
}

bool NodeState::persist() const {
  return currentState != NodeExecutionState::NOOP;
}

bool NodeState::needsInteraction() const {
  return currentState == NodeExecutionState::NEEDS_INTERACTION;
}

bool NodeState::countProcessed() const {
  return isExecuted();
}

bool NodeState::countTotal() const {
  return (currentState != NodeExecutionState::NOOP) && (currentState != NodeExecutionState::NOT_SET) && (currentState != NodeExecutionState::TO_BE_SKIPPED);
}

void NodeState::setCurrentState(NodeExecutionState state) {
  NodeState::currentState = state;
}

NodeExecutionState NodeState::getCurrentState() const {
  return currentState;
}

unsigned int NodeState::getLoopIteration() const {
  return loopIteration;
}

const std::string &NodeState::getStdoutData() const {
  return stdoutData;
}

void NodeState::setStdoutData(const std::string &data) {
  NodeState::stdoutData = data;
}

void NodeState::setLoopIteration(unsigned int value) {
  loopIteration = value;
}

void NodeState::skip() {
  currentState = NodeExecutionState::TO_BE_SKIPPED;
}

bool NodeState::isSkipped() const {
  return currentState == NodeExecutionState::TO_BE_SKIPPED;
}

bool NodeState::isExecuted() const {
  return currentState == NodeExecutionState::EXECUTED;
}

void NodeState::setBranchTokens(int tokens) {
  branchTokens = tokens;
}

WorkflowBranch *NodeState::getActiveBranch() const {
  return activeBranch;
}

void NodeState::setActiveBranch(WorkflowBranch *activeBranch) {
  NodeState::activeBranch = activeBranch;
}

bool NodeState::isRunning() {
  return currentState == NodeExecutionState::RUNNING;
}
