/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "WorkflowExecution.h"
#include "../../Logger.h"
#include "../Workflow.h"
#include "../WorkflowAnalyzer.h"
#include "../WorkflowBranch.h"
#include "../WorkflowExecutor.h"
#include "../interaction/FileInteractionManager.h"
#include "../nodes/SourceNode.h"
#include "../nodes/SystemCommandNode.h"
#include <fstream>
#include <iomanip>
#include <stdexcept>
#include <utility>

WorkflowExecution::WorkflowExecution(std::unique_ptr<InteractionManager> interactionManager, AbstractWorkflow *workflow, bool enablePersistence,
                                     WorkflowExecution *parent)
    : workflow(workflow), enablePersistence(enablePersistence), parentExecution(parent),
      pid(0), iteration(0), dryRun(false), state(ExecutionState::READY), cancelExecution(false),
      interactionManager(std::move(interactionManager)) {

  if (parent) {
    iteration = parent->getIteration() + 1;
  } else {
    WorkflowAnalyzer workflowAnalyzer(workflow);
    workflow->initCoherentGroups();
    workflow->initBranches();
    if (enablePersistence) {
      persistTree();
    }
    workflow->resetNodeStates(); // important to calculate token counters initially!
  }
}

json WorkflowExecution::toJson() const {
  json jsonObj;
  jsonObj["pid"] = pid;
  jsonObj["fileName"] = fileName;
  jsonObj["state"] = executionStateToString(state);
  jsonObj["startDateTime"] = startDateTime;
  jsonObj["endDateTime"] = endDateTime;
  jsonObj["processEngine"] = "SequentialPE";
  jsonObj["nodesProcessed"] = workflow->getNodesProcessed();
  jsonObj["nodesProcessedInLoops"] = workflow->getNodesProcessedInLoop();
  jsonObj["nodesTotal"] = workflow->getNodesTotal();
  jsonObj["iteration"] = iteration;

  jsonObj[std::string(config::VARIABLES_KEY)] = variables;

  return jsonObj;
}

void WorkflowExecution::setState(ExecutionState newState) {
  state = newState;
}

void WorkflowExecution::setFileName(const std::string &s) {
  fileName = s;
}

void WorkflowExecution::setStartDateTime(const std::string &dateTime) {
  startDateTime = dateTime;
}

void WorkflowExecution::persistInfo() const {
  if (enablePersistence) {
    json jsonInfo = toJson();
    std::string statusFile = std::string(config::STATUS_FILENAME);
    std::ofstream out(statusFile);
    out << std::setw(config::JSON_INDENT) << jsonInfo << std::endl;
  }
  persistTree();
}

void WorkflowExecution::persistTree() const {
  if (enablePersistence) {
    auto rootWorkflow = dynamic_cast<Workflow *>(workflow);
    if (rootWorkflow) {
      json tree = rootWorkflow->getTree();
      std::string treeFile = std::string(config::TREE_FILENAME);
      std::ofstream out(treeFile);
      out << std::setw(config::JSON_INDENT) << tree << std::endl;
    }
  }
}

void WorkflowExecution::setPid(pid_t p) {
  pid = p;
}

int WorkflowExecution::getIteration() const {
  if (parentExecution) {
    return parentExecution->getIteration();
  }
  return iteration;
}

void WorkflowExecution::setEndDateTime(const std::string &dateTime) {
  endDateTime = dateTime;
}


ExecutionState WorkflowExecution::getState() const {
  return state;
}

void WorkflowExecution::setIteration(int n) {
  iteration = n;
}

void WorkflowExecution::setVariable(const std::string &name, const std::string &value, bool overwrite, bool silent) {
  if (parentExecution) {
    // note: this would be a problem when running branches in parallel because they all share the same variable store
    parentExecution->setVariable(name, value, overwrite, silent);
  } else {
    if (overwrite || (variables.find(name) == variables.end())) {
      variables[name] = std::string(value);
      if (!silent) LOG("${" << name << "} = \"" << value << "\"");
      else
        LOG_VERBOSE("execution", "${" << name << "} = \"" << value << "\"");
    } else {
      LOG_VERBOSE("execution", "${" << name << "} unchanged (already set to \"" << value << "\")");
    }
  }
}

std::string WorkflowExecution::expandVariables(const std::string &string) const {
  if (parentExecution) {
    return parentExecution->expandVariables(string);
  }
  std::string result(string);
  std::smatch matches;

  int i = 0;
  std::string variableRegexString(config::VARIABLE_REGEX);
  std::regex variableRegex(variableRegexString);
  while (std::regex_search(result, matches, variableRegex)) {
    if (++i > config::MAX_VAR_REPLACEMENTS) throw std::logic_error("Maximum variable replacement attempts exceeded.");
    std::string to_replace(matches[0]);
    std::string variable_name(matches[1]);

    auto variable = variables.find(variable_name);
    if (variable != variables.end()) {
      LOG_VERBOSE("execution", "Replacing variable \"" << variable_name << "\" with value \"" << variable->second << "\".");
      result.replace(matches.position(0), to_replace.size(), variable->second);
    } else {
      LOG("Warning: Variable \"" << variable_name << "\" not set!");
      result.replace(matches.position(0), to_replace.size(), config::VARIABLE_VALUE_UNDEFINED);
    }
  }
  return result;
}

NodeState &WorkflowExecution::getNodeState(const std::string &nodeId) const {
  return workflow->getNode(nodeId)->getState();
}

void WorkflowExecution::cancel() {
  if (parentExecution) {
    parentExecution->cancel();
    return;
  }
  cancelExecution = true;
  setState(ExecutionState::CANCELLING);
  persistInfo();
}

bool WorkflowExecution::isCancelling() {
  if (parentExecution) {
    return parentExecution->isCancelling();
  }
  return cancelExecution;
}

void WorkflowExecution::setVariables(const std::unordered_map<std::string, std::string> &vars, bool silent) {
  for (const auto &var : vars) {
    setVariable(var.first, var.second, true, silent);
  }
}

std::string WorkflowExecution::getBranchIdentifier() const {
  return workflow->getIdentifier();
}

bool WorkflowExecution::isDryRun() const {
  return dryRun;
}

void WorkflowExecution::setDryRun(bool dry) {
  dryRun = dry;
}

void WorkflowExecution::createShortcuts() const {
  // create shortcuts
  json jsonObject;
  jsonObject["shortcuts"] = json::array();
  for (const auto &relativePath : shortcuts) {
    std::string path = relativePath;
    try {
      path = util::getAbsoluteFilePath(relativePath);
    } catch (const std::invalid_argument &err) {
      LOG("Failed to convert relative shortcut path to absolute path: " << err.what());
    }
    json shortcutJson;
    shortcutJson["path"] = path;
    shortcutJson["name"] = relativePath;
    jsonObject["shortcuts"].push_back(shortcutJson);
  }
  std::string path = std::string(config::SHORTCUT_FILENAME);
  std::ofstream out(path);
  out << std::setw(4) << jsonObject << std::endl;
}

void WorkflowExecution::handleError(const Node *node, std::exception_ptr exception) {
  setState(ExecutionState::ERROR);
  persistInfo();
  LOG("An error occurred while executing the node " << node->getId() << ", shutting down gracefully.");
  if (exception != nullptr) {
    try {
      std::rethrow_exception(exception);
    } catch (const std::exception &e) {
      LOG("Exception message: " << e.what());
    }
  }
}

void WorkflowExecution::setNodeState(const std::string &nodeId, NodeState newState) {
  workflow->getNode(nodeId)->setState(std::move(newState));
}

InteractionManager *WorkflowExecution::getInteractionManager() {
  return interactionManager.get();
}
