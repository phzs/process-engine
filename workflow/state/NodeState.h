/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once

#include <nlohmann/json.hpp>

class WorkflowBranch;

using json = nlohmann::json;

enum class NodeExecutionState {
  NOT_SET,
  NOOP, // no execution necessary, do not count
  TO_BE_EXECUTED,
  TO_BE_SKIPPED,
  RUNNING,
  EXECUTED,
  NEEDS_INTERACTION,
  ERROR
};

NLOHMANN_JSON_SERIALIZE_ENUM(NodeExecutionState, {
                                                   {NodeExecutionState::NOT_SET, nullptr},
                                                   {NodeExecutionState::NOOP, "N"},
                                                   {NodeExecutionState::TO_BE_EXECUTED, "TBE"},
                                                   {NodeExecutionState::TO_BE_SKIPPED, "TBS"},
                                                   {NodeExecutionState::RUNNING, "R"},
                                                   {NodeExecutionState::EXECUTED, "EX"},
                                                   {NodeExecutionState::NEEDS_INTERACTION, "NI"},
                                                   {NodeExecutionState::ERROR, "ER"},
                                                 })

class NodeState {
public:
  explicit NodeState(NodeExecutionState state = NodeExecutionState::NOOP, int tokens = -1);
  explicit NodeState(const json &jsonObject);

  void setCurrentState(NodeExecutionState currentState);
  void setLoopIteration(unsigned int value);
  void setStdoutData(const std::string &stdoutData);
  [[nodiscard]] unsigned int getLoopIteration() const;
  [[nodiscard]] NodeExecutionState getCurrentState() const;
  [[nodiscard]] const std::string &getStdoutData() const;

  [[nodiscard]] json toJson() const;
  void fromJson(const json &obj);

  /**
   * This functions determines if the state of a node allows
   * dependent nodes to be executed.
   * @return True if the execution state allows dependent nodes to be executed.
   */
  [[nodiscard]] bool dependencyResolved() const;

  /**
   * Returns true if the node must still be processed.
   * Used for statistics only.
   * @return True if the node still must be processed according to this node state
   */
  [[nodiscard]] bool stillToProcess() const;

  /**
   * Decides whether this node state must to be persisted between workflow executions.
   * @return True if the node state information is important after execution.
   */
  [[nodiscard]] bool persist() const;

  /**
   * Decides whether this node state implies that the node needs user interaction.
   * @return True if the node needs interaction.
   */
  [[nodiscard]] bool needsInteraction() const;

  /**
   * Decides whether a node with this NodeState should be counted to the number of processed nodes.
   * @return True if the node with this state should be counted to processed nodes
   */
  [[nodiscard]] bool countProcessed() const;

  /**
   * Decides whether a node with this NodeState should be counted to the total number of nodes in the workflow.
   * @return True if the node with this state should be counted to nodes total
   */
  [[nodiscard]] bool countTotal() const;

  [[nodiscard]] bool isSkipped() const;

  bool takeToken() {
    if (tokens > 0) {
      tokens--;
      return true;
    }
    return false;
  };

  void setTokens(int t) {
    tokens = t;
  }

  [[nodiscard]] int tokenCount() const {
    return tokens;
  }


  void skip();

  [[nodiscard]] bool isExecuted() const;

  void setBranchTokens(int tokens);

  bool takeBranchToken() {
    if (branchTokens > 0) {
      branchTokens--;
      return true;
    }
    return false;
  }

  [[nodiscard]] int branchTokenCount() const {
    return branchTokens;
  }

  [[nodiscard]] WorkflowBranch *getActiveBranch() const;
  void setActiveBranch(WorkflowBranch *activeBranch);

  bool isRunning();

private:
  int tokens;
  int branchTokens;
  NodeExecutionState currentState = NodeExecutionState::NOT_SET;

  // only for LoopNode
  unsigned int loopIteration = 0;

  std::string stdoutData;

  // for BranchingNodes which have multiple branches, it stores a pointer to the branch they actually execute
  WorkflowBranch *activeBranch;
};
