/* Copyright 2021 Karlsruhe Institute of Technology
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License. */

#include "ExecutionState.h"
#include <stdexcept>

std::string executionStateToString(ExecutionState state) {
  switch (state) {
    case ExecutionState::READY:
      return "Ready";
    case ExecutionState::RUNNING:
      return "Running";
    case ExecutionState::NEEDS_INTERACTION:
      return "Needs_interaction";
    case ExecutionState::CANCELLING:
      return "Cancelling";
    case ExecutionState::CANCELLED:
      return "Cancelled";
    case ExecutionState::ERROR:
      return "Error";
    case ExecutionState::FINISHED:
      return "Finished";
    default:
      return "Unknown state";
  }
}

ExecutionState stringToExecutionState(const std::string &stringState) {
  if (stringState == "Ready") {
    return ExecutionState::READY;
  } else if (stringState == "Running") {
    return ExecutionState::RUNNING;
  } else if (stringState == "Needs_interaction") {
    return ExecutionState::NEEDS_INTERACTION;
  } else if (stringState == "Cancelling") {
    return ExecutionState::CANCELLING;
  } else if (stringState == "Cancelled") {
    return ExecutionState::CANCELLED;
  } else if (stringState == "Error") {
    return ExecutionState::ERROR;
  } else if (stringState == "Finished") {
    return ExecutionState::FINISHED;
  } else {
    throw std::logic_error("Unknown state string: " + stringState);
  }
}
