/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once

#include "../../config.h"
#include "../../util.h"
#include "../interaction/FileInteractionManager.h"
#include "ExecutionState.h"
#include <csignal>
#include <nlohmann/json.hpp>
#include <string>
#include <unordered_set>

using json = nlohmann::json;

class AbstractWorkflow;
class WorkflowBranch;
struct NodeState;
enum class NodeExecutionState;
class LoopNode;
class BranchingNode;

class WorkflowExecution {
public:
  explicit WorkflowExecution(std::unique_ptr<InteractionManager> interactionManager,
                             AbstractWorkflow *workflow = nullptr,
                             bool enablePersistence = false,
                             WorkflowExecution *parent = nullptr);

  void setVariable(const std::string &name, const std::string &value, bool overwrite = true, bool silent = false);
  void setVariables(const std::unordered_map<std::string, std::string> &vars, bool silent = false);
  std::string expandVariables(const std::string &string) const;

  void setPid(pid_t pid);
  void setState(ExecutionState state);
  void setFileName(const std::string &fileName);
  void setStartDateTime(const std::string &dateTime);
  void setEndDateTime(const std::string &dateTime);
  void setIteration(int iteration);
  void setDryRun(bool dry);

  [[nodiscard]] std::string getBranchIdentifier() const;

  void visitParents(std::function<void(const WorkflowExecution &execution)> &visitor) const {
    if (parentExecution) {
      parentExecution->visitParents(visitor);
    }
    visitor(*this);
  }

  // interface methods related to nodeState and statistics
  ExecutionState getState() const;
  NodeState &getNodeState(const std::string &nodeId) const;
  int getIteration() const;
  bool isDryRun() const;

  void addShortcut(const std::string &path) {
    shortcuts.insert(path);
  }

  void createShortcuts() const;

  // persistence
  json toJson() const;
  void persistInfo() const;
  void persistTree() const;

  /**
   * When cancel() was called, the Executor should attempt to gracefully shutdown the process of executing the workflow.
   * This decision can not be undone!
   */
  void cancel();

  /**
   * @return true if execution is about to be cancelled
   */
  bool isCancelling();

  void handleError(const Node *node, std::exception_ptr exception = nullptr);

  InteractionManager *getInteractionManager();

  void setNodeState(const std::string &nodeId, NodeState newState);

private:
  AbstractWorkflow *workflow;
  bool enablePersistence;
  WorkflowExecution *parentExecution;
  std::unordered_map<std::string, std::string> variables;

  // pid of the running execution
  pid_t pid;

  /* number which will be increased by one every time a specific workflow will be executed/continued
   * (used to group interactions into "pages") */
  int iteration;

  bool dryRun;

  ExecutionState state;
  std::string fileName;
  std::string startDateTime;
  std::string endDateTime;

  // this will be true if a SIGTERM was received, and a graceful shutdown should be performed
  bool cancelExecution;

  std::unordered_set<std::string> shortcuts;

  std::unique_ptr<InteractionManager> interactionManager;
};
