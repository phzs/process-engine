#include "InteractionManager.h"
#include "../WorkflowExecutor.h"
#include "../nodes/UserInteractionNode.h"

using namespace std::chrono_literals;

InteractionManager::InteractionManager(WorkflowExecutor *executor)
    : execution(nullptr), executor(executor) {
}

std::vector<Node *> InteractionManager::updateAndNotify() {

  std::vector<unsigned int> toNotify = updateValues();
  std::vector<Node *> reportToExecute;

  {
    std::lock_guard<std::mutex> guard(mutex);
    for (unsigned int index : toNotify) {
      notifyInteraction(index, reportToExecute);
    }

    // output interactions don't expect any input value, assume they have already been shown to the user
    for (unsigned int index : outputInteractionIds) {
      notifyInteraction(index, reportToExecute);
    }
    outputInteractionIds.clear();
  }

  toNotify.clear();
  return reportToExecute;
}

AbstractInteraction *InteractionManager::getInteraction(unsigned int id) {
  if (id > interactions.size()) {
    throw std::invalid_argument("Interaction id " + std::to_string(id) + " not found");
  }
  return interactions[id].get();
}

bool InteractionManager::hasUnresolvedInteractions() const {
  return !unresolvedInteractions.empty();
}

void InteractionManager::updateState(ExecutionState newState) {
  execution->setState(newState);
  execution->persistInfo();
}

void InteractionManager::processInteractions(const std::function<void(AbstractInteraction *)> &visitor) {
  for (const auto &interaction : interactions) {
    visitor(interaction.get());
  }
}

void InteractionManager::notifyInteraction(unsigned int index, std::vector<Node *> &nodesToExecute) {
  util::joinVector(nodesToExecute, interactions[index]->notify(execution));
  unresolvedInteractions.erase(interactions[index].get());
  if (unresolvedInteractions.empty() && execution->getState() == ExecutionState::NEEDS_INTERACTION) {
    updateState(ExecutionState::RUNNING);
  }
}
