/* Copyright 2021 Karlsruhe Institute of Technology
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License. */

#include "FileInteractionManager.h"
#include "../../Logger.h"
#include "../../util.h"

#include <utility>

FileInteractionManager::FileInteractionManager(std::string fileName)
    : InteractionManager(), fileName(std::move(fileName)) {
}

std::vector<unsigned int> FileInteractionManager::updateValues() {
  json jsonObj;
  {
    std::lock_guard<std::mutex> guard(fileMutex);
    jsonObj = getStoredJson();
  }

  std::vector<unsigned int> updatedInteractions;

  for (const auto &interactionJson : jsonObj[std::string(config::INTERACTION_ARRAY_KEY)]) {
    if (!interactionJson.contains("id")) throw std::invalid_argument("Invalid or broken interaction json");
    AbstractInteraction *interaction;
    try {
      interaction = getInteraction(std::stoul(interactionJson["id"].get<std::string>()));
    } catch (const std::invalid_argument &err) {
      LOG_VERBOSE("interactions", "Unexpected interaction entry for unknown interaction with id " << interactionJson["id"].get<unsigned int>());
      continue;
    }
    bool valueChanged = interaction->fromJson(interactionJson);
    if (valueChanged) {
      updatedInteractions.push_back(interaction->getId());
    }
  }
  return updatedInteractions;
}

void FileInteractionManager::interactionAdded(const AbstractInteraction *interaction) {
  std::lock_guard<std::mutex> guard(fileMutex);
  json jsonObj = getStoredJson();
  jsonObj[std::string(config::INTERACTION_ARRAY_KEY)].push_back(interaction->toJson());

  // save json file
  std::ofstream out(fileName);
  out << std::setw(4) << jsonObj << std::endl;
  LOG_VERBOSE("interactions", "Wrote interaction info to " << fileName);
}

json FileInteractionManager::getStoredJson() const {
  json jsonObj;
  if (util::fileExists(fileName)) {
    std::ifstream in(fileName);
    in >> jsonObj;
  }

  if (!jsonObj.contains(std::string(config::INTERACTION_ARRAY_KEY))) {
    jsonObj[std::string(config::INTERACTION_ARRAY_KEY)] = json::array();
  }
  return jsonObj;
}
