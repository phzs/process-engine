/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once

#include "../../Logger.h"
#include "../state/ExecutionState.h"
#include "AbstractInteraction.h"
#include "Interaction.h"
#include <mutex>
#include <thread>
#include <unordered_set>

template<typename T>
class UserInteractionNode;
class Node;
class WorkflowExecutor;

class InteractionManager {
public:
  explicit InteractionManager(WorkflowExecutor *executor = nullptr);
  virtual ~InteractionManager() = default;

  template<typename T>
  Interaction<T> *registerInteraction(UserInteractionNode<T> *node, int order, InteractionType type, InteractionDirection direction) {
    Interaction<T> *result{};
    std::lock_guard<std::mutex> guard(mutex);
    auto interaction = std::make_unique<Interaction<T>>(node, nextId++, pageNumber, order, type, direction);
    result = interaction.get();
    if (!interaction->isCompleted()) {
      unresolvedInteractions.insert(interaction.get());
      updateState(ExecutionState::NEEDS_INTERACTION);
    }
    node->setInteraction(interaction.get());
    if (!node->processInputPortData(execution)) {
      return nullptr;
    }
    if (interaction->getDirection() == InteractionDirection::OUTPUT) {
      outputInteractionIds.insert(interactions.size());
    }
    interactions.push_back(std::move(interaction));

    interactionAdded(interactions.back().get());
    return result;
  }

  std::vector<Node *> updateAndNotify();

  void setExecutor(WorkflowExecutor *executor) {
    this->executor = executor;
  }

  void setExecution(WorkflowExecution *execution) {
    this->execution = execution;
  }

  void pageFinished() {
    pageNumber++;
  }

  bool hasUnresolvedInteractions() const;

protected:
  /**
   * This function is to be provided by implementors of InteractionManager.
   * It is called whenever a new interaction is created and can be used by
   * implementors to create a new storage unit in their persistent backend.
   * It may also do nothing if this is not necessary (depends on the storage
   * method)
   * @param interaction
   */
  virtual void interactionAdded(const AbstractInteraction *interaction) = 0;
  /**
   * This function is to be provided by implementors of InteractionManager.
   * It is responsible for updating the interaction values [from the data store],
   * and it must report back for which interactions there was an update.
   * This information is then used to notify the UserInteractionNodes which
   * registered the updated interactions (the "owners").
   * @return ids of the interactions for which the value was updated
   */
  virtual std::vector<unsigned int> updateValues() = 0;

  AbstractInteraction *getInteraction(unsigned int id);

  void processInteractions(const std::function<void(AbstractInteraction *)> &visitor);

  WorkflowExecution *execution;
  WorkflowExecutor *executor;
  std::mutex mutex;

private:
  void updateState(ExecutionState newState);
  void notifyInteraction(unsigned int index, std::vector<Node *> &nodesToExecute);

  std::vector<std::unique_ptr<AbstractInteraction>> interactions;
  std::unordered_set<AbstractInteraction *> unresolvedInteractions;
  std::unordered_set<unsigned int> outputInteractionIds;

  unsigned int nextId = 0;
  unsigned int pageNumber = 0;
};
