/* Copyright 2021 Karlsruhe Institute of Technology
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License. */

#pragma once

#include <nlohmann/json.hpp>

using json = nlohmann::json;

template<typename T>
class UserInteractionNode;
class WorkflowExecution;
class Node;

namespace t {
  struct CropSelection;
  void to_json(json &j, const CropSelection &c);
  void from_json(const json &j, CropSelection &c);
} // namespace t

enum class InteractionDirection {
  INPUT,
  OUTPUT
};

enum class InteractionType {
  FILE,
  STRING,
  INT,
  FLOAT,
  BOOL,
  CROP_IMAGES,
  CHOOSE,
  SELECT,
  WEB_VIEW,
  PERIODIC_TABLE,
  FORM
};

class AbstractInteraction {
public:
  AbstractInteraction(unsigned int id, unsigned int pageNumber, int order, InteractionType type, InteractionDirection direction, const std::string &description)
      : id(id), pageNumber(pageNumber), order(order), type(type), direction(direction),
        description(description), multiline(false), wasNotified(false) {
  }

  [[nodiscard]] unsigned int getId() const {
    return id;
  }

  [[nodiscard]] InteractionDirection getDirection() const {
    return direction;
  }

  [[nodiscard]] const std::string &getDescription() const {
    return description;
  }

  [[nodiscard]] virtual Node *getIssuingNode() const = 0;

  // common interface functions for all Interaction<T>
  [[nodiscard]] virtual json toJson() const = 0;
  virtual bool fromJson(json jsonObj) = 0; // has to return true iff the value(!) has changed
  virtual bool isCompleted() const = 0;
  virtual std::string getPath() const = 0;
  virtual void setPath(const std::string &path) = 0;
  virtual void setDescription(const std::string &description) = 0;
  virtual void setUrl(const std::string &url) = 0;
  virtual int getPageNumber() const = 0;
  virtual const std::vector<std::string> &getOptions() const = 0;
  virtual void setOptions(const std::vector<std::string> &options) = 0;
  virtual std::vector<Node *> notify(WorkflowExecution *execution) = 0;
  virtual void setValue(const json &value) = 0;
  virtual void setMultiline(bool enabled) = 0;

  static std::string typeToString(InteractionType type) {
    switch (type) {
      case InteractionType::FILE:
        return "file";
      case InteractionType::STRING:
        return "string";
      case InteractionType::INT:
        return "int";
      case InteractionType::FLOAT:
        return "float";
      case InteractionType::BOOL:
        return "bool";
      case InteractionType::CROP_IMAGES:
        return "cropImages";
      case InteractionType::CHOOSE:
        return "choose";
      case InteractionType::SELECT:
        return "select";
      case InteractionType::WEB_VIEW:
        return "webView";
      case InteractionType::PERIODIC_TABLE:
        return "periodicTable";
      case InteractionType::FORM:
        return "form";
    }
    throw std::invalid_argument("Invalid argument: Unsupported interaction type");
  }

  static InteractionType typeFromString(const std::string &string) {
    if (string == "file") {
      return InteractionType::FILE;
    } else if (string == "string") {
      return InteractionType::STRING;
    } else if (string == "int") {
      return InteractionType::INT;
    } else if (string == "float") {
      return InteractionType::FLOAT;
    } else if (string == "bool") {
      return InteractionType::BOOL;
    } else if (string == "cropImages") {
      return InteractionType::CROP_IMAGES;
    } else if (string == "choose") {
      return InteractionType::CHOOSE;
    } else if (string == "select") {
      return InteractionType::SELECT;
    } else if (string == "webView") {
      return InteractionType::WEB_VIEW;
    } else if (string == "periodicTable") {
      return InteractionType::PERIODIC_TABLE;
    } else if (string == "form") {
      return InteractionType::FORM;
    }
    throw std::invalid_argument("Invalid argument: Unknown interaction type: " + (string.empty() ? "(empty string)" : string));
  }

protected:
  unsigned int id;
  int pageNumber;
  int order;
  InteractionDirection direction;
  InteractionType type;
  std::string description;
  std::string path;
  std::vector<std::string> options;
  std::string url;
  bool multiline;
  bool wasNotified;

  static InteractionDirection directionFromString(const std::string &string) {
    if (string == "input") {
      return InteractionDirection::INPUT;
    } else if (string == "output") {
      return InteractionDirection::OUTPUT;
    }
    throw std::invalid_argument("Invalid argument: Unknown interaction direction: " + (string.empty() ? "(empty string)" : string));
  }
};
