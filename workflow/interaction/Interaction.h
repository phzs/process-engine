/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once

#include "AbstractInteraction.h"
#include <regex>
#include <string>

class WorkflowExecution;

template<class T>
class Interaction : public AbstractInteraction {
public:
  Interaction(UserInteractionNode<T> *issuingNode, unsigned int id, unsigned int pageNumber, int order, InteractionType type, InteractionDirection direction, const std::string &description = "")
      : AbstractInteraction(id, pageNumber, order, type, direction, description), issuingNode(issuingNode) {
  }

  [[nodiscard]] Node *getIssuingNode() const override {
    return issuingNode;
  }

  [[nodiscard]] json toJson() const override {
    json jsonObj;
    jsonObj["id"] = std::to_string(id);
    jsonObj["pageNumber"] = pageNumber;
    jsonObj["order"] = order;
    jsonObj["direction"] = direction == InteractionDirection::INPUT ? "input" : "output";
    jsonObj["type"] = typeToString(type);
    jsonObj["description"] = description;
    if (default_value) {
      json j = *default_value;
      jsonObj["default_value"] = j;
    }
    if (value) {
      jsonObj["value"] = *value;
    } else {
      jsonObj["value"] = nullptr; // creates "null"
    }
    if (!path.empty()) {
      jsonObj["url"] = path;
    }
    if (!options.empty()) {
      jsonObj["options"] = options;
    }
    if (!url.empty()) {
      jsonObj["url"] = url;
    }
    if (multiline) {
      jsonObj["multiline"] = multiline;
    }
    return jsonObj;
  }

  bool fromJson(json jsonObj) override {
    bool valueChanged = false;
    id = std::stoi(jsonObj["id"].get<std::string>());
    pageNumber = jsonObj["pageNumber"].get<int>();
    order = jsonObj["order"].get<int>();
    if (jsonObj["direction"].get<std::string>() == "input") {
      direction = InteractionDirection::INPUT;
    } else if (jsonObj["direction"].get<std::string>() == "output") {
      direction = InteractionDirection::OUTPUT;
    } else {
      throw std::logic_error("Parsing error: Unknown interaction direction " + jsonObj["direction"].get<std::string>());
    }
    type = typeFromString(jsonObj["type"].get<std::string>());
    description = jsonObj["description"].get<std::string>();
    if (jsonObj.contains("default_value")) {
      default_value = std::make_unique<T>(jsonObj["default_value"]);
    }
    if (jsonObj.contains("value") && !jsonObj["value"].is_null()) {
      if (!value || (json(*value) != jsonObj["value"])) { // if value was nullptr, there is a change in any case
        valueChanged = true;
      }
      value = std::make_unique<T>(jsonObj["value"]);
    }
    if (jsonObj.contains("url") && !jsonObj["url"].is_null()) {
      url = jsonObj["url"].get<std::string>();
    }
    if (jsonObj.contains("multiline") && !jsonObj["multiline"].is_null()) {
      multiline = jsonObj["multiline"].get<bool>();
    }
    return valueChanged;
  }

  [[nodiscard]] bool isCompleted() const override {
    return (value != nullptr);
  }

  void setDefaultValue(std::unique_ptr<T> defaultValue) {
    default_value = std::move(defaultValue);
  }

  const std::unique_ptr<T> &getDefaultValue() const {
    return default_value;
  }

  void setValue(const json &rawValue) override {
    Interaction::value = std::make_unique<T>(rawValue);
  }

  [[nodiscard]] std::string getPath() const override {
    return path;
  }

  void setPath(const std::string &path) override {
    Interaction::path = path;
  }

  void setDescription(const std::string &description) override {
    Interaction::description = description;
  }

  T *getValue() const {
    return value.get();
  }

  void setUrl(const std::string &url) override {
    Interaction::url = url;
  }

  [[nodiscard]] int getPageNumber() const override {
    return pageNumber;
  }

  [[nodiscard]] const std::vector<std::string> &getOptions() const override {
    return options;
  }

  void setOptions(const std::vector<std::string> &options) override {
    Interaction::options = options;
  }

  std::vector<Node *> notify(WorkflowExecution *execution) override {
    if (!wasNotified) {
      wasNotified = true;
      return issuingNode->onContinue(execution);
    }
    return {};
  }

  void setMultiline(bool enabled) override {
    multiline = enabled;
  }

private:
  std::unique_ptr<T> default_value;
  std::unique_ptr<T> value;
  UserInteractionNode<T> *issuingNode;
};
