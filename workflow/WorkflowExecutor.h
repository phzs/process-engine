/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once

#include "Workflow.h"
#include <ctpl_stl.h>

class BranchingNode;

class WorkflowExecutor {
public:
  explicit WorkflowExecutor(int nThreads = (int) std::thread::hardware_concurrency());
  ~WorkflowExecutor();

  bool execute(AbstractWorkflow *workflow, WorkflowExecution *execution);

private:
  void lock();
  void unlock();
  void notify();
  bool isThreadPoolIdling();

  // to add nodes to execute directly, does not call takeToken() and does not respect any dependencies
  void addNodesToExecute(const std::vector<Node *> &nodes);

  static bool isBranch(const AbstractWorkflow *workflow);

  // printing of log messages
  static void printExecutionStartMessage(const AbstractWorkflow *workflow, bool dryRun);
  static void printExecutionResult(AbstractWorkflow *workflow, const WorkflowExecution *execution);
  static void printCancelMessage(AbstractWorkflow *workflow);

  ctpl::thread_pool threadPool;
  std::condition_variable executorCV; // to get notified once a node was executed by a worker thread
  std::mutex executorMutex;           // to allow only one worker at a time to wake the (executor)
  std::mutex waitMutex;               // to allow only one worker at a time to wake the (executor)
  std::vector<Node *> nodesToExecute;

  friend class ProcessEngine;
  friend class InteractionManager;
  friend class TestInteractionManager;
};
