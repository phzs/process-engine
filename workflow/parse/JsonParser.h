/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once

#include "../Workflow.h"
#include <memory>
#include <string>

class JsonParser {
public:
  explicit JsonParser(std::string filename);
  std::unique_ptr<Workflow> parseJsonWorkflow();
  std::unordered_map<std::string, std::string> parseVariables();

private:
  std::unique_ptr<Port> parseJsonPort(json jsonPort, int listIndex);
  static int getNodeVerticalPosition(const json &jsonNode);
  static std::pair<bool, std::string> isProhibitedPortAllocation(const std::string &modelName, const Port *port);
  std::string filename;
  json jsonObj;
};
