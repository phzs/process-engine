/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "../nodes/FileInputNode.h"
#include "../nodes/SourceNode.h"
#include "../nodes/ToolNode.h"
#include "../nodes/control/VariableJsonNode.h"
#include "../nodes/control/VariableListNode.h"
#include "../nodes/control/VariableNode.h"
#include "../nodes/interaction/UserInputCropImagesNode.h"
#include "../nodes/interaction/UserInputFileNode.h"
#include "../nodes/interaction/UserInputIntegerNode.h"
#include "../nodes/interaction/UserInputTextNode.h"
#include "../nodes/interaction/UserOutputTextNode.h"
#include "../nodes/interaction/UserOutputWebViewNode.h"
#include "../nodes/source/BoolSourceNode.h"
#include "../nodes/source/FloatSourceNode.h"
#include "../nodes/source/IntegerSourceNode.h"
#include "../nodes/source/StringSourceNode.h"
#include "../ports/ParameterPort.h"
#include <fstream>
#include <iostream>
#include <nlohmann/json.hpp>
#include <utility>

#include "../../Logger.h"
#include "../../config.h"
#include "../errors/JsonParsingException.h"
#include "../nodes/EnvNode.h"
#include "../nodes/FileOutputNode.h"
#include "../nodes/FormatStringNode.h"
#include "../nodes/control/BranchSelectNode.h"
#include "../nodes/control/IfNode.h"
#include "../nodes/control/LoopNode.h"
#include "../nodes/interaction/UserInputBoolNode.h"
#include "../nodes/interaction/UserInputChooseNode.h"
#include "../nodes/interaction/UserInputFloatNode.h"
#include "../nodes/interaction/UserInputFormNode.h"
#include "../nodes/interaction/UserInputPeriodicTableNode.h"
#include "../nodes/interaction/UserInputSelectBoundingBoxNode.h"
#include "../nodes/interaction/UserInputSelectNode.h"
#include "JsonParser.h"

using json = nlohmann::json;

JsonParser::JsonParser(std::string filename)
    : filename(std::move(filename)) {
  std::ifstream in(JsonParser::filename);
  in >> jsonObj;
}

std::unique_ptr<Workflow> JsonParser::parseJsonWorkflow() {

  auto workflow = std::make_unique<Workflow>();

  for (auto jsonNode : jsonObj["nodes"]) {
    std::unique_ptr<Node> node;
    auto nodeId = jsonNode["id"].get<std::string>();
    json model = jsonNode["model"];
    int nodeVerticalPosition = getNodeVerticalPosition(jsonNode);

    if (!model.empty()) {
      auto model_name = model["name"].get<std::string>();
      json tool = model["tool"];
      json modelValue = model["value"];
      std::string value;

      if (model_name == "ToolNode" || model_name == "EnvNode") {
        std::unique_ptr<ToolNode> dynamicNode;
        if (model_name == "EnvNode") {
          dynamicNode = std::make_unique<EnvNode>(nodeId);
        } else {
          dynamicNode = std::make_unique<ToolNode>(nodeId);
        }
        auto tool_path = model["tool"]["path"].get<std::string>();
        dynamicNode->setToolPath(tool_path);
        int listIndex = 0;
        for (const auto &jsonPort : model["tool"]["ports"]) {
          std::unique_ptr<Port> parsedPort = parseJsonPort(jsonPort, listIndex++);
          std::pair<bool, std::string> isProhibited = isProhibitedPortAllocation(model_name, parsedPort.get());
          if (!isProhibited.first) {
            dynamicNode->addPort(std::move(parsedPort));
          } else {
            LOG("Warning: Prohibited port assignment for node " << nodeId << " ignored with reason: " << isProhibited.second);
          }
        }
        node = std::move(dynamicNode);
      } else if (model_name == "FileOutput") {
        std::string outputFilePath, inputFilePath;
        if (!model["outputFilePath"].empty()) {
          outputFilePath = model["outputFilePath"].get<std::string>();
        }
        if (!model["inputFilePath"].empty()) {
          inputFilePath = model["inputFilePath"].get<std::string>();
        }
        bool createShortcut = false;
        if (!model["createShortcut"].empty()) {
          createShortcut = model["createShortcut"].get<bool>();
        }
        node = std::make_unique<FileOutputNode>(nodeId, outputFilePath, inputFilePath, createShortcut);
      } else if (model_name == "FileInput") {
        std::string path;
        node = std::make_unique<FileInputNode>(nodeId);
      } else if (model_name == "UserInputText") {
        std::string description, defaultValue;
        if (!model["prompt_text"].empty()) {
          description = model["prompt_text"];
        }
        if (!model["default_value"].empty()) {
          defaultValue = model["default_value"];
        }
        node = std::make_unique<UserInputTextNode>(nodeId, description, defaultValue, nodeVerticalPosition);
      } else if (model_name == "UserInputFile") {
        std::string description, defaultValue;
        if (!model["prompt_text"].empty()) {
          description = model["prompt_text"];
        }
        node = std::make_unique<UserInputFileNode>(nodeId, description, nodeVerticalPosition);
      } else if (model_name == "UserInputCropImages") {
        // UserInputCropImages is deprecated
        std::string description, url;
        if (!model["prompt_text"].empty()) {
          description = model["prompt_text"];
        }
        // the attribute "url" (the path to the image or image stack) is not necessary for the engine, so ignore it here
        node = std::make_unique<UserInputCropImagesNode>(nodeId, description, nodeVerticalPosition);
      } else if (model_name == "UserInputSelectBoundingBox") {
        std::string description, url;
        if (!model["prompt_text"].empty()) {
          description = model["prompt_text"];
        }
        // the attribute "path" is not necessary for the engine, so ignore it here
        node = std::make_unique<UserInputSelectBoundingBoxNode>(nodeId, description, nodeVerticalPosition);
      } else if (model_name == "UserOutputWebView") {
        std::string description, url, defaultValue;
        if (!model["prompt_text"].empty()) {
          description = model["prompt_text"];
        }
        if (!model["url"].empty()) {
          url = model["url"];
        }
        node = std::make_unique<UserOutputWebViewNode>(nodeId, description, url, nodeVerticalPosition);
      } else if (model_name == "UserInputInteger") {
        std::string description;
        if (!model["prompt_text"].empty()) {
          description = model["prompt_text"];
        }
        node = std::make_unique<UserInputIntegerNode>(nodeId, description, nodeVerticalPosition);
      } else if (model_name == "UserInputPeriodicTable") {
        std::string description;
        if (!model["prompt_text"].empty()) {
          description = model["prompt_text"];
        }
        node = std::make_unique<UserInputPeriodicTableNode>(nodeId, description, nodeVerticalPosition);
      } else if (model_name == "UserInputFloat") {
        std::string description;
        if (!model["prompt_text"].empty()) {
          description = model["prompt_text"];
        }
        node = std::make_unique<UserInputFloatNode>(nodeId, description, nodeVerticalPosition);
      } else if (model_name == "UserInputForm") {
        node = std::make_unique<UserInputFormNode>(nodeId, nodeVerticalPosition);
      } else if (model_name == "UserInputBool") {
        std::string description;
        if (!model["prompt_text"].empty()) {
          description = model["prompt_text"];
        }
        node = std::make_unique<UserInputBoolNode>(nodeId, description, nodeVerticalPosition);
      } else if (model_name == "UserInputChoose") {

        unsigned int numberOfOptions = 5;
        if (model.contains("nOptions")) {
          numberOfOptions = model["nOptions"].get<unsigned int>();
        }
        std::string description;
        if (!model["prompt_text"].empty()) {
          description = model["prompt_text"];
        }
        node = std::make_unique<UserInputChooseNode>(nodeId, description, nodeVerticalPosition, numberOfOptions);
      } else if (model_name == "UserInputSelect") {
        std::string description;
        if (!model["prompt_text"].empty()) {
          description = model["prompt_text"];
        }
        node = std::make_unique<UserInputSelectNode>(nodeId, description, nodeVerticalPosition);
      } else if (model_name == "UserOutputText") {
        std::string inputFile;
        if (!model["inputFile"].empty()) {
          inputFile = model["inputFile"];
        }
        node = std::make_unique<UserOutputTextNode>(nodeId, inputFile, nodeVerticalPosition);
      }
      /* only add source nodes if they contain a value other than null */
      else if (model_name == "String") {
        if (!modelValue.is_null()) {
          value = modelValue.get<std::string>();
        }
        node = std::make_unique<StringSourceNode>(nodeId, value);
      } else if (model_name == "Boolean") {
        value = "false";
        if (!modelValue.is_null()) {
          if (modelValue.is_boolean()) {
            value = modelValue.get<bool>() ? config::TRUE_STRING_REPR : config::FALSE_STRING_REPR;
          } else {
            value = modelValue.get<std::string>();
          }
        }
        node = std::make_unique<BoolSourceNode>(nodeId, value);
      } else if (model_name == "Float") {
        value = "0.0";
        if (!modelValue.is_null()) {
          // note: modelValue.is_number_float() would be more strict
          value = modelValue.is_number() ? std::to_string(modelValue.get<float>()) : modelValue.get<std::string>();
        }
        node = std::make_unique<FloatSourceNode>(nodeId, value);
      } else if (model_name == "Integer") {
        value = "0";
        if (!modelValue.is_null()) {
          value = modelValue.is_number_integer() ? std::to_string(modelValue.get<int>()) : modelValue.get<std::string>();
        }
        node = std::make_unique<IntegerSourceNode>(nodeId, value);
      }
      /* control nodes */
      else if (model_name == "Variable") {
        node = std::make_unique<VariableNode>(nodeId);
      } else if (model_name == "VariableJson") {
        node = std::make_unique<VariableJsonNode>(nodeId);
      } else if (model_name == "VariableList") {
        node = std::make_unique<VariableListNode>(nodeId);
      } else if (model_name == "IfBranch") {
        node = std::make_unique<IfNode>(nodeId);
      } else if (model_name == "Loop") {
        node = std::make_unique<LoopNode>(nodeId);
      } else if (model_name == "BranchSelect") {
        unsigned int numberOfBranches = BranchSelectNode::defaultNumberOfBranches;
        if (model.contains("nBranches")) {
          numberOfBranches = model["nBranches"].get<unsigned int>();
        }
        node = std::make_unique<BranchSelectNode>(nodeId, numberOfBranches);
      } else if (model_name == "FormatString") {
        unsigned int numberOfInputs = FormatStringNode::defaultNumberOfInputs;
        if (model.contains("nInputs")) {
          numberOfInputs = model["nInputs"].get<unsigned int>();
        }
        node = std::make_unique<FormatStringNode>(nodeId, numberOfInputs, modelValue.get<std::string>());
      } else if (model_name == "Note") {
        continue; // nothing to do (Notes have no effect on workflow execution and can be ignored)
      }
      // add new nodes to parse here
      else {
        throw std::invalid_argument("Unknown node model: " + model_name);
      }

      // add execution profile for all nodes of type ExecutableNode
      auto *executableNode = dynamic_cast<ExecutableNode *>(node.get());
      if (executableNode) {
        if (model.contains("executionProfile")) {
          executableNode->setExecutionProfile(model["executionProfile"].get<ExecutionProfile>());
        } else {
          LOG("Warning: Missing execution profile for node " << node->getId() << ", assuming default");
        }
      }

      if (jsonNode.contains("position") && jsonNode["position"].contains("x")) {
        node->setOrder(jsonNode["position"]["x"]);
      }
      workflow->addNode(std::move(node));
    }
  }

  for (const auto &jsonConnection : jsonObj["connections"]) {
    if (!jsonConnection.empty()) {
      // terminology: "in" refers to the port/node where the connection goes in, not the port type
      auto right_nodeId = jsonConnection["in_id"].get<std::string>();
      int right_PortIndex = jsonConnection["in_index"].get<int>();
      auto left_nodeId = jsonConnection["out_id"].get<std::string>();
      int left_PortIndex = jsonConnection["out_index"].get<int>();

      workflow->connectPorts(left_nodeId, left_PortIndex, right_nodeId, right_PortIndex, false);
    }
  }
  return workflow;
}

std::unique_ptr<Port> JsonParser::parseJsonPort(json jsonPort, int listIndex) {
  int index = jsonPort["port_index"].get<int>();
  auto name = jsonPort["name"].get<std::string>();
  std::string shortName;
  bool positional = false;
  if (jsonPort.contains("shortName") && !jsonPort["shortName"].is_null()) {
    shortName = jsonPort["shortName"].get<std::string>();
  }
  if (jsonPort.contains("positional") && jsonPort["positional"].is_boolean()) {
    positional = jsonPort["positional"].get<bool>();
  }
  auto type = jsonPort["type"].get<std::string>();
  auto required = jsonPort["required"].get<bool>();

  // parse data
  bool dataAvailable = false;

  std::string string_data;
  bool bool_data;
  double double_data;
  int int_data;
  json dataJson = jsonPort["value"];
  if (!dataJson.empty()) {
    dataAvailable = true;
    if (type == "flag") {
      if (dataJson.type() == json::value_t::boolean) {
        bool_data = dataJson.get<bool>();
      } else {
        throw JsonParsingException("Type mismatch: Data has unexpected type (expected bool)", filename, dataJson);
      }
    } else if (type == "real") {
      if (dataJson.type() == json::value_t::number_float || dataJson.type() == json::value_t::number_unsigned) {
        double_data = dataJson.get<float>();
      } else {
        throw JsonParsingException("Type mismatch: Data has unexpected type (expected float)", filename, dataJson);
      }
    } else if (type == "long") {
      if (dataJson.type() == json::value_t::number_integer || dataJson.type() == json::value_t::number_unsigned) {
        int_data = dataJson.get<int>();
      } else {
        throw JsonParsingException("Type mismatch: Data has unexpected type (expected int)", filename, dataJson);
      }
    } else if (dataJson.type() == json::value_t::string) {
      string_data = dataJson.get<std::string>();
    } else {
      throw JsonParsingException("Type mismatch: Data has unexpected type)", filename, dataJson);
    }
  }

  // parse the port direction
  PortDirection direction;
  json directionJson = jsonPort["port_direction"];
  if (directionJson.empty()) {
    throw JsonParsingException("Invalid input: Port without direction", filename, jsonPort);
  } else if (directionJson.get<std::string>() == "in") {
    direction = PortDirection::In;
  } else if (directionJson.get<std::string>() == "out") {
    direction = PortDirection::Out;
  } else {
    throw JsonParsingException("Invalid input: Unknown port direction", filename, jsonPort);
  }

  // create the port object
  if (type == "dependency") {
    return std::make_unique<DependencyPort>(index, direction);
  }
  if (type == "pipe") {
    return std::make_unique<PipePort>(index, direction);
  }
  if (type == "env") {
    return std::make_unique<EnvPort>(index, direction);
  }
  // note: the listIndex is used as the argumentPosition (it was a separate json attribute 'position' before)
  auto parameterPort = std::make_unique<ParameterPort>(index, listIndex, name, shortName, type, direction, required);
  if (type == "flag") {
    if (dataAvailable) {
      parameterPort->setData(std::make_unique<StaticData>(bool_data));
    }
  } else if (type == "real") {
    if (dataAvailable) {
      parameterPort->setData(std::make_unique<StaticData>(double_data));
    }
  } else if (type == "long") {
    if (dataAvailable) {
      parameterPort->setData(std::make_unique<StaticData>(int_data));
    }
  } else { // treat everything else as string data for now
    parameterPort = std::make_unique<ParameterPort>(index, listIndex, name, shortName, type, direction, required, positional);
    if (dataAvailable) {
      parameterPort->setData(std::make_unique<StaticData>(string_data));
    }
  }
  return parameterPort;
}

int JsonParser::getNodeVerticalPosition(const json &jsonNode) {
  return jsonNode["position"]["y"].get<int>();
}

std::pair<bool, std::string> JsonParser::isProhibitedPortAllocation(const std::string &modelName, const Port *port) {
  if (!port) throw std::invalid_argument("Port is null");
  if (modelName == "EnvNode" && dynamic_cast<const DependencyPort *>(port)) {
    return {true, "EnvNodes may not have dependency ports (ingoing or outgoing)"};
  }
  if (modelName == "EnvNode" && dynamic_cast<const EnvPort *>(port) && port->getDirection() == PortDirection::In) {
    return {true, "EnvNodes may not have ingoing ports of type env. They can provide environments but can not run in an environment."};
  }
  if (modelName == "ToolNode" && dynamic_cast<const EnvPort *>(port) && port->getDirection() == PortDirection::Out) {
    return {true, "ToolNodes may not have outgoing ports of type env. Only EnvNodes can provide environments."};
  }
  return {false, {}};
}

std::unordered_map<std::string, std::string> JsonParser::parseVariables() {
  std::unordered_map<std::string, std::string> result;
  if (!jsonObj.contains("variables")) return result;
  for (const auto &jsonVariable : jsonObj["variables"]) {
    if (jsonVariable.contains("name") && jsonVariable.contains("value")) {
      result[jsonVariable["name"].get<std::string>()] = jsonVariable["value"].get<std::string>();
    }
  }
  return result;
}
