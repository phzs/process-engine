/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once

#include "NodeStatisticsInterface.h"
#include <string>
#include <unordered_map>
#include <unordered_set>

class WorkflowExecution;
class Node;
class NodeState;

/** Interface for Workflows or WorkflowBranches */
class AbstractWorkflow : public NodeStatisticsInterface {

public:
  virtual ~AbstractWorkflow() = default;

  [[nodiscard]] virtual Node *getNode(const std::string &nodeId) const = 0;
  [[nodiscard]] virtual const Node *getNodeConst(const std::string &nodeId) const = 0;

  virtual std::unordered_map<std::string, Node *> getNodes() = 0;
  [[nodiscard]] virtual std::unordered_map<std::string, const Node *> getNodesConst() const = 0;

  [[nodiscard]] virtual bool isEmpty() const = 0;
  [[nodiscard]] virtual bool areCoherentGroupsInitialized() const = 0;

  [[nodiscard]] virtual std::unordered_set<std::string> getNodeIds() const = 0;

  [[nodiscard]] virtual std::string getIdentifier() const = 0;

  /**
   * Internally initializes the coherent groups for all SystemCommandNodes.
   * Must be called before generating the execution order (and before executing the workflow).
   * Should be called after all nodes and connections were added, to avoid the need to reinitialize the groups.
   */
  virtual void initCoherentGroups() = 0;

  /**
   * Identifies and initializes branches internally.
   * Iterates through all BranchingNodes and give them opportunity to initialize their branches and register
   * them with the execution.
   */
  virtual void initBranches();

  unsigned int getNodesProcessed() const override;
  unsigned int getNodesProcessedInLoop() const override;
  unsigned int getNodesTotal() const override;

  void setNodeState(const std::string &nodeId, NodeState state) const;
  void resetNodeStates();
  void skipUnterminatedNodes();
};
