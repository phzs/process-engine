/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "DataPort.h"
#include "../../Logger.h"

DataPort::DataPort(int index, PortDirection direction)
    : Port(index, direction) {
}

void DataPort::setData(std::shared_ptr<StaticData> data) {
  this->data = std::move(data);
}

std::shared_ptr<StaticData> DataPort::getData() const {
  if (direction == PortDirection::In) {
    auto otherDataPort = getConnectedDataPort();
    if (otherDataPort) {
      return otherDataPort->getData();
    }
  }
  return data;
}

const DataPort *DataPort::getConnectedDataPort() const {
  if (!connectedPorts.empty()) {
    auto otherPort = connectedPorts[0];
    if (otherPort) {
      auto otherDataPort = dynamic_cast<const DataPort *>(otherPort);
      if (otherDataPort) {
        return otherDataPort;
      } else {
        LOG_NODE("Warning: Can not access connected data port " << getNodeId());
      }
    }
  }
  return nullptr;
}

void DataPort::addConnectedPort(Port *connectedPort) {
  Port::addConnectedPort(connectedPort);

  // additionally, get a shared pointer to the data of the connected port, in case this is an input port
  if (direction == PortDirection::In) {
    auto otherDataPort = getConnectedDataPort();
    if (otherDataPort) {
      data = otherDataPort->getData();
    }
  }
}

void DataPort::updateData() {
  if (!connectedPorts.empty()) {
    if (connectedPorts.size() > 1) {
      LOG_NODE("Warning: Invalid number of ingoing connections in Parameter Port");
    }
    auto otherPort = connectedPorts[0];
    if (otherPort) {
      auto otherDataPort = dynamic_cast<const DataPort *>(otherPort);
      if (otherDataPort) {
        data = otherDataPort->getData();
      } else {
        LOG_NODE("Warning: Can not access connected data port " << getNodeId());
      }
    }
  }
}
