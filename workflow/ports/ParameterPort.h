/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once

#include "../StaticData.h"
#include "DataPort.h"
#include "Port.h"
#include <string>

class WorkflowExecution;

class ParameterPort : public DataPort {

public:
  ParameterPort(int index, int argumentPosition, std::string name, std::string shortName,
                std::string type, PortDirection direction, bool required, bool positional = false);
  ~ParameterPort() override = default;

  std::string getCommandLineArgument(const WorkflowExecution *execution);
  bool isSet() const;              // handles if this argument should be added to the command line call
  int getArgumentPosition() const; // position of the argument when calling the binary
  const std::string &getType() const;

private:
  static bool isQuoted(const std::string &str);

  int argumentPosition;
  std::string name;
  std::string shortName;
  std::string type;
  bool required;
  bool positional;
};
