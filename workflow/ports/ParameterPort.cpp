/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include <utility>

#include "../../Logger.h"
#include "../../workflow/Workflow.h"
#include "ParameterPort.h"

ParameterPort::ParameterPort(int index, int argumentPosition, std::string name, std::string shortName,
                             std::string type, PortDirection direction, bool required, bool positional)
    : DataPort(index, direction), name(std::move(name)), shortName(std::move(shortName)), type(std::move(type)),
      argumentPosition(argumentPosition), required(required), positional(positional) {}

std::string ParameterPort::getCommandLineArgument(const WorkflowExecution *execution) {
  std::string result;

  // build the identifier string (e.g. -n, --name) for non-positional arguments
  if (!positional) {
    if (!shortName.empty()) {
      result = "-" + shortName;
    } else if (!name.empty() && (name.rfind("arg", 0) != 0)) {
      result = "--" + name;
    }
  }

  // obtain incoming data if possible (this is only an update: the DataPort does this once when the ports are connected
  // but if the data changes since, it is not updated -> TODO put this into something like DataPort::updateData and call it here
  updateData();

  // now add the argument after the identifier (does not apply to flags!)
  // e.g. --name <argument>, or just <argument> for positional arguments
  if (data && !data->isEmpty() && type != "flag") {
    std::string argument = execution->expandVariables(data->toString());
    if (!isQuoted(argument)) {
      if (argument.find('\"') != std::string::npos) {
        result += " \'" + argument + "\'";
      } else {
        result += " \"" + argument + "\"";
      }
    } else {
      result += " " + argument;
    }
  }

  return result;
}

bool ParameterPort::isSet() const {
  if (data && !data->isEmpty()) {
    if (type == "flag") {
      return data->toBoolean();
    } else {
      return true;
    }
  }
  return false;
}

int ParameterPort::getArgumentPosition() const {
  return argumentPosition;
}

const std::string &ParameterPort::getType() const {
  return type;
}

bool ParameterPort::isQuoted(const std::string &str) {
  const char first = *str.begin();
  const char last = *str.rbegin();
  return (first == '"' && last == '"') || (first == '\'' && last == '\'');
}
