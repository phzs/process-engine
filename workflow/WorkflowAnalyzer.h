/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once

#include "CoherentNodeGroup.h"
#include "WorkflowBranch.h"
#include <set>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

class Workflow;
class WorkflowNodeStates;
class AbstractWorkflow;

class WorkflowAnalyzer {

public:
  explicit WorkflowAnalyzer(AbstractWorkflow *workflow);

  /**
   * Looks at a specific node and identifies coherent nodes, based on it's outgoing and
   * incoming connections of all ports that are subtypes of CoherentPort.
   * Returns an object of CoherentNodeGroup, which encapsulates information and functionality
   * related to a coherent group of nodes.
   * @param nodeId Id of the node for which coherent nodes should be identified.
   * @return Pointer to a CoherentNodeGroup object.
   */
  std::shared_ptr<CoherentNodeGroup> findCoherentNodeGroup(const std::string &nodeId) const;

  /**
   * Find a branch which starts on the node with the given id on the specified output port.
   * Return all node ids of nodes belonging to this branch,
   * or an empty set if no branch was found.
   * The algorithm scans for nodes connected to the specified port and their "dependents" recursively, until it finds no further dependents (a dead-end)
   * In the second case, the node which exitCondition returned true on will still be included in the result.
   *
   * @param parent An non-const pointer to the parent workflow or branch (AbstractWorkflow*)
   * @param startNodeId NodeId of the node where the branch search should start
   * @param startPortIndex PortIndex of the port on the start node where the search should start
   * @return A WorkflowBranch object with ownership to it
   */
  std::unique_ptr<WorkflowBranch>
  findBranch(AbstractWorkflow *parent, BranchingNode *originNode, int startPortIndex);

  static std::vector<std::string> getStartNodes(const std::unordered_map<std::string, const Node *> &nodes, WorkflowBranch *branch = nullptr);

private:
  /**
   * Returns a map containing the branchIds (as key) and the recursionDepth (value) at which they were found.
   */
  std::unordered_map<Node *, unsigned int> findBranchRecursively(Node *node,
                                                                 unsigned int currentRecursionDepth = 0);

  const AbstractWorkflow *workflow;

  // the following member works as a cache for the const Node pointers of the workflow
  // it will be initialized once in the constructor and will never be updated
  std::unordered_map<std::string, const Node *> _nodes;
};
