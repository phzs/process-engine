/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "SourceNode.h"

SourceNode::SourceNode(const std::string &id)
    : Node(id) {
  // port specification must be done in child classes
}

bool SourceNode::execute(WorkflowExecution *) {
  return true; // nothing to do for source nodes
}

NodeState SourceNode::getInitialState() const {
  return NodeState(NodeExecutionState::NOOP, 0);
}
