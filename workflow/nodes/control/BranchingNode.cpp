/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "BranchingNode.h"
#include "../../Workflow.h"
#include "../../WorkflowAnalyzer.h"
#include "../../ports/BranchingPort.h"

BranchingNode::BranchingNode(const std::string &id)
    : ExecutableNode(id), branchesInitialized(false) {
}

void BranchingNode::initBranches() {
  WorkflowAnalyzer analyzer(workflow);
  for (const auto &item : ports_out) {
    int outgoingPortIndex = item.first;
    auto branchingPort = dynamic_cast<BranchingPort *>(item.second.get());
    if (branchingPort) {
      if (!branchesInitialized) {
        auto branch = analyzer.findBranch(workflow, this, outgoingPortIndex);
        branches.push_back(branch.get());
        branchingPort->setBranch(std::move(branch));
      } else {
        // the port already has its branch assigned, but the execution (sub execution) still needs the registration
        auto branchPtr = branchingPort->getBranch();
        if (!branchPtr) throw std::logic_error("Branches of BranchingPort not initialized correctly or invalidated");
      }
    }
  }
  branchesInitialized = true;
}

std::vector<Node *> BranchingNode::getBranchStartNodes(const BranchingPort *branchingPort) const {
  std::vector<Node *> result;
  for (Port *port : branchingPort->getConnectedPorts()) {
    result.push_back(port->getNode());
  }

  /* Also, (separately) add nodes that are "in branch dependents", which means they are in the branch
   * and have a value dependency to their LoopNode (e.g. to the outgoing index port of LoopNode */
  for (Node *node : getInBranchValueDependents(branchingPort)) {
    result.push_back(node);
  }
  return result;
}

std::unordered_set<Node *> BranchingNode::getInBranchValueDependents(const BranchingPort *branchingPort) const {
  std::unordered_set<Node *> result;
  const std::unordered_set<std::string> &branchNodes = branchingPort->getBranch()->getNodeIds();

  processPortsOfType<Port>(PortDirection::Out, [branchNodes, &result](Port *port) {
    if (dynamic_cast<DependencyPort *>(port)) {
      return; // iterate only non-dependency ports to get only nodes which depend on a value of this node!
    }
    for (Port *connectedPort : port->getConnectedPorts()) {
      Node *connectedNode = connectedPort->getNode();
      if (branchNodes.find(connectedNode->getId()) != branchNodes.end()) {
        result.insert(connectedNode);
      }
    }
  });
  return result;
}

std::vector<Node *> BranchingNode::takeBranchToken(WorkflowExecution *execution) {
  if (!state.takeBranchToken()) {
    throw std::logic_error("Invalid attempt to take branch token");
  }
  if (state.branchTokenCount() == 0) {
    // branch has finished, lets return the direct dependents of this node, but without nodes which are contained in the branch
    // (we don't want to start the branch again (partly) because it was executed already)
    std::unordered_set<Node *> directDependents = getDirectDependentsExcludingBranch(execution->getNodeState(getId()).getActiveBranch());
    std::vector<Node *> result;
    result.insert(result.begin(), directDependents.begin(), directDependents.end());

    if (branchParent && getDirectDependentsWithBranchParent(branchParent).empty()) {
      // since this is a "branchEnd" node, we need to "notify" the parent (take a branchToken from it and return the nodes ourselves)
      const std::vector<Node *> fromBranchParent = branchParent->takeBranchToken(execution);
      util::joinVector(result, fromBranchParent);
    }
    return result;
  }
  return {};
}

std::vector<Node *> BranchingNode::nodeFinished(WorkflowExecution *execution) {
  state.setCurrentState(NodeExecutionState::EXECUTED);
  std::vector<Node *> result;
  WorkflowBranch *activeBranch = execution->getNodeState(getId()).getActiveBranch();
  std::vector<Node *> branchStartNodes = getBranchStartNodes(activeBranch->getOriginPort());
  // note: we can not return the start nodes directly, otherwise they will be executed without respecting any dependencies / tokens
  // instead, call takeToken() on them and return the results
  for (Node *branchStartNode : branchStartNodes) {
    util::joinVector(result, branchStartNode->takeToken(execution));
  }

  // if the active branch is empty, we need to make sure that takeBranchToken() finally executes and takes a token
  // (this would be done by the nodes in the branch otherwise)
  if (activeBranch->isEmpty()) {
    state.setBranchTokens(1);
    util::joinVector(result, takeBranchToken(execution));
  }
  return result;
}

unsigned int BranchingNode::getNodesProcessed() const {
  unsigned int result = Node::getNodesProcessed();
  if (WorkflowBranch *branch = getActiveOrLargestBranch()) {
    result += branch->getNodesProcessed();
  }
  return result;
}

unsigned int BranchingNode::getNodesProcessedInLoop() const {
  unsigned int result = Node::getNodesProcessedInLoop();
  if (WorkflowBranch *branch = getActiveOrLargestBranch()) {
    result += branch->getNodesProcessedInLoop();
  }
  return result;
}

unsigned int BranchingNode::getNodesTotal() const {
  unsigned int result = Node::getNodesTotal();
  if (WorkflowBranch *branch = getActiveOrLargestBranch()) {
    result += branch->getNodesTotal();
  }
  return result;
}

WorkflowBranch *BranchingNode::getActiveOrLargestBranch() const {
  if (WorkflowBranch *activeBranch = state.getActiveBranch()) {
    return activeBranch;
  }
  auto largestBranch = std::max_element(branches.begin(), branches.end(), [](WorkflowBranch *lhs, WorkflowBranch *rhs) {
    return lhs->getNodesTotal() < rhs->getNodesTotal();
  });
  if (largestBranch != branches.end()) {
    return *largestBranch;
  }
  return {};
}

void BranchingNode::skip() {
  Node::skip();

  for (WorkflowBranch *branch : branches) {
    branch->skip();
  }
}
