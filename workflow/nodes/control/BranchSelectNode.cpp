/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "BranchSelectNode.h"
#include "../../../Logger.h"
#include "../../WorkflowExecutor.h"

BranchSelectNode::BranchSelectNode(const std::string &id, unsigned int numberOfBranches)
    : BranchingNode(id), name("BranchSelect") {

  // inputs
  createPort<DependencyPort>(0, PortDirection::In);
  selectPort = createPort<DataPort>(1, PortDirection::In);

  // outputs
  createPort<DependencyPort>(0, PortDirection::Out);
  for (int i = 1; i <= numberOfBranches; i++) {
    branchingPorts.push_back(createPort<BranchingPort>(i));
  }
}

bool BranchSelectNode::execute(WorkflowExecution *execution) {
  if (!processInputPortData(execution)) return false;
  WorkflowBranch *selectedBranch;
  state.setCurrentState(NodeExecutionState::RUNNING);
  if (selectPort->getData()) {
    unsigned int selectedBranchNumber = selectPort->getData()->toInt();
    selectedBranchNumber -= 1; // in the workflow editor, the numbering (of branches) starts from 1
    if (selectedBranchNumber < 0 || selectedBranchNumber >= branchingPorts.size()) {
      LOG_NODE("There is no branch with the number " << selectedBranchNumber + 1);
      state.setCurrentState(NodeExecutionState::ERROR);
      return false;
    }
    selectedBranch = branchingPorts[selectedBranchNumber]->getBranch();
    execution->getNodeState(getId()).setActiveBranch(selectedBranch);
    execution->getNodeState(getId()).setBranchTokens((int) selectedBranch->getBranchTokens());
    LOG_NODE("Selected branch: " << selectedBranchNumber + 1 << " (skipping other branches)");

    if (!selectedBranch) {
      LOG_NODE("Unable to execute the selected branch");
      return false;
    }

    for (int i = 0; i < branchingPorts.size(); i++) {
      if (!branchingPorts[i]) continue;
      auto branch = branchingPorts[i]->getBranch();
      if (branch && i != selectedBranchNumber) {
        branch->skip();
      }
    }
  }
  if (!processOutputPortData(execution)) return false;
  return true;
}

const std::string &BranchSelectNode::getName() const {
  return name;
}
