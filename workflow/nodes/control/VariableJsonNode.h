/* Copyright 2022 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once

#include "../ExecutableNode.h"

class DataPort;

class VariableJsonNode : public ExecutableNode {
public:
  explicit VariableJsonNode(const std::string &id);
  ~VariableJsonNode() override = default;

  bool execute(WorkflowExecution *execution) override;
  const std::string &getName() const override;

private:
  void setVariables(WorkflowExecution *execution, const json::array_t &json_array);
  static std::string valueToString(const json &value);

  const std::string name;

  DataPort *jsonPort;
  DataPort *keyPort;
};
