/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "VariableNode.h"
#include "../../ports/DependencyPort.h"
#include "../../workflow/Workflow.h"

VariableNode::VariableNode(const std::string &id)
    : ExecutableNode(id), name("StringVariable") {
  createPort<DependencyPort>(0, PortDirection::In);
  createPort<DependencyPort>(0, PortDirection::Out);

  namePort = createPort<DataPort>(1, PortDirection::In);
  valuePort = createPort<DataPort>(2, PortDirection::In);
}

const std::string &VariableNode::getName() const {
  return name;
}

bool VariableNode::execute(WorkflowExecution *execution) {
  if (!processInputPortData(execution)) return false;
  state.setCurrentState(NodeExecutionState::RUNNING);
  auto name_data = namePort->getData();
  auto value_data = valuePort->getData();
  bool result = false;
  if (name_data) {
    std::string value;
    if (value_data) {
      value = value_data->toString();
    }
    execution->setVariable(name_data->toString(), value);
    result = true;
  }
  if (result) {
    state.setCurrentState(NodeExecutionState::EXECUTED);
  } else {
    state.setCurrentState(NodeExecutionState::ERROR);
    LOG_NODE("Error setting variable: name is required!");
    return false;
  }
  return processOutputPortData(execution);
}
