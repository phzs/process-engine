/* Copyright 2022 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "VariableListNode.h"
#include "../../../Logger.h"
#include "../../../util.h"
#include "../../ports/DependencyPort.h"
#include "../../workflow/Workflow.h"

VariableListNode::VariableListNode(const std::string &id)
    : ExecutableNode(id), name("VariableList") {
  createPort<DependencyPort>(0, PortDirection::In);
  createPort<DependencyPort>(0, PortDirection::Out);

  namePort = createPort<DataPort>(1, PortDirection::In);
  valuePort = createPort<DataPort>(2, PortDirection::In);
  delimiterPort = createPort<DataPort>(3, PortDirection::In);
}

const std::string &VariableListNode::getName() const {
  return name;
}

bool VariableListNode::execute(WorkflowExecution *execution) {
  if (!processInputPortData(execution)) return false;
  state.setCurrentState(NodeExecutionState::RUNNING);
  auto name_data = namePort->getData();
  auto value_data = valuePort->getData();
  bool result = false;
  if (name_data) {
    std::vector<std::string> value_vector;
    std::vector<std::string> name_vector;
    std::string delimiter = ";";
    if (delimiterPort->getData()) {
      delimiter = execution->expandVariables(delimiterPort->getData()->toString());
    }
    if (value_data) {
      value_vector = util::splitString(value_data->toString(), delimiter);
      name_vector = util::splitString(name_data->toString(), delimiter);
    }
    if (name_vector.size() != value_vector.size()) {
      LOG_NODE("Error setting variable: number of names does not match number of values!");
    } else {
      for (int i = 0; i < name_vector.size(); i++) {
        execution->setVariable(name_vector[i], value_vector[i]);
      }
      result = true;
    }
  } else {
    LOG_NODE("Error setting variable: name is required!");
  }
  if (result) {
    state.setCurrentState(NodeExecutionState::EXECUTED);
  } else {
    state.setCurrentState(NodeExecutionState::ERROR);
    return false;
  }
  return processOutputPortData(execution);
}
