/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "IfNode.h"
#include "../../../Logger.h"
#include "../../Workflow.h"
#include "../../WorkflowBranch.h"
#include "../../WorkflowExecutor.h"
#include "../../ports/BranchingPort.h"

IfNode::IfNode(const std::string &id)
    : BranchingNode(id), name("If") {

  // inputs
  createPort<DependencyPort>(0, PortDirection::In);
  conditionPort = createPort<DataPort>(1, PortDirection::In);

  // outputs
  createPort<DependencyPort>(0, PortDirection::Out);
  trueBranchPort = createPort<BranchingPort>(1);
  falseBranchPort = createPort<BranchingPort>(2);
}

bool IfNode::execute(WorkflowExecution *execution) {
  if (!processInputPortData(execution)) return false;
  WorkflowBranch *selectedBranch;
  WorkflowBranch *branchToSkip;
  state.setCurrentState(NodeExecutionState::RUNNING);
  if (conditionPort->getData() && conditionPort->getData()->toBoolean()) {
    LOG_NODE("The condition is true, executing respective branch");
    selectedBranch = trueBranchPort->getBranch();
    branchToSkip = falseBranchPort->getBranch();
  } else {
    LOG_NODE("The condition is false or not specified, executing respective branch");
    selectedBranch = falseBranchPort->getBranch();
    branchToSkip = trueBranchPort->getBranch();
  }

  if (!selectedBranch) {
    LOG_NODE("Unable to execute the selected branch");
    return false;
  }

  state.setActiveBranch(selectedBranch);
  state.setBranchTokens((int) selectedBranch->getBranchTokens());
  if (branchToSkip) {
    branchToSkip->skip();
  }
  if (!processOutputPortData(execution)) return false;
  return true;
}

const std::string &IfNode::getName() const {
  return name;
}
