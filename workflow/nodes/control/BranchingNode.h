/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once

#include <string>
#include <unordered_set>

class WorkflowBranch;
class Node;
class BranchingPort;

#include "../ExecutableNode.h"

class BranchingNode : public ExecutableNode {
public:
  explicit BranchingNode(const std::string &id);

  void initBranches();

  virtual std::vector<Node *> takeBranchToken(WorkflowExecution *execution);

  std::vector<Node *> nodeFinished(WorkflowExecution *execution) override;

  /**
   * Returns nodes on which takeToken() must be called to start the branch @param branchingPort.
   * Note: A single node can (and should in some cases) occur multiple times in the vector!
   * @param branchingPort A branch pointer which must be non null
   * @return Vector of nodes
   */
  std::vector<Node *> getBranchStartNodes(const BranchingPort *branchingPort) const;

  unsigned int getNodesProcessed() const override;
  unsigned int getNodesProcessedInLoop() const override;
  unsigned int getNodesTotal() const override;

  void skip() override;

protected:
  /**
   * Returns all nodes which are in the branch originating from @param branchingPort
   * and also have an additional *value dependency* to this node (for example to the outgoing index port
   * of LoopNode). It is important to call takeToken() on these dependents, because otherwise
   * they won't be executed because the token is not taken when starting the branch (there will be an
   * additional token for this value dependency, regardless of whether the node is a start node of the branch
   * or not).
   */
  std::unordered_set<Node *> getInBranchValueDependents(const BranchingPort *branchingPort) const;

private:
  WorkflowBranch *getActiveOrLargestBranch() const;

  bool branchesInitialized;
  std::vector<WorkflowBranch *> branches;
};
