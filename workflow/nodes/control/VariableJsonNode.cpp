/* Copyright 2022 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "VariableJsonNode.h"
#include "../../../Logger.h"
#include "../../../util.h"
#include "../../ports/DataPort.h"
#include "../../ports/DependencyPort.h"
#include "../../workflow/Workflow.h"

VariableJsonNode::VariableJsonNode(const std::string &id)
    : ExecutableNode(id), name("VariableJson") {
  createPort<DependencyPort>(0, PortDirection::In);
  createPort<DependencyPort>(0, PortDirection::Out);

  jsonPort = createPort<DataPort>(1, PortDirection::In);
  keyPort = createPort<DataPort>(2, PortDirection::In);
}

const std::string &VariableJsonNode::getName() const {
  return name;
}

bool VariableJsonNode::execute(WorkflowExecution *execution) {
  if (!processInputPortData(execution)) return false;
  state.setCurrentState(NodeExecutionState::RUNNING);
  auto json_data = jsonPort->getData();
  auto key_data = keyPort->getData();
  bool result = false;
  if (json_data) {
    std::string key = std::string(config::VARIABLE_JSON_DEFAULT_KEY);
    if (key_data) {
      key = execution->expandVariables(key_data->toString());
    }
    if (json_data) {
      std::string json_raw = execution->expandVariables(json_data->toString());
      try {
        json json_object = json::parse(json_raw);
        if (!json_object.contains(key)) {
          LOG_NODE("Error: Key \"" << key << "\" not found in given json data");
        } else if (!json_object[key].is_array()) {
          LOG_NODE("Error: Key \"" << key << "\" does not contain an array");
        } else {
          setVariables(execution, json_object[key]);
          result = true;
        }
      } catch (const json::parse_error &e) {
        LOG_NODE("Parse error: " << e.what());
      } catch (const std::invalid_argument &e) {
        LOG_NODE("Json format doesn't match: " << e.what());
      }
    }
  } else {
    LOG_NODE("Error setting variables: json input is required!");
  }
  if (result) {
    state.setCurrentState(NodeExecutionState::EXECUTED);
  } else {
    state.setCurrentState(NodeExecutionState::ERROR);
    return false;
  }
  return processOutputPortData(execution);
}

void VariableJsonNode::setVariables(WorkflowExecution *execution, const json::array_t &json_array) {
  bool nested_values = false;
  bool empty_keys = false;
  for (const auto &entry : json_array) {
    if (entry.contains("key") && entry.contains("value")) {
      if (entry["value"].is_object() || entry["value"].is_array()) {
        nested_values = true;
      } else {
        std::string key = entry["key"].get<std::string>();
        std::string value = valueToString(entry["value"]);
        if (!key.empty()) {
          execution->setVariable(key, value);
        } else {
          empty_keys = true;
        }
      }
    }
  }

  if (nested_values) {
    LOG_NODE("Warning: Nested values were ignored");
  }
  if (empty_keys) {
    LOG_NODE("Warning: Values with empty key were ignored");
  }
}

std::string VariableJsonNode::valueToString(const json &value) {
  if (value.is_string()) {
    return value.get<std::string>();
  } else {
    return to_string(value);
  }
}
