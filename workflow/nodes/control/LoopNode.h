#pragma once


#include "../../ports/DataPort.h"
#include "../../ports/DependencyPort.h"
#include "../../state/WorkflowExecution.h"
#include "../Node.h"
#include "BranchingNode.h"

class BranchingPort;

class LoopNode : public BranchingNode {
public:
  explicit LoopNode(const std::string &id);

  bool execute(WorkflowExecution *execution) override;
  const std::string &getName() const override;

  NodeState getInitialState() const override;
  bool processInputPortData(WorkflowExecution *execution) override;
  bool processOutputPortData(WorkflowExecution *execution) override;

  std::vector<Node *> takeBranchToken(WorkflowExecution *execution) override;

  std::vector<Node *> nodeFinished(WorkflowExecution *execution) override;

private:
  bool loopCondition(WorkflowExecution *execution);

  const std::string name;

  /**
   * This function calculates the dynamic loop index which starts at startIndexPort and is incremented by `step`
   * after each loop iteration.
   * This is the value which is populated to the loop output port (loopIndexPort) and to the index variable.
   *
   * Note: The current implementation relies on startIndex and step being constant throughout all loop iterations!
   */
  int loopIndex(WorkflowExecution *execution) const;

  int branchTokenCount() const;

  bool condition;
  bool conditionSet;
  int startIndex;
  int endIndex;
  bool endIndexSet;
  int step;
  std::string varName;

  // input ports
  DataPort *conditionPort;
  DataPort *startIndexPort;
  DataPort *endIndexPort;
  DataPort *stepPort;
  DataPort *varNamePort;

  // output ports
  BranchingPort *loopBranchPort;
  DataPort *loopIndexPort;
};
