#include "../../../Logger.h"
#include "../../Workflow.h"
#include "../../WorkflowBranch.h"
#include "../../ports/BranchingPort.h"

#include "LoopNode.h"


LoopNode::LoopNode(const std::string &id)
    : BranchingNode(id), name("Loop"),
      condition(false), conditionSet(false), startIndex(0), endIndex(0),
      endIndexSet(false), step(1),
      varName(config::DEFAULT_LOOP_VARIABLE) {
  // inputs
  createPort<DependencyPort>(0, PortDirection::In);
  conditionPort = createPort<DataPort>(1, PortDirection::In);
  startIndexPort = createPort<DataPort>(2, PortDirection::In);
  endIndexPort = createPort<DataPort>(3, PortDirection::In);
  stepPort = createPort<DataPort>(4, PortDirection::In);
  varNamePort = createPort<DataPort>(5, PortDirection::In);

  startIndexPort->setData(std::make_shared<StaticData>(0)); // default value

  // outputs
  createPort<DependencyPort>(0, PortDirection::Out);
  loopBranchPort = createPort<BranchingPort>(1);
  loopIndexPort = createPort<DataPort>(2, PortDirection::Out);
}

bool LoopNode::execute(WorkflowExecution *execution) {
  if (!processInputPortData(execution)) return false;

  state.setCurrentState(NodeExecutionState::RUNNING);
  bool performLoopIteration = loopCondition(execution);
  if (performLoopIteration) {
    loopBranchPort->getBranch()->resetNodeStates();
    state.setBranchTokens(branchTokenCount());
  }
  if (!processOutputPortData(execution)) return false;
  return true;
}

const std::string &LoopNode::getName() const {
  return name;
}

bool LoopNode::loopCondition(WorkflowExecution *execution) {
  bool result = true;
  if (endIndexSet) {
    if (loopIndex(execution) > endIndex) { // the loop should still run once with index == endIndex!
      result = false;
      LOG_NODE("Exiting loop: Loop index is greater than the value of endIndex");
    }
  }
  if (conditionSet) {
    if (!condition) {
      LOG_NODE("Exiting loop: Loop condition is false");
      result = false;
    }
  }
  return result;
}

NodeState LoopNode::getInitialState() const {
  NodeState result = ExecutableNode::getInitialState();
  result.setBranchTokens(branchTokenCount());
  result.setActiveBranch(loopBranchPort->getBranch());
  return result;
}

bool LoopNode::processInputPortData(WorkflowExecution *execution) {
  if (startIndexPort->getData()) {
    startIndex = startIndexPort->getData()->expandInt(execution);
  }
  varName = config::DEFAULT_LOOP_VARIABLE;
  if (varNamePort->getData() && !varNamePort->getData()->isEmpty()) {
    varName = varNamePort->getData()->toString();
  }
  auto conditionData = conditionPort->getData();
  if (conditionData) {
    conditionSet = true;
    condition = conditionData->expandBool(execution);
  }
  auto endIndexData = endIndexPort->getData();
  if (endIndexData) {
    endIndexSet = true;
    endIndex = endIndexData->expandInt(execution);
  }
  auto stepData = stepPort->getData();
  if (stepData) {
    step = stepData->expandInt(execution);
  }
  return true;
}

bool LoopNode::processOutputPortData(WorkflowExecution *execution) {
  int index = loopIndex(execution);
  execution->setVariable(varName, std::to_string(index), true, true);
  loopIndexPort->setData(std::make_shared<StaticData>(index));
  return true;
}

int LoopNode::loopIndex(WorkflowExecution *execution) const {
  // note: step (and startIndex) must be constant for this calculation to be correct!
  return static_cast<int>(startIndex + (execution->getNodeState(getId()).getLoopIteration() * step));
}

std::vector<Node *> LoopNode::takeBranchToken(WorkflowExecution *execution) {
  if (!state.takeBranchToken()) {
    throw std::logic_error("Invalid attempt to take branch token from loop");
  }

  if (state.branchTokenCount() == 0) {
    // loop iteration has finished, let's start the next if necessary

    // Important: Add a token for the takeToken() call which will be issued when we return {this}
    state.setTokens(state.tokenCount() + 1);
    return {this};
  }
  return {};
}

int LoopNode::branchTokenCount() const {
  return (int) loopBranchPort->getBranch()->getBranchTokens();
}

std::vector<Node *> LoopNode::nodeFinished(WorkflowExecution *execution) {
  std::vector<Node *> result;

  bool continueLoop = loopCondition(execution);
  if (continueLoop) {
    for (Node *startNode : getBranchStartNodes(loopBranchPort)) {
      util::joinVector(result, startNode->takeToken(execution));
    }
    state.setLoopIteration(state.getLoopIteration() + 1); // already set the index for the next iteration
  } else {
    state.setCurrentState(NodeExecutionState::EXECUTED);
    std::vector<Node *> directDependents = getDirectDependents();
    for (Node *dependent : directDependents) {
      if (loopBranchPort->getBranch()->isInBranch(dependent->getId())) {
        continue;
      }
      util::joinVector(result, dependent->takeToken(execution));
    }

    if (branchParent && getDirectDependentsWithBranchParent(branchParent).empty()) {
      const std::vector<Node *> fromBranchParent = branchParent->takeBranchToken(execution);
      util::joinVector(result, fromBranchParent);
    }
  }

  return result;
}
