#include "ExecutableNode.h"

ExecutableNode::ExecutableNode(const std::string &id)
    : Node(id), executionProfile(ExecutionProfile::DEFAULT) {
}

void ExecutableNode::setExecutionProfile(ExecutionProfile profile) {
  executionProfile = profile;
}

NodeState ExecutableNode::getInitialState() const {
  if (executionProfile == ExecutionProfile::SKIP) {
    return NodeState(NodeExecutionState::TO_BE_SKIPPED);
  } else {
    return Node::getInitialState();
  }
}

ExecutionProfile ExecutableNode::getExecutionProfile() const {
  return executionProfile;
}
