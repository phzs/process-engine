/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once

#include "../ports/DataPort.h"
#include "Node.h"
#include "SystemCommandNode.h"
#include <string>
#include <utility>

class FileOutputNode : public SystemCommandNode {

public:
  explicit FileOutputNode(const std::string &id, std::string outputFilePath, std::string inputFilePath, bool createShortcut);
  ~FileOutputNode() override = default;

  std::string const &getName() const override;
  std::string getCommand(const WorkflowExecution *execution) const override;
  bool execute(WorkflowExecution *execution) override;

  std::string getPipeConnector() const;

private:
  /**
   * Return shortcut path as string.
   * Needs pointer to execution in order to expand variables.
   * This is called by WorkflowExecutor, independently from Node::execute!
   * @param execution Const pointer to the actual WorkflowExecution (containing actual variables).
   * @return Shortcut path as a string.
   */
  std::string getShortcut(const WorkflowExecution *execution);
  // helper method to avoid code redundancy
  static std::string getStringData(const DataPort *port, const std::string &defaultValue);

  const std::string name;
  DataPort *inputFilePathPort;
  DataPort *appendPort;

  std::string inputFilePath;
  std::string outputFilePath;
  bool create_shortcut;
};
