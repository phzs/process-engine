/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "../nodes/EnvNode.h"
#include "../ports/ParameterPort.h"
#include <algorithm>

#include "ToolNode.h"


void ToolNode::setToolPath(const std::string &path) {
  toolPath = path;
}

std::string ToolNode::getCommand(const WorkflowExecution *execution) const {
  std::string command = util::trim(toolPath);

  std::vector<ParameterPort *> ports;
  for (auto &element : ports_in) {
    auto parameterPort = dynamic_cast<ParameterPort *>(element.second.get());
    if (parameterPort) {
      ports.push_back(parameterPort);
    }
  }
  for (auto &element : ports_out) {
    auto parameterPort = dynamic_cast<ParameterPort *>(element.second.get());
    if (parameterPort) {
      ports.push_back(parameterPort);
    }
  }

  sort(ports.begin(), ports.end(), [](ParameterPort *lhs, ParameterPort *rhs) {
    return lhs->getArgumentPosition() < rhs->getArgumentPosition();
  });

  for (auto &port : ports) {
    port->updateData();
    if (port->isSet()) {
      command += " " + port->getCommandLineArgument(execution);
    }
  }

  if (getExecutionProfile() == ExecutionProfile::DETACHED) {
    command = "screen -dm " + command;
  }

  const EnvNode *connectedEnvNode = getConnectedEnvNode();
  if (connectedEnvNode && connectedEnvNode->getExecutionProfile() != ExecutionProfile::SKIP) {
    command = connectedEnvNode->getCommand(execution) + " --env-exec \"" + command + "\"";
  }

  return command;
}

std::string const &ToolNode::getName() const {
  return toolPath;
}

NodeState ToolNode::getInitialState() const {
  return SystemCommandNode::getInitialState();
}

const EnvNode *ToolNode::getConnectedEnvNode() const {

  // find the ingoing env port
  EnvPort *ingoingEnvPort = nullptr;
  for (const auto &port : ports_in) {
    auto *envPort = dynamic_cast<EnvPort *>(port.second.get());
    if (envPort) {
      if (ingoingEnvPort) {
        throw std::logic_error("Erroneous tool definition: ToolNodes may not have multiple ingoing ports of type Env.");
      } else {
        ingoingEnvPort = envPort;
      }
    }
  }

  // if connected, return connected EnvPort
  if (ingoingEnvPort) {
    std::vector<Port *> connectedPorts = ingoingEnvPort->getConnectedPorts();
    if (connectedPorts.size() > 1) {
      throw std::logic_error("Invalid topology: Inputs of type 'env' may only have one ingoing connection");
    }
    if (!connectedPorts.empty()) {
      auto connectedEnvPort = dynamic_cast<const EnvPort *>(connectedPorts[0]);
      if (connectedEnvPort) {
        return dynamic_cast<const EnvNode *>(connectedEnvPort->getNode());
      }
    }
  }

  return {};
}

std::unordered_set<std::string> ToolNode::getAllDependencies() const {
  std::unordered_set<std::string> result = SystemCommandNode::getAllDependencies();
  const EnvNode *connectedEnvNode = getConnectedEnvNode();
  if (connectedEnvNode) {
    // env dependencies must be treated as if they were direct dependencies
    std::unordered_set<std::string> envDependencies = connectedEnvNode->getAllDependencies();
    result.insert(envDependencies.begin(), envDependencies.end());
  }
  return result;
}

std::unordered_set<std::string> ToolNode::getDirectDependencies() const {
  std::unordered_set<std::string> result = SystemCommandNode::getDirectDependencies();
  const EnvNode *connectedEnvNode = getConnectedEnvNode();
  if (connectedEnvNode) {
    // env dependencies must be treated as if they were direct dependencies
    std::unordered_set<std::string> envDependencies = connectedEnvNode->getDirectDependencies();
    result.insert(envDependencies.begin(), envDependencies.end());
  }
  return result;
}
