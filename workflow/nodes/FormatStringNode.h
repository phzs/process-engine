/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once

#include "../ports/DataPort.h"
#include "ExecutableNode.h"

/**
 * A node which allows to format a string out of a variable number of inputs using replaceable patterns,
 * e.g. [%0, %1, %2, %3] -> [10, 20, 30, 40]. Input values are expected to be convertible to string.
 */
class FormatStringNode : public ExecutableNode {

public:
  FormatStringNode(const std::string &id, unsigned int numberOfInputs, std::string formatString);

  bool execute(WorkflowExecution *execution) override;

  const std::string &getName() const override {
    return name;
  }

  bool processOutputPortData(WorkflowExecution *execution) override;

  static const unsigned int defaultNumberOfInputs = 4; // to have a default available in the Parser

private:
  const std::string name;
  std::string format;
  std::string result; // temporary storage for the result

  DataPort *resultPort;
  std::vector<DataPort *> valueInputPorts;
};
