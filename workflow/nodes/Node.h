/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once

#include "../CoherentNodeGroup.h"
#include "../NodeStatisticsInterface.h"
#include "../ports/Port.h"
#include "../state/NodeState.h"
#include "../state/WorkflowExecution.h"
#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

class NodeState;
class Workflow;
class BranchingNode;

class Node : public NodeStatisticsInterface {
public:
  explicit Node(std::string id)
      : id(std::move(id)), order(0), branchId("root"), branchDepth(std::numeric_limits<unsigned int>::max()), executed(false), workflow(nullptr), branchParent(nullptr){};
  virtual ~Node() = default;

  void setOrder(int order);
  void setWorkflow(Workflow *workflow);
  const std::string &getId() const;

  /**
   * Create a port and add it to this node. This will create a new object of type `PortType` which will be owned by Node.
   * It returns a non-owning pointer, so that subclasses of Node can keep a pointer of the (more specific) type,
   * without having to deal with the ownership in their implementation.
   * @tparam PortType Type of Port which should be added
   * @tparam ConstructorParams
   * @param params Parameters for the constructor of PortType
   * @return Non-owning pointer of type PortType*
   */
  template<typename PortType, typename... ConstructorParams>
  PortType *createPort(ConstructorParams &&...params) {
    std::unique_ptr<PortType> port = std::make_unique<PortType>(std::forward<ConstructorParams>(params)...);
    PortType *result = port.get();
    addPort(std::move(port));
    return result;
  }

  virtual std::vector<Node *> onContinue(WorkflowExecution *execution) {
    return {};
  }

  virtual void skip() {
    state.skip();
  }

  /**
   * Adds a port to this node, takes ownership and performs various actions to register the port with it's node.
   * This function is public only for the JsonParser where a port is built outside the node and added later.
   * It is preferred to use createPort() instead when possible.
   * @param port Owned pointer to the port which will be added to the node
   */
  void addPort(std::unique_ptr<Port> port);

  Port *getPort(PortDirection direction, int index) const;

  virtual std::vector<CoherentConnectionInfo> getCoherentConnections(PortDirection direction) const;

  template<class T>
  void processPortsOfType(PortDirection direction, const std::function<void(T *)> &visitor) const {
    const auto &portMap = direction == PortDirection::Out ? ports_out : ports_in;
    for (const auto &port : portMap) {
      auto matchingPort = dynamic_cast<T *>(port.second.get());
      if (matchingPort) {
        visitor(matchingPort);
      }
    }
  }

  template<class T>
  std::vector<T *> getPortsByType(PortDirection direction) const {
    std::vector<T *> matchingPorts;
    processPortsOfType<T>(direction, [&](T *port) {
      matchingPorts.push_back(port);
    });
    return matchingPorts;
  }

  virtual std::unordered_set<std::string> getAllDependencies() const;
  std::vector<Node *> getDirectDependents() const;
  /**
   * Indirect dependents are dependents which need a value from this node.
   * Note that a node can be both indirect dependent and direct dependent, if it is connected with both a normal connection and dependency connection.
   */
  std::vector<Node *> getIndirectDependents() const;
  std::unordered_set<std::string> getExplicitDependents() const;

  std::unordered_set<Node *> getDirectDependentsExcludingBranch(const WorkflowBranch *branch) const;

  std::unordered_set<Node *> getDirectDependentsWithBranchParent(const BranchingNode *branchParent) const;

  /**
   * Execute the node. Each subclass must provide it's own implementation.
   * Should use the pointer to nodeStates to update the nodes state afterwards.
   *
   * Giving all nodeStates into the function is necessary because SystemCommandNodes must be able to
   * set the status for other nodes in their group as well.
   * @param execution Pointer to WorkflowExecution object, can be used to keep track of state related to execution
   * @param dry True when the execution should be simulated without performing real action
   * @param iteration Number of current workflow iteration
   * @return True if execution was successful
   */
  virtual bool execute(WorkflowExecution *execution) = 0;

  /**
   * Decreases the tokenCount and returns all Nodes which must be executed
   */
  virtual std::vector<Node *> takeToken(WorkflowExecution *execution);

  /**
   * Returns all nodes on which takeToken() must be called after this node was executed.
   * This includes a) dependents of the node
   *               b) dependents of a BranchingNode, if this node is a branch end and the branch is now executed completely
   *               c) start nodes of a branch, if this node is a branch end of a loop branch, the loop branch is now executed completely, and a new
   */
  virtual std::vector<Node *> nodeFinished(WorkflowExecution *execution);

  /**
   * Return the initial state of the node. The result is also used when node states will be reset.
   * The subclass can override this function to set a different initial state.
   * For example, SourceNodes set their state to NOOP, stating that there is nothing to do for them.
   * @return NodeState to be set for this Node when initializing or resetting WorkflowNodeStates
   */
  virtual NodeState getInitialState() const;

  virtual const std::string &getName() const = 0;

  virtual bool processInputPortData(WorkflowExecution *execution);
  virtual bool processOutputPortData(WorkflowExecution *execution);

  [[nodiscard]] const std::string &getBranchId() const;
  void setBranchId(unsigned int branchDepth, BranchingNode *branchParent, const std::string &branchId);

  BranchingNode *getBranchParent() const;

  [[nodiscard]] std::string getContext() const;

  [[nodiscard]] virtual bool toBeExecuted() const {
    return true;
  }

  unsigned int getNodesProcessed() const override;
  unsigned int getNodesProcessedInLoop() const override;
  unsigned int getNodesTotal() const override;

  NodeState &getState() {
    return state;
  }

  const NodeState &getState() const {
    return state;
  }

  void setState(NodeState newState) {
    state = std::move(newState);
  }

  void resetState() {
    state = getInitialState();
  }

  [[nodiscard]] virtual json toJson() const;

protected:
  // a helper for implementations of getNodeStatistics()
  bool isInRunningLoop() const;

  std::unordered_map<int, std::unique_ptr<Port>> ports_in;
  std::unordered_map<int, std::unique_ptr<Port>> ports_out;
  Workflow *workflow;

  // if this node is an end node of a branch, this will contain the pointer to the BranchingNode (used to call takeBranchToken() when this node was executed)
  BranchingNode *branchParent;
  NodeState state;

private:
  // internal helpers to avoid code redundancy
  static std::unordered_set<std::string> getConnectedIds(const std::unordered_map<int, std::unique_ptr<Port>> &portList, bool onlyDependencyPorts);
  static std::vector<Node *> getConnectedNodes(const std::unordered_map<int, std::unique_ptr<Port>> &portList, bool excludeDependencyPorts, bool excludeBranchPorts);
  static CoherentConnectionType resultingCoherentConnectionType(CoherentConnectionType left, CoherentConnectionType right);
  int getInitialTokens() const;

  std::string id;
  std::string branchId;
  unsigned int branchDepth;
  int order;
  bool executed;
};
