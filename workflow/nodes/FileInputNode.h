/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once


#include "../ports/DataPort.h"
#include "Node.h"
#include "SystemCommandNode.h"
#include <string>

class FileInputNode : public SystemCommandNode {

public:
  explicit FileInputNode(const std::string &id);
  ~FileInputNode() override = default;

  std::string const &getName() const override;
  std::string getCommand(const WorkflowExecution *execution) const override;

  bool processOutputPortData(WorkflowExecution *execution) override;

private:
  bool outgoingPipeConnection() const;

  DataPort *filePathPort;
  PipePort *stdoutPort;
  const std::string name;
};
