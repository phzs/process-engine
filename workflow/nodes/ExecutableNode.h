#pragma once

#include "../state/ExecutionProfile.h"
#include "Node.h"

class ExecutableNode : public Node {
public:
  explicit ExecutableNode(const std::string &id);
  ~ExecutableNode() override = default;

  bool execute(WorkflowExecution *execution) override = 0;
  void setExecutionProfile(ExecutionProfile profile);
  NodeState getInitialState() const override;

  ExecutionProfile getExecutionProfile() const;

  bool toBeExecuted() const override {
    return getExecutionProfile() != ExecutionProfile::SKIP;
  }

protected:
  ExecutionProfile executionProfile;
};
