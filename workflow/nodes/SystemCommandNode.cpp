/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "../../Logger.h"
#include "../ports/PipePort.h"
#include "FileOutputNode.h"
#include <process.hpp>

#include "SystemCommandNode.h"

bool SystemCommandNode::execute(WorkflowExecution *execution) {
  if (!processInputPortData(execution)) return false;

  bool success;
  if (state.getCurrentState() == NodeExecutionState::EXECUTED) {
    // TODO remove this if condition
    LOG_NODE("Node already executed");
    success = true; // already executed
  } else {
    if (!isInCoherentGroup()) {
      success = executeSingleNode(execution);
    } else {
      success = executeCoherentNodes(execution);
    }
  }
  if (!success) return false;
  return processOutputPortData(execution);
}

bool SystemCommandNode::executeSingleNode(WorkflowExecution *execution) {
  // assume that all dependencies are resolved
  auto ingoingPipeConnections = getCoherentConnections(PortDirection::In);
  if (!ingoingPipeConnections.empty()) {
    throw std::logic_error("Unexpected ingoing coherent connection: "
                           "Nodes with ingoing coherent connections must not be executed directly");
  }
  std::string command = getCommand(execution);
  bool result = executeCommandInternal(execution, command);
  state.setCurrentState(result ? NodeExecutionState::EXECUTED : NodeExecutionState::ERROR);
  return result;
}

bool SystemCommandNode::executeCoherentNodes(WorkflowExecution *execution) {
  /* Executing a node more than once is not allowed, so we have to make sure that either all of them are
   * executed (then skip) or none of them (then executing) */
  bool all_executed = true;
  bool at_least_one_executed = false;
  for (const auto &coherentNode : coherentGroup->getCoherentNodes()) {
    bool coherentNodeExecuted = (execution->getNodeState(coherentNode.connectedNode->getId()).getCurrentState() ==
                                 NodeExecutionState::EXECUTED);
    at_least_one_executed |= coherentNodeExecuted;
    all_executed &= coherentNodeExecuted;
  }
  if (all_executed) {
    LOG_NODE("All coherent nodes already executed");
    return true; // already executed
  } else if (at_least_one_executed) {
    throw std::logic_error("Some of the nodes in this coherent group were already executed");
  }

  std::vector<CoherentNodePtr> coherentGroupNodes = coherentGroup->getCoherentNodes();
  std::string command = coherentGroup->getCommand(execution);

  bool result = executeCommandInternal(execution, command);

  // set result in all nodes
  NodeExecutionState resultState = result ? NodeExecutionState::EXECUTED : NodeExecutionState::ERROR;
  for (const auto &groupElement : coherentGroupNodes) {
    std::string nodeId = groupElement.connectedNode->getId();
    if (execution->getNodeState(nodeId).getCurrentState() != NodeExecutionState::TO_BE_SKIPPED) {
      state.setCurrentState(resultState);
    }
  }

  return result;
}

bool SystemCommandNode::executeCommandInternal(WorkflowExecution *execution, const std::string &command) {
  bool result = true;
  if (command.empty()) {
    return true; // nothing to do
  }
  LOG_NODE(command);
  state.setCurrentState(NodeExecutionState::RUNNING);
  if (!execution->isDryRun()) {
    std::string output;
    std::string context = getContext();
    TinyProcessLib::Process runProcess(
      command, "", [&output, &context](const char *bytes, size_t n) {
      output = std::string(bytes, n);
      LOG_CONTEXT(context, "[stdout] " << output); }, [&context](const char *bytes, size_t n) { LOG_CONTEXT(context, "[stderr] " << std::string(bytes, n)); });
    int exit_code = runProcess.get_exit_status();
    LOG_NODE("[exit code: " << exit_code << " ]");
    result = (exit_code == 0);

    saveStdoutData(execution, output);
    // set the output on stdout to the outgoing PipePort, to allow other non-piping nodes to read it (conversion stdout -> *)
    setStdoutOutgoing(output);
  }
  printCommandLog(command);

  return result;
}

void SystemCommandNode::setCoherentGroup(std::shared_ptr<CoherentNodeGroup> group) {
  SystemCommandNode::coherentGroup = std::move(group);
}

std::unordered_set<std::string> SystemCommandNode::getAllDependencies() const {
  auto dependencies = Node::getAllDependencies();

  if (isInCoherentGroup()) {
    dependencies = coherentGroup->getGroupDependencies();
  }

  LOG_NODE_VERBOSE(getId() << " depends on");
  for (const auto &dependency : dependencies) {
    LOG_NODE_VERBOSE("\t\t" << dependency);
  }
  return dependencies;
}

bool SystemCommandNode::isInCoherentGroup() const {
  return (coherentGroup && (coherentGroup->size() > 1)); // ignore groups with one member
}

CoherentNodeGroup *SystemCommandNode::getCoherentGroup() const {
  return coherentGroup ? coherentGroup.get() : nullptr;
}

void SystemCommandNode::setStdoutOutgoing(const std::string &stdout_output) {
  std::string output = stdout_output;
  output.erase(output.find_last_not_of(" \n\r\t") + 1);
  if (isInCoherentGroup()) {
    getCoherentGroup()->setPipeOutputData(std::make_shared<StaticData>(output));
  } else {
    // note: this sets the stdout to all outgoing pipe ports (assuming there is only one)
    processPortsOfType<PipePort>(PortDirection::Out, [&](PipePort *pipePort) {
      pipePort->setData(std::make_shared<StaticData>(output));
    });
  }
}
NodeState SystemCommandNode::getInitialState() const {
  return ExecutableNode::getInitialState();
}

std::unordered_set<std::string>
SystemCommandNode::getDirectDependencies() const {
  return Node::getAllDependencies();
}

void SystemCommandNode::printCommandLog(const std::string &command) {
  std::ofstream commandLogFile;
  std::ostringstream fileName;
  fileName << std::string(config::LOG_FOLDER) << "/" << std::string(config::COMMAND_LOG_FILENAME);
  commandLogFile.open(fileName.str(), std::ios_base::app);
  commandLogFile << command << std::endl;
}

void SystemCommandNode::saveStdoutData(WorkflowExecution *execution, const std::string &stdout_output) {
  NodeState &nodeState = execution->getNodeState(getId());
  nodeState.setStdoutData(stdout_output);
}
