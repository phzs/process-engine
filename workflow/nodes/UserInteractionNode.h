/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once

#include "../interaction/Interaction.h"
#include "../ports/DependencyPort.h"
#include "../ports/PipePort.h"

#include "../../Logger.h"
#include "../state/WorkflowExecution.h"
#include "ExecutableNode.h"

template<typename T>
class UserInteractionNode : public ExecutableNode {
public:
  UserInteractionNode(const std::string &id, int interactionOrder, InteractionType type, InteractionDirection direction)
      : ExecutableNode(id), direction(direction), type(type), interactionOrder(interactionOrder) {
    createPort<DependencyPort>(0, PortDirection::In);
    createPort<DependencyPort>(0, PortDirection::Out);
  }

  ~UserInteractionNode() override = default;

  bool processInputPortData(WorkflowExecution *execution) override {
    return true;
  }

  bool processOutputPortData(WorkflowExecution *execution) override = 0;
  // note: deriving classes must also override pure virtual methods from Node:
  // - const std::string & getName() const

  bool execute(WorkflowExecution *execution) override {
    Interaction<T> *result = execution->getInteractionManager()->template registerInteraction<T>(this, interactionOrder, type, direction);
    if (result) {
      LOG_NODE("Prompt: " << result->getDescription());
      if (direction == InteractionDirection::INPUT) {
        LOG_NODE("Awaiting user input of type " << AbstractInteraction::typeToString(type) << "...");
      } else {
        LOG_NODE("Processing user output of type " << AbstractInteraction::typeToString(type));
      }
      state.setCurrentState(NodeExecutionState::NEEDS_INTERACTION);
      return true;
    } else {
      state.setCurrentState(NodeExecutionState::ERROR);
      LOG_NODE("Error: Unable to register a new user interaction");
      execution->persistTree();
      return false;
    }
  }

  std::vector<Node *> nodeFinished(WorkflowExecution *execution) override {
    // nothing to do, we have to wait for the interaction to be filled-out
    // Node::nodeFinished() will be called later via onContinue()
    return {};
  }

  /**
   * To be called (by the InteractionManager) once the value for an interaction which was registered by this node has become available.
   * Performs all necessary actions and returns nodes which have to be executed after this node has completed, as it would have been done
   * by nodeFinished() after execute().
   * @param execution
   * @return Vector of Node* to be executed
   */
  std::vector<Node *> onContinue(WorkflowExecution *execution) override {
    processOutputPortData(execution);
    state.setCurrentState(NodeExecutionState::EXECUTED);
    if (direction == InteractionDirection::INPUT) {
      LOG_NODE("Input value received");
    } else {
      LOG_NODE("Output confirmation received");
    }
    return Node::nodeFinished(execution);
  }

  void setInteraction(Interaction<T> *interaction) {
    this->interaction = interaction;
  }

protected:
  Interaction<T> *interaction;

private:
  InteractionDirection direction;
  InteractionType type;
  int interactionOrder;
};
