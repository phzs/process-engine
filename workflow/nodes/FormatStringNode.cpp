/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "FormatStringNode.h"

#include "../../Logger.h"
#include "../../util.h"
#include "../ports/DependencyPort.h"
#include <utility>

FormatStringNode::FormatStringNode(const std::string &id, unsigned int numberOfInputs, std::string formatString)
    : ExecutableNode(id), name("FormatString"), format(std::move(formatString)) {
  // inputs
  createPort<DependencyPort>(0, PortDirection::In);
  for (int i = 1; i <= numberOfInputs; i++) {
    valueInputPorts.push_back(createPort<DataPort>(i, PortDirection::In));
  }

  // outputs
  createPort<DependencyPort>(0, PortDirection::Out);
  resultPort = createPort<DataPort>(1, PortDirection::Out);
}

bool FormatStringNode::execute(WorkflowExecution *execution) {
  if (!processInputPortData(execution)) return false;
  std::vector<std::string> inputs;
  for (const auto &dataPort : valueInputPorts) {
    auto data = dataPort->getData();
    if (data) {
      inputs.push_back(data->toString());
    } else {
      inputs.emplace_back("");
    }
  }

  try {
    result = util::formatString(format, inputs);
    LOG_NODE("Formatted string is \"" << result << "\" (format: \"" << format << "\")");
    state.setCurrentState(NodeExecutionState::EXECUTED);
  } catch (const std::exception &e) {
    LOG_NODE("Error when formatting the string: " << e.what());
    state.setCurrentState(NodeExecutionState::ERROR);
    return false;
  }
  return processOutputPortData(execution);
}

bool FormatStringNode::processOutputPortData(WorkflowExecution *execution) {
  resultPort->setData(std::make_shared<StaticData>(result));
  return true;
}
