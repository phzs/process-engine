/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "EnvNode.h"

#include <utility>

EnvNode::EnvNode(std::string id)
    : ToolNode(std::move(id)) {
}

NodeState EnvNode::getInitialState() const {
  return NodeState(NodeExecutionState::NOOP);
}

std::unordered_set<std::string> EnvNode::getAllDependencies() const {
  // do not use the overridden implementation from ToolNode, because EnvNode supports no incoming environment connections
  return SystemCommandNode::getAllDependencies(); //NOLINT
}

std::unordered_set<std::string> EnvNode::getDirectDependencies() const {
  // do not use the overridden implementation from ToolNode, because EnvNode supports no incoming environment connections
  return SystemCommandNode::getDirectDependencies(); //NOLINT
}
