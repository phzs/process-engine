/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "../../Logger.h"
#include "../ports/BranchingPort.h"
#include "../ports/DependencyPort.h"
#include "../ports/PipePort.h"
#include <algorithm>
#include <unordered_set>

#include "../WorkflowExecutor.h"
#include "Node.h"
#include "SourceNode.h"
#include "control/LoopNode.h"

void Node::addPort(std::unique_ptr<Port> port) {
  if (!port) {
    throw std::invalid_argument("Can not add a port to the node which is null");
  }
  port->setNode(this);
  port->setWorkflow(workflow);
  auto invalidIndexMessage = "Port index " + std::to_string(port->getIndex()) + " is already taken!";
  if (port->getDirection() == PortDirection::In) {
    if (ports_in.find(port->getIndex()) != ports_in.end()) {
      throw std::invalid_argument(invalidIndexMessage);
    }
    ports_in[port->getIndex()] = std::move(port);
  } else if (port->getDirection() == PortDirection::Out) {
    if (ports_out.find(port->getIndex()) != ports_out.end()) {
      throw std::invalid_argument(invalidIndexMessage);
    }
    ports_out[port->getIndex()] = std::move(port);
  } else {
    throw std::logic_error("Port could not be added to Node: Unknown direction");
  }
}

Port *Node::getPort(PortDirection direction, int index) const {
  if (direction == PortDirection::In) {
    if (ports_in.find(index) != ports_in.end()) {
      return ports_in.at(index).get();
    }
  } else if (direction == PortDirection::Out) {
    if (ports_out.find(index) != ports_out.end()) {
      return ports_out.at(index).get();
    }
  } else {
    throw std::invalid_argument("Unknown direction");
  }
  return {};
}

const std::string &Node::getId() const {
  return id;
}

std::unordered_set<std::string> Node::getAllDependencies() const {
  std::unordered_set<std::string> dependencies;
  for (auto &port : ports_in) {
    auto newDependencies = port.second->getDependencies();
    for (const auto &dependencyNodeId : newDependencies) {
      dependencies.insert(dependencyNodeId);
    }
  }

  return dependencies;
}

std::vector<CoherentConnectionInfo> Node::getCoherentConnections(PortDirection direction) const {
  std::vector<CoherentConnectionInfo> result;
  if (!(direction == PortDirection::Out || direction == PortDirection::In)) {
    throw std::logic_error("Unknown Port Direction");
  }
  processPortsOfType<CoherencyPort>(direction, [&](const CoherencyPort *port) {
    auto connectedPorts = port->getConnectedPorts();
    for (const Port *connectedPort : connectedPorts) {
      if (!connectedPort) {
        throw std::logic_error("Connected port is null");
      }
      auto connectedCoherencyPort = dynamic_cast<const CoherencyPort *>(connectedPort);
      CoherentConnectionType left = port->getCoherentConnectionType();
      CoherentConnectionType right = CoherentConnectionType::NONE;
      if (connectedCoherencyPort) {
        right = connectedCoherencyPort->getCoherentConnectionType();
      }
      CoherentConnectionType connectionType = resultingCoherentConnectionType(left, right);
      result.push_back(CoherentConnectionInfo{connectedPort->getNodeId(), connectionType});
    }
  });
  return std::move(result);
}

void Node::setWorkflow(Workflow *workflow) {
  this->workflow = workflow;
  for (const auto &item : ports_in) {
    item.second->setWorkflow(workflow);
  }
  for (const auto &item : ports_out) {
    item.second->setWorkflow(workflow);
  }
}

NodeState Node::getInitialState() const {
  return NodeState(NodeExecutionState::TO_BE_EXECUTED, getInitialTokens());
}

std::unordered_set<std::string> Node::getExplicitDependents() const {
  return getConnectedIds(ports_out, true);
}

std::vector<Node *> Node::getDirectDependents() const {
  return getConnectedNodes(ports_out, false,
                           true); // do not include "branch dependents"
}

std::unordered_set<std::string>
Node::getConnectedIds(const std::unordered_map<int, std::unique_ptr<Port>> &portList, bool onlyDependencyPorts) {
  std::unordered_set<std::string> result;
  for (auto &port : portList) {
    auto connectedPorts = port.second->getConnectedPorts();
    for (const Port *connectedPort : connectedPorts) {
      if (connectedPort) {
        if (onlyDependencyPorts && !dynamic_cast<const DependencyPort *>(connectedPort)) {
          continue;
        }
        auto dependencyId = connectedPort->getNodeId();
        result.insert(dependencyId);
      }
    }
  }
  return result;
}

std::vector<Node *>
Node::getConnectedNodes(const std::unordered_map<int, std::unique_ptr<Port>> &portList, bool excludeDependencyPorts, bool excludeBranchPorts) {
  // TODO extract redundant part from getConnectedIds and() this function
  std::vector<Node *> result;
  for (auto &port : portList) {
    if (excludeBranchPorts && dynamic_cast<BranchingPort *>(port.second.get())) {
      continue;
    }
    if (excludeDependencyPorts && dynamic_cast<DependencyPort *>(port.second.get())) {
      continue;
    }
    auto connectedPorts = port.second->getConnectedPorts();
    for (Port *connectedPort : connectedPorts) {
      if (connectedPort) {
        result.push_back(connectedPort->getNode());
      }
    }
  }
  return result;
}

std::vector<Node *> Node::getIndirectDependents() const {
  return getConnectedNodes(ports_out, true, true); // return only nodes connected via outgoing "value" connections
}

bool Node::processInputPortData(WorkflowExecution *) {
  return true;
}

bool Node::processOutputPortData(WorkflowExecution *) {
  return true;
}

CoherentConnectionType
Node::resultingCoherentConnectionType(CoherentConnectionType left, CoherentConnectionType right) {
  if (left == right) {
    return left;
  } else if (left == CoherentConnectionType::PIPE && right != CoherentConnectionType::PIPE) {
    return CoherentConnectionType::PIPE_TO_OTHER;
  }
  return CoherentConnectionType::INVALID;
}

const std::string &Node::getBranchId() const {
  return branchId;
}

void Node::setBranchId(unsigned int depth, BranchingNode *newBranchParent, const std::string &newBranchId) {
  if (depth < branchDepth) {
    // branchId should contain the _direct_ parent branch of this node,
    // but the WorkflowAnalyzer will call this method every time it finds this node inside a branch
    // -> the branchDepth check avoids incorrect overwrites which come from a more distant relative
    branchId = newBranchId;
    branchDepth = depth;
    branchParent = newBranchParent;
  }
}

std::vector<Node *> Node::takeToken(WorkflowExecution *execution) {
  if (state.isSkipped()) {
    // this assures that dependents of skipped nodes are processed correctly and that they can notify
    // their branch parents via takeBranchToken if necessary
    return nodeFinished(execution);
  }
  if (!state.takeToken()) {
    throw std::logic_error("Invalid request to take a token");
  }
  if (state.tokenCount() == 0) {
    return {this};
  }
  return {};
}

int Node::getInitialTokens() const {
  int tokens = 0;
  for (auto &port : ports_in) {
    for (Node *node : port.second->getDependencyNodes()) {
      // count each appearance of a node, even if they appear more than once
      // they will also call takeToken() on us multiple times if connected on different ports
      if (node->toBeExecuted()) {
        tokens++;
      }
    }
  }

  return tokens;
}

BranchingNode *Node::getBranchParent() const {
  return branchParent;
}

std::vector<Node *> Node::nodeFinished(WorkflowExecution *execution) {
  execution->persistInfo();
  std::vector<Node *> nodesToExecute;

  std::vector<Node *> nodesToTakeTokenFrom;
  /* 1. See if the node has a branch parent which it needs to notify (via takeBranchToken())
   *    This is necessary to handle BranchNodes via nodeFinished() as soon as their branch finished.
   *    It must only be done, if this node is the end of a branch (which is the case when there are no further dependents). */
  std::vector<Node *> directDependents = getDirectDependents();
  if (branchParent && getDirectDependentsWithBranchParent(branchParent).empty()) {
    const std::vector<Node *> fromBranchParent = branchParent->takeBranchToken(execution);
    util::joinVector(nodesToTakeTokenFrom, fromBranchParent);
  }
  // 2. add dependents
  for (Node *dependentNode : directDependents) {
    nodesToTakeTokenFrom.push_back(dependentNode);
  }

  // call takeToken() on nodes from step1 and step2, each call will return a list of nodes to actually execute
  for (Node *node : nodesToTakeTokenFrom) {
    std::vector<Node *> toExecute = node->takeToken(execution);
    util::joinVector(nodesToExecute, toExecute);
  }

  return nodesToExecute;
}

std::string Node::getContext() const {
  return getId();
}

std::unordered_set<Node *> Node::getDirectDependentsExcludingBranch(const WorkflowBranch *branch) const {
  std::unordered_set<Node *> result;
  auto allDirectDependencies = getDirectDependents();
  const std::unordered_set<std::string> &branchNodes = branch->getNodeIds();
  std::copy_if(allDirectDependencies.begin(), allDirectDependencies.end(),
               std::inserter(result, result.end()), [branchNodes](Node *const node) {
                 // this filters out all nodes which are in the branch
                 return branchNodes.find(node->getId()) == branchNodes.end();
               });
  return result;
}

std::unordered_set<Node *> Node::getDirectDependentsWithBranchParent(const BranchingNode *branchParent) const {
  std::unordered_set<Node *> result;
  auto allDirectDependencies = getDirectDependents();
  std::copy_if(allDirectDependencies.begin(), allDirectDependencies.end(),
               std::inserter(result, result.end()), [branchParent](Node *const node) {
                 // this filters out all nodes which are in the branch
                 return node->getBranchParent() == branchParent;
               });
  return result;
}

bool Node::isInRunningLoop() const {
  if (!branchParent) {
    return false;
  } else if (auto *parentLoop = dynamic_cast<LoopNode *>(branchParent)) {
    return parentLoop->getState().getCurrentState() == NodeExecutionState::RUNNING;
  } else {
    return branchParent->isInRunningLoop();
  }
}

unsigned int Node::getNodesProcessed() const {
  if (!isInRunningLoop() && getState().isExecuted()) {
    return 1;
  }
  return 0;
}

unsigned int Node::getNodesProcessedInLoop() const {
  if (isInRunningLoop() && state.isExecuted()) {
    return 1;
  }
  return 0;
}

unsigned int Node::getNodesTotal() const {
  return state.isSkipped() ? 0 : 1;
}

json Node::toJson() const {
  json result;
  result["name"] = getName();
  if (BranchingNode *parent = getBranchParent()) {
    result["parent"] = parent->getId();
  }
  result["state"] = state.getCurrentState();
  result["order"] = order;
  return result;
}

void Node::setOrder(int order) {
  Node::order = order;
}
