/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "UserInputCropImagesNode.h"
#include "../../workflow/Workflow.h"
#include <regex>
#include <string>

UserInputCropImagesNode::UserInputCropImagesNode(const std::string &id, const std::string &description,
                                                 int interactionOrder)
    : UserInteractionNode(id, interactionOrder, InteractionType::CROP_IMAGES, InteractionDirection::INPUT), name("UserInputCropImagesNode") {

  // input ports
  promptMessagePort = createPort<DataPort>(1, PortDirection::In);
  imagePathPort = createPort<DataPort>(2, PortDirection::In);

  // output ports
  cropInfoOutPort = createPort<DataPort>(1, PortDirection::Out);
}

const std::string &UserInputCropImagesNode::getName() const {
  return name;
}

bool UserInputCropImagesNode::processInputPortData(WorkflowExecution *execution) {
  UserInteractionNode::processInputPortData(execution);
  if (imagePathPort->getData()) {
    auto path = imagePathPort->getData()->toString();
    path = execution->expandVariables(path);
    interaction->setPath(path);
  }
  if (promptMessagePort->getData()) {
    auto promptMessage = promptMessagePort->getData()->toString();
    promptMessage = execution->expandVariables(promptMessage);
    interaction->setDescription(promptMessage);
  }
  return true;
}

bool UserInputCropImagesNode::processOutputPortData(WorkflowExecution *execution) {
  if (interaction->getValue()) {
    auto value = std::make_shared<StaticData>(interaction->getValue()->toString());
    cropInfoOutPort->setData(std::move(value));
  }
  return true;
}
