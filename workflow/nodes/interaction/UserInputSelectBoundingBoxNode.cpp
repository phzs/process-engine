/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "UserInputSelectBoundingBoxNode.h"
#include "../../workflow/Workflow.h"
#include <regex>
#include <string>


UserInputSelectBoundingBoxNode::UserInputSelectBoundingBoxNode(const std::string &id, const std::string &description,
                                                               int interactionOrder)
    : UserInteractionNode(id, interactionOrder, InteractionType::CROP_IMAGES, InteractionDirection::INPUT), name("UserInputSelectBoundingBoxNode") {

  // input ports
  promptMessagePort = createPort<DataPort>(1, PortDirection::In);
  urlPort = createPort<DataPort>(2, PortDirection::In);

  // output ports
  xOutPort = createPort<DataPort>(1, PortDirection::Out);
  yOutPort = createPort<DataPort>(2, PortDirection::Out);
  widthOutPort = createPort<DataPort>(3, PortDirection::Out);
  heightOutPort = createPort<DataPort>(4, PortDirection::Out);
}

const std::string &UserInputSelectBoundingBoxNode::getName() const {
  return name;
}

bool UserInputSelectBoundingBoxNode::processInputPortData(WorkflowExecution *execution) {
  UserInteractionNode::processInputPortData(execution);
  if (urlPort->getData()) {
    auto path = urlPort->getData()->toString();
    path = execution->expandVariables(path);
    interaction->setPath(path);
  }
  if (promptMessagePort->getData()) {
    auto promptMessage = promptMessagePort->getData()->toString();
    promptMessage = execution->expandVariables(promptMessage);
    interaction->setDescription(promptMessage);
  }
  return true;
}

bool UserInputSelectBoundingBoxNode::processOutputPortData(WorkflowExecution *execution) {
  if (interaction->getValue()) {
    data::BoundingBox *boundingBox = interaction->getValue();
    xOutPort->setData(std::make_shared<StaticData>(boundingBox->x));
    yOutPort->setData(std::make_shared<StaticData>(boundingBox->y));
    widthOutPort->setData(std::make_shared<StaticData>(boundingBox->width));
    heightOutPort->setData(std::make_shared<StaticData>(boundingBox->height));
  }
  return true;
}
