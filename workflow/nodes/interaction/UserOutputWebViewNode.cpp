
#include "../../workflow/Workflow.h"

#include "UserOutputWebViewNode.h"

UserOutputWebViewNode::UserOutputWebViewNode(const std::string &id, const std::string &description,
                                             const std::string &url, int interactionOrder)
    : UserInteractionNode(id, interactionOrder, InteractionType::WEB_VIEW, InteractionDirection::OUTPUT), name("UserOutputWebViewNode") {
  // input ports
  promptMessagePort = createPort<DataPort>(1, PortDirection::In);
  urlPort = createPort<DataPort>(2, PortDirection::In);
}

const std::string &UserOutputWebViewNode::getName() const {
  return name;
}

bool UserOutputWebViewNode::processInputPortData(WorkflowExecution *execution) {
  UserInteractionNode::processInputPortData(execution);
  if (promptMessagePort->getData()) {
    auto promptMessage = execution->expandVariables(promptMessagePort->getData()->toString());
    interaction->setDescription(promptMessage);
  }
  if (urlPort->getData()) {
    auto url = execution->expandVariables(urlPort->getData()->toString());
    interaction->setUrl(url);
  }
  return true; // no validation of required ports for now
}

bool UserOutputWebViewNode::processOutputPortData(WorkflowExecution *) {
  return true;
}
