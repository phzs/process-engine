#include "UserInputChooseNode.h"

UserInputChooseNode::UserInputChooseNode(const std::string &id, const std::string &description, int interactionOrder,
                                         unsigned int numberOfOptions)
    : UserInteractionNode(id, interactionOrder, InteractionType::CHOOSE, InteractionDirection::INPUT), name("UserInputChooseNode"), numberOfOptions(numberOfOptions) {

  // inputs
  promptMessagePort = createPort<DataPort>(1, PortDirection::In);
  defaultValuePort = createPort<DataPort>(2, PortDirection::In);
  for (int i = 0; i < numberOfOptions; i++) {
    int index = i + 3; // starting at 3
    optionTextPorts.push_back(createPort<DataPort>(index, PortDirection::In));
  }

  // outputs
  selectedOptionPort = createPort<DataPort>(1, PortDirection::Out);
  selectedValuePort = createPort<DataPort>(2, PortDirection::Out);
}

bool UserInputChooseNode::processInputPortData(WorkflowExecution *execution) {
  UserInteractionNode::processInputPortData(execution);
  if (promptMessagePort->getData()) {
    auto promptMessage = execution->expandVariables(promptMessagePort->getData()->toString());
    interaction->setDescription(promptMessage);
  }
  if (defaultValuePort->getData()) {
    int defaultSelection = StaticData(execution->expandVariables(defaultValuePort->getData()->toString())).toInt();
    if (defaultSelection < optionTextPorts.size() && optionTextPorts[defaultSelection]->getData()) {
      interaction->setDefaultValue(std::make_unique<std::string>(optionTextPorts[defaultSelection]->getData()->toString()));
    }
  }
  std::vector<std::string> options;
  for (const auto &optionTextPort : optionTextPorts) {
    if (optionTextPort) {
      auto data = optionTextPort->getData();
      if (data && !data->isEmpty()) {
        options.push_back(data->toString());
      }
    }
  }
  interaction->setOptions(options);
  return true;
}

bool UserInputChooseNode::processOutputPortData(WorkflowExecution *execution) {
  std::string *interactionValue = interaction->getValue();
  if (interactionValue) {
    int outputValue = -1;
    std::string selectedOption = std::string(*interactionValue);
    for (int i = 0; i < optionTextPorts.size(); i++) {
      if (optionTextPorts[i]) {
        auto data = optionTextPorts[i]->getData();
        if (data && !data->isEmpty() && data->toString() == selectedOption) {
          outputValue = i;
          break; // take the first match, ambiguous values will be ignored
        }
      }
    }
    outputValue++; // in the workflow editor, values start from 1
    selectedOptionPort->setData(std::make_unique<StaticData>(outputValue));
    selectedValuePort->setData(std::make_unique<StaticData>(selectedOption));
  }
  return true;
}

const std::string &UserInputChooseNode::getName() const {
  return name;
}
