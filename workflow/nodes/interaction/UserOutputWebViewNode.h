#pragma once


#include "../../ports/DataPort.h"
#include "../UserInteractionNode.h"
#include <sstream>

class UserOutputWebViewNode : public UserInteractionNode<std::string> {

public:
  explicit UserOutputWebViewNode(const std::string &id, const std::string &description, const std::string &url, int interactionOrder);
  const std::string &getName() const override;

private:
  bool processInputPortData(WorkflowExecution *execution) override;
  bool processOutputPortData(WorkflowExecution *execution) override;

  std::string name;
  DataPort *promptMessagePort;
  DataPort *urlPort;
};
