#pragma once

#include <memory>

#include "../../ports/DataPort.h"
#include "../UserInteractionNode.h"
#include "../control/BranchingNode.h"

class UserInputChooseNode : public UserInteractionNode<std::string> {

public:
  explicit UserInputChooseNode(const std::string &id, const std::string &description, int interactionOrder,
                               unsigned int numberOfOptions);

  const std::string &getName() const override;

private:
  bool processInputPortData(WorkflowExecution *execution) override;
  bool processOutputPortData(WorkflowExecution *execution) override;

  std::string name;

  // ingoing ports
  DataPort *promptMessagePort;
  DataPort *defaultValuePort;
  std::vector<DataPort *> optionTextPorts; // text labels that the user provided

  // outgoing ports
  DataPort *selectedOptionPort;
  DataPort *selectedValuePort;

  // other
  const unsigned int numberOfOptions;
};
