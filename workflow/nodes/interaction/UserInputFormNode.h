/* Copyright 2021 Karlsruhe Institute of Technology
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License. */

#ifndef PROCESS_ENGINE_USERINPUTFORMNODE_H
#define PROCESS_ENGINE_USERINPUTFORMNODE_H


#include "../UserInteractionNode.h"

class UserInputFormNode : public UserInteractionNode<json> {

public:
  explicit UserInputFormNode(const std::string &id, int interactionOrder);
  ~UserInputFormNode() override = default;

  const std::string &getName() const override;

private:
  bool processInputPortData(WorkflowExecution *execution) override;
  bool processOutputPortData(WorkflowExecution *execution) override;

  std::string name;
  DataPort *dataInPort;
  DataPort *dataOutPort;
};


#endif
