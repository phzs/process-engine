/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "UserOutputTextNode.h"
#include "../../../Logger.h"
#include "../../../util.h"
#include "../../workflow/Workflow.h"

UserOutputTextNode::UserOutputTextNode(const std::string &id, const std::string &inputFile, int interactionOrder)
    : UserInteractionNode(id, interactionOrder, InteractionType::STRING, InteractionDirection::OUTPUT), name("UserOutputTextNode") {
  // input ports
  inputFilePort = createPort<DataPort>(1, PortDirection::In);
}

const std::string &UserOutputTextNode::getName() const {
  return name;
}

bool UserOutputTextNode::processInputPortData(WorkflowExecution *execution) {
  UserInteractionNode::processInputPortData(execution);
  if (inputFilePort->getData()) {
    std::string path = execution->expandVariables(inputFilePort->getData()->toString());
    if (path.empty()) {
      LOG_NODE("Error: Path for the input file was empty");
      return false;
    }
    try {
      path = util::getAbsoluteFilePath(path);
    } catch (const std::invalid_argument &err) {
      LOG_NODE("Failed to convert relative path for input file to absolute path: " << err.what());
    }
    if (!util::fileExists(path)) {
      LOG_NODE("Error: File does not exist");
      return false;
    }
    std::string fileContent = util::readFile(path);
    LOG_NODE_VERBOSE("Reading file content (file name: " << path << "): ");
    LOG_NODE_VERBOSE(fileContent);
    if (!fileContent.empty()) {
      interaction->setDescription(fileContent);
      return true;
    } else {
      LOG_NODE("Text file was empty!");
    }
  } else {
    LOG_NODE("Mandatory input port not connected!");
  }
  return false;
}

bool UserOutputTextNode::processOutputPortData(WorkflowExecution *) {
  return true; // no output ports with data
}
