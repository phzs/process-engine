/* Copyright 2021 Karlsruhe Institute of Technology
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License. */

#include "UserInputFormNode.h"
#include "../../workflow/Workflow.h"

UserInputFormNode::UserInputFormNode(const std::string &id, int interactionOrder)
    : UserInteractionNode(id, interactionOrder, InteractionType::FORM, InteractionDirection::INPUT), name("UserInputFormNode") {
  // input ports
  dataInPort = createPort<DataPort>(1, PortDirection::In);

  // output ports
  dataOutPort = createPort<DataPort>(1, PortDirection::Out);
}

const std::string &UserInputFormNode::getName() const {
  return name;
}

bool UserInputFormNode::processInputPortData(WorkflowExecution *execution) {
  UserInteractionNode::processInputPortData(execution);
  if (dataInPort->getData()) {
    std::string path = execution->expandVariables(dataInPort->getData()->toString());
    try {
      path = util::getAbsoluteFilePath(path);
    } catch (const std::invalid_argument &err) {
      LOG_NODE("Failed to convert relative path to absolute path: " << err.what());
    }
    interaction->setPath(path);
  }
  return true;
}

bool UserInputFormNode::processOutputPortData(WorkflowExecution *execution) {
  if (interaction->getValue()) {
    std::stringstream jsonString;
    jsonString << "'";
    jsonString << interaction->getValue()->dump();
    jsonString << "'";
    dataOutPort->setData(std::make_shared<StaticData>(jsonString.str()));
  }
  return true;
}
