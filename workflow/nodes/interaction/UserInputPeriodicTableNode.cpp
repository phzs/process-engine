/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "UserInputPeriodicTableNode.h"

UserInputPeriodicTableNode::UserInputPeriodicTableNode(const std::string &id, const std::string &description,
                                                       int interactionOrder)
    : UserInteractionNode(id, interactionOrder, InteractionType::PERIODIC_TABLE, InteractionDirection::INPUT), name("UserInputPeriodicTable") {

  // input ports
  promptMessagePort = createPort<DataPort>(1, PortDirection::In);
  defaultValuePort = createPort<DataPort>(2, PortDirection::In);

  // output ports
  valueOutPort = createPort<DataPort>(1, PortDirection::Out);
}

const std::string &UserInputPeriodicTableNode::getName() const {
  return name;
}

bool UserInputPeriodicTableNode::processInputPortData(WorkflowExecution *execution) {
  UserInteractionNode::processInputPortData(execution);
  if (promptMessagePort->getData()) {
    auto promptMessage = execution->expandVariables(promptMessagePort->getData()->toString());
    interaction->setDescription(promptMessage);
  }
  if (defaultValuePort->getData()) {
    auto defaultValue = execution->expandVariables(defaultValuePort->getData()->toString());
    interaction->setDefaultValue(std::make_unique<std::string>(StaticData(defaultValue).toString()));
  }
  if (defaultValuePort->getData()) {
    interaction->setDefaultValue(std::make_unique<std::string>(defaultValuePort->getData()->toString()));
  }
  return true;
}

bool UserInputPeriodicTableNode::processOutputPortData(WorkflowExecution *execution) {
  if (interaction->getValue()) {
    valueOutPort->setData(std::make_shared<StaticData>(*interaction->getValue()));
  }
  return true;
}
