/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "../../util.h"
#include "../../workflow/Workflow.h"
#include "../ports/DependencyPort.h"
#include "../ports/ParameterPort.h"
#include "../ports/PipePort.h"

#include "FileOutputNode.h"

FileOutputNode::FileOutputNode(const std::string &id, std::string outputFilePath, std::string inputFilePath,
                               bool createShortcut)
    : SystemCommandNode(id), outputFilePath(std::move(outputFilePath)), name("FileOutputNode"),
      inputFilePath(std::move(inputFilePath)), create_shortcut(createShortcut) {

  // input ports
  createPort<DependencyPort>(0, PortDirection::In);
  inputFilePathPort = createPort<DataPort>(1, PortDirection::In);
  appendPort = createPort<DataPort>(2, PortDirection::In);
  createPort<PipePort>(3, PortDirection::In);

  // output ports
  createPort<DependencyPort>(0, PortDirection::Out);
}

std::string const &FileOutputNode::getName() const {
  return name;
}

std::string FileOutputNode::getCommand(const WorkflowExecution *execution) const {
  if (!workflow->areCoherentGroupsInitialized()) {
    throw std::logic_error("Internal error: Coherent groups must be initialized before running");
  }
  std::string command;
  std::string inputFile = getStringData(inputFilePathPort, inputFilePath);

  // do nothing if path is empty
  if (inputFile.empty()) {
    return {};
  }

  inputFile = execution->expandVariables(inputFile);
  // return only the output file, to get the following result (as a command):
  // ...[previous_command] > [this_command: outputFilePath]
  if (!getCoherentConnections(PortDirection::In).empty()) {
    command += inputFile;
  }
  return command;
}

std::string FileOutputNode::getShortcut(const WorkflowExecution *execution) {
  if (!execution) throw std::invalid_argument("Execution must not be null");
  std::string inputFile, outputFile;
  inputFile = getStringData(inputFilePathPort, inputFilePath);
  if (!inputFile.empty()) {
    return execution->expandVariables(inputFile);
  }
  outputFile = getStringData(appendPort, outputFilePath);
  return execution->expandVariables(outputFile);
}

std::string FileOutputNode::getStringData(const DataPort *port, const std::string &defaultValue) {
  std::string result = defaultValue;
  if (port && port->getData() && !port->getData()->isEmpty()) {
    result = port->getData()->toString();
  }
  return result;
}

std::string FileOutputNode::getPipeConnector() const {
  std::string inputFile = getStringData(inputFilePathPort, inputFilePath);
  if (inputFile.empty() || getCoherentConnections(PortDirection::In).empty()) {
    return {};
  }
  bool append = false;
  if (appendPort && appendPort->getData() && !appendPort->getData()->isEmpty()) {
    append = appendPort->getData()->toBoolean();
  }
  return append ? " >> " : " > ";
}

bool FileOutputNode::execute(WorkflowExecution *execution) {
  bool result = SystemCommandNode::execute(execution);
  if (result && create_shortcut) {
    execution->addShortcut(getShortcut(execution));
  }
  return result;
}
