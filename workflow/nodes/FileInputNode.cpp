/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "FileInputNode.h"
#include "../../Logger.h"
#include "../../util.h"
#include "../Workflow.h"
#include "../ports/DependencyPort.h"
#include "../ports/PipePort.h"
#include <fstream>

FileInputNode::FileInputNode(const std::string &id)
    : SystemCommandNode(id), name("FileInputNode") {
  createPort<DependencyPort>(0, PortDirection::In);
  filePathPort = createPort<DataPort>(1, PortDirection::In);
  createPort<DependencyPort>(0, PortDirection::Out);
  stdoutPort = createPort<PipePort>(1, PortDirection::Out);
}

const std::string &FileInputNode::getName() const {
  return name;
}

std::string FileInputNode::getCommand(const WorkflowExecution *execution) const {
  auto path = filePathPort->getData();
  if (path && outgoingPipeConnection()) {
    return "cat " + execution->expandVariables(path->toString());
  }
  return {};
}

bool FileInputNode::outgoingPipeConnection() const {
  std::vector<CoherentConnectionInfo> outgoingCoherentConnections = getCoherentConnections(PortDirection::Out);
  return !outgoingCoherentConnections.empty() && outgoingCoherentConnections[0].connectionType != CoherentConnectionType::PIPE_TO_OTHER;
}

bool FileInputNode::processOutputPortData(WorkflowExecution *execution) {
  if (!outgoingPipeConnection()) {
    auto path_raw = filePathPort->getData();
    if (path_raw) {
      std::string path = util::expandString(execution->expandVariables(path_raw->toString()));
      try {
        std::string contents = util::readFile(path);
        stdoutPort->setData(std::make_shared<StaticData>(std::move(contents)));
      } catch (const std::invalid_argument &err) {
        LOG_NODE(err.what());
        return false;
      }
    }
  }
  return true;
}
