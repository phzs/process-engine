/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "CoherentNodeGroup.h"
#include "nodes/SystemCommandNode.h"
#include "ports/PipePort.h"
#include <sstream>
#include <stdexcept>

#include "nodes/FileOutputNode.h"

void CoherentNodeGroup::addCoherentNode(const SystemCommandNode *node, CoherentConnectionType type) {
  group.emplace_back(CoherentNodePtr{node, type});
}

std::vector<CoherentNodePtr> CoherentNodeGroup::getCoherentNodes() const {
  return group;
}

void CoherentNodeGroup::addStartNode(const SystemCommandNode *node) {
  if (!group.empty()) {
    throw std::logic_error("Internal error when creating coherent node group: The group is not empty");
  }
  group.emplace_back(CoherentNodePtr{node, CoherentConnectionType::NONE});
}

unsigned int CoherentNodeGroup::size() const {
  return group.size();
}

bool CoherentNodeGroup::empty() const {
  return group.empty();
}

std::unordered_set<std::string> CoherentNodeGroup::getNodeIds() const {
  std::unordered_set<std::string> nodeIds{};
  for (const auto &entry : group) {
    nodeIds.insert(entry.connectedNode->getId());
  }
  return nodeIds;
}

std::string CoherentNodeGroup::toString() const {
  std::ostringstream result;
  for (const auto &entry : group) {
    if (entry.connectionType == CoherentConnectionType::PIPE) {
      result << "PIPE";
    } else if (entry.connectionType == CoherentConnectionType::PIPE_TO_OTHER) {
      result << "PIPE TO OTHER";
    } else {
      result << "    "; // starting node has no connection type
    }
    result << " " << entry.connectedNode->getId() << "\n";
  }
  return result.str();
}

bool CoherentNodeGroup::operator==(const CoherentNodeGroup &rhs) const {
  return group.size() == rhs.group.size() && std::equal(group.begin(), group.end(), rhs.group.begin());
}

bool CoherentNodeGroup::operator!=(const CoherentNodeGroup &rhs) const {
  return !(rhs == *this);
}

std::unordered_set<std::string> CoherentNodeGroup::getGroupDependencies() const {
  std::unordered_set<std::string> result;
  // add up direct dependencies of coherent nodes in this group
  for (const auto &coherentNode : group) {
    auto coherentDependencies = coherentNode.connectedNode->getDirectDependencies();
    for (const auto &coherentDependency : coherentDependencies) {
      result.insert(coherentDependency);
    }
  }

  // erase ids of group members
  auto ignoreNodeIds = getNodeIds();
  if (!ignoreNodeIds.empty()) {
    for (const auto &ignoreNodeId : ignoreNodeIds) {
      auto toErase = result.find(ignoreNodeId);
      if (toErase != result.end()) {
        result.erase(toErase);
      }
    }
  }
  return result;
}

void CoherentNodeGroup::setPipeOutputPort(PipePort *portPtr) {
  pipeOutputPort = portPtr;
}

void CoherentNodeGroup::setPipeOutputData(std::shared_ptr<StaticData> data) {
  if (pipeOutputPort) {
    pipeOutputPort->setData(std::move(data));
  }
}

std::string CoherentNodeGroup::getCommand(const WorkflowExecution *execution) const {
  // start with the command of the first node in the coherent group (not necessarily this node!)
  auto coherentGroupNodes = getCoherentNodes();
  auto nextGroupEntry = coherentGroupNodes.begin();
  const SystemCommandNode *firstNode = (*nextGroupEntry).connectedNode;
  if (!execution->getNodeState(firstNode->getId()).stillToProcess()) {
    firstNode = (*(++nextGroupEntry)).connectedNode;
  }
  std::string command = firstNode->getCommand(execution);

  // iterate over the "following nodes" (all coherent nodes but the first)
  for (; nextGroupEntry != coherentGroupNodes.end(); ++nextGroupEntry) {
    const SystemCommandNode *nextNode = (*nextGroupEntry).connectedNode;
    CoherentConnectionType nextConnectionType = (*nextGroupEntry).connectionType;
    if (nextNode->getId() == firstNode->getId())
      continue; // first node was already processed before the loop!

    if (!execution->getNodeState(nextNode->getId()).stillToProcess())
      continue; // ignore nodes that should not be executed (for example if their state is TO_BE_SKIPPED)

    // assume all dependencies are resolved
    const auto outputNode = dynamic_cast<const FileOutputNode *>(nextNode);
    std::string commandConnector;
    if (outputNode && nextConnectionType == CoherentConnectionType::PIPE) {
      commandConnector = outputNode->getPipeConnector();
    } else if (nextConnectionType == CoherentConnectionType::PIPE) {
      commandConnector = " | ";
    } else {
      throw std::logic_error("Unable to find correct method to concatenate coherent node commands");
    }
    const auto systemCommandNode = dynamic_cast<const SystemCommandNode *>(nextNode);
    if (systemCommandNode) {
      command += commandConnector;
      command += systemCommandNode->getCommand(execution);
    }
  }
  return command;
}

bool CoherentNodePtr::operator==(const CoherentNodePtr &rhs) const {
  return connectedNode == rhs.connectedNode &&
         connectionType == rhs.connectionType;
}

bool CoherentNodePtr::operator!=(const CoherentNodePtr &rhs) const {
  return !(rhs == *this);
}
