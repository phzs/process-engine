/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "AbstractWorkflow.h"
#include "nodes/control/BranchingNode.h"
#include "state/NodeState.h"
#include <utility>

void AbstractWorkflow::initBranches() {
  auto nodes = getNodes();
  std::unordered_set<std::string> branchingNodeIds;
  for (const auto &node : nodes) {
    auto branchingNode = dynamic_cast<BranchingNode *>(node.second);
    if (branchingNode) {
      branchingNode->initBranches();
    }
  }
}

unsigned int AbstractWorkflow::getNodesProcessed() const {
  unsigned int result = 0;
  for (const auto &node : getNodesConst()) {
    result += node.second->getNodesProcessed();
  }
  return result;
}

unsigned int AbstractWorkflow::getNodesProcessedInLoop() const {
  unsigned int result = 0;
  for (const auto &node : getNodesConst()) {
    result += node.second->getNodesProcessedInLoop();
  }
  return result;
}

unsigned int AbstractWorkflow::getNodesTotal() const {
  unsigned int result = 0;
  for (const auto &node : getNodesConst()) {
    result += node.second->getNodesTotal();
  }
  return result;
}

void AbstractWorkflow::setNodeState(const std::string &nodeId, NodeState newState) const {
  getNode(nodeId)->setState(std::move(newState));
}

void AbstractWorkflow::resetNodeStates() {
  for (const auto &nodeEntry : getNodes()) {
    const std::string &nodeId = nodeEntry.first;
    Node *node = nodeEntry.second;

    node->resetState();
  }
}

void AbstractWorkflow::skipUnterminatedNodes() {
  for (const auto &nodeEntry : getNodes()) {
    Node *node = nodeEntry.second;

    NodeState &state = node->getState();
    if (state.stillToProcess() || state.isRunning()) {
      state.skip();
    }
  }
}
