/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "../Logger.h"
#include "Workflow.h"
#include "nodes/FileOutputNode.h"
#include <iostream>
#include <set>

#include "CoherentNodeGroup.h"
#include "WorkflowAnalyzer.h"
#include "WorkflowBranch.h"
#include "nodes/control/BranchingNode.h"
#include "ports/BranchingPort.h"
#include "ports/CoherencyPort.h"
#include "ports/PipePort.h"
#include "state/WorkflowExecution.h"


WorkflowAnalyzer::WorkflowAnalyzer(AbstractWorkflow *workflow)
    : workflow(workflow) {
  if (!workflow) {
    throw std::invalid_argument("Workflow must be non-null!");
  }
  _nodes = workflow->getNodesConst();
}

std::shared_ptr<CoherentNodeGroup> WorkflowAnalyzer::findCoherentNodeGroup(const std::string &nodeId) const {

  auto result = std::make_shared<CoherentNodeGroup>();
  const std::string invalidTypeErrorMessage = "Invalid node type connected via coherent connection";
  const std::string unsupportedTopologyErrorMessage = "Erroneous workflow topology:\n"
                                                      "Only one outgoing coherent connection is allowed per node!\n"
                                                      "- Do not connect both ENV and STDOUT ports on the same node\n"
                                                      "- Do not connect ENV or STDOUT ports to more than one node";

  auto nodes = workflow->getNodesConst();
  std::vector<CoherentConnectionInfo> tailConnections;
  std::vector<CoherentConnectionInfo> headConnections;
  std::string currentNodeId = nodeId;
  std::string lastGroupNodeId;

  // 1. go to the beginning of the chain of piped nodes
  do {
    headConnections = nodes[currentNodeId]->getCoherentConnections(PortDirection::In);
    if (headConnections.size() > 1) throw std::logic_error(unsupportedTopologyErrorMessage);
    if (headConnections.size() == 1) {
      CoherentConnectionType connectionType = headConnections[0].connectionType;
      if (connectionType == CoherentConnectionType::INVALID) {
        throw std::logic_error("Invalid coherent connection type");
      }
      if (connectionType == CoherentConnectionType::PIPE_TO_OTHER) {
        // for now only follow "pure" connections to the left, pure means ENV-ENV or PIPE-PIPE
        break;
      }
      // continue to traverse to the left
      currentNodeId = headConnections[0].connectedNodeId;
    }
  } while (!headConnections.empty());

  // 2. add the start node
  auto startNode = dynamic_cast<const SystemCommandNode *>(nodes[currentNodeId]);
  if (startNode) {
    result->addStartNode(startNode);
  } else {
    throw std::logic_error(invalidTypeErrorMessage);
  }

  // 3. go to the end of the chain, add each found node to the result
  do {
    tailConnections = nodes[currentNodeId]->getCoherentConnections(PortDirection::Out);
    if (tailConnections.size() > 1) throw std::logic_error(unsupportedTopologyErrorMessage);
    if (tailConnections.size() == 1) {
      lastGroupNodeId = currentNodeId;
      currentNodeId = tailConnections[0].connectedNodeId;
      /* possible check: is this node in the next execution set? (but no real need to check because
       * an error will be thrown later if the node will be processed but has unresolved dependencies */
      auto followingNode = dynamic_cast<const SystemCommandNode *>(nodes[currentNodeId]);

      if (followingNode && (tailConnections[0].connectionType == CoherentConnectionType::PIPE)) {
        result->addCoherentNode(followingNode, tailConnections[0].connectionType);
      } else {
        /* if the next Node is no SystemCommandNode, or the ports of the connection are not both of type pipe or
         * environment, the group ends here */
        break;
      }
    }
  } while (!tailConnections.empty());

  // Find the stdout output port of the group , so that other nodes can obtain it from the connection.
  if (nodes[lastGroupNodeId]) {
    std::vector<PipePort *> lastNodesOutgoingPipePorts = nodes[lastGroupNodeId]->getPortsByType<PipePort>(
      PortDirection::Out);
    if (lastNodesOutgoingPipePorts.size() > 1) {
      LOG("Warning: The last node in the node group has multiple pipe outputs. Using the first one for stdout.");
    }
    if (!lastNodesOutgoingPipePorts.empty()) {
      result->setPipeOutputPort(lastNodesOutgoingPipePorts[0]);
    }
  }
  if (result->size() > 1) {
    LOG_VERBOSE("analyzer", "\tfound coherent group:\n"
                              << result->toString());
    return std::move(result);
  } else {
    return {};
  }
}

std::unique_ptr<WorkflowBranch>
WorkflowAnalyzer::findBranch(AbstractWorkflow *parent, BranchingNode *originNode, int startPortIndex) {
  if (!originNode) {
    throw std::invalid_argument("Node does not exist in the workflow");
  }
  const Port *port = originNode->getPort(PortDirection::Out, startPortIndex);
  if (!port) {
    throw std::invalid_argument("Target node does not have an outgoing port on index " + std::to_string(startPortIndex));
  }
  auto rootPort = dynamic_cast<const BranchingPort *>(port);
  if (!rootPort) {
    throw std::invalid_argument("Can only create branches originating from a port of type BranchingPort");
  }

  auto relevantDependents = rootPort->getDependents();
  std::unordered_map<Node *, unsigned int> result; // nodeId of the branch (key), recursion depth (value)
  for (Node *dependent : relevantDependents) {
    std::unordered_map<Node *, unsigned int> branchNodes = findBranchRecursively(dependent);
    result.insert(branchNodes.begin(), branchNodes.end());
  }

  auto branch = std::make_unique<WorkflowBranch>(parent, originNode, rootPort);
  for (const auto &resultNodeEntry : result) {
    branch->addNode(resultNodeEntry.first->getId());
    parent->getNode(resultNodeEntry.first->getId())->setBranchId(resultNodeEntry.second, originNode, branch->getIdentifier());
  }
  return std::move(branch);
}

std::unordered_map<Node *, unsigned int> WorkflowAnalyzer::findBranchRecursively(Node *node,
                                                                                 unsigned int currentRecursionDepth) {
  if (currentRecursionDepth > config::MAX_BRANCH_RECURSION_DEPTH) {
    throw std::logic_error("Maximum recursion depth exceeded while finding workflow branches.");
  }
  if (!node) {
    throw std::invalid_argument("Node does not exist");
  }

  // 1. add this node, it is part of the branch
  std::unordered_map<Node *, unsigned int> result;
  result.insert({node, currentRecursionDepth});

  // 2. recursively follow all explicit dependencies (means: connected via outgoing dependency connection)
  auto explicitDependents = node->getDirectDependents();
  for (Node *explicitDependent : explicitDependents) {
    auto branch = findBranchRecursively(explicitDependent, currentRecursionDepth + 1);
    result.insert(branch.begin(), branch.end());
  }

  return result;
}

std::vector<std::string> WorkflowAnalyzer::getStartNodes(const std::unordered_map<std::string, const Node *> &nodes, WorkflowBranch *branch) {
  std::vector<std::string> startNodes;
  for (const auto &entry : nodes) {
    const Node *node = entry.second;
    if (!node->toBeExecuted()) {
      continue;
    }
    const NodeState &nodeState = node->getState();
    if (branch && !branch->isInBranch(node->getId())) {
      continue;
    }
    if (nodeState.tokenCount() == 0) {
      startNodes.emplace_back(node->getId());
    }
  }
  return startNodes;
}
