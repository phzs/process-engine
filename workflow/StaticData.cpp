/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "state/WorkflowExecution.h"
#include <utility>

#include "../config.h"
#include "StaticData.h"
#include "state/WorkflowExecution.h"

StaticData::StaticData()
    : empty(true) {}

StaticData::StaticData(bool data)
    : empty(false), raw_data(data ? config::TRUE_STRING_REPR : config::FALSE_STRING_REPR) {}

StaticData::StaticData(double data)
    : empty(false), raw_data(std::to_string(data)) {}

StaticData::StaticData(std::string data)
    : empty(data.empty()), raw_data(std::move(data)) {}

StaticData::StaticData(int data)
    : empty(false), raw_data(std::to_string(data)) {}

StaticData::StaticData(float data)
    : empty(false), raw_data(std::to_string(data)) {}

StaticData::StaticData(const char *data)
    : empty(false), raw_data(std::string(data)) {}

bool StaticData::isEmpty() const {
  return empty;
}

bool StaticData::toBoolean() const {
  return stringToBool(raw_data);
}

std::string StaticData::toString() const {
  return raw_data;
}

int StaticData::toInt() const {
  return stringToInt(raw_data);
}

float StaticData::toFloat() const {
  return stringToFloat(raw_data);
}

int StaticData::expandInt(WorkflowExecution *execution) const {
  std::string expanded_data = execution->expandVariables(raw_data);
  return stringToInt(expanded_data);
}

bool StaticData::expandBool(WorkflowExecution *execution) const {
  std::string expanded_data = execution->expandVariables(raw_data);
  return stringToBool(expanded_data);
}

int StaticData::stringToInt(const std::string &value) {
  try {
    char *end;
    int result = static_cast<int>(std::strtol(value.c_str(), &end, 10));
    if (*end) {
      throw std::invalid_argument("The following part could not be converted: \"" + std::string(end) + "\".");
    }
    return result;
  } catch (std::exception &error) {
    throw std::invalid_argument("Unable to convert \"" + value + "\" into integer: " + error.what());
  }
}

float StaticData::stringToFloat(const std::string &value) {
  try {
    char *end;
    float result = std::strtof(value.c_str(), &end);
    if (*end) {
      throw std::invalid_argument("The following part could not be converted: \"" + std::string(end) + "\".");
    }
    return result;
  } catch (std::exception &error) {
    throw std::invalid_argument("Unable to convert \"" + value + "\" into float: " + error.what());
  }
}

bool StaticData::stringToBool(const std::string &stringValue) {
  auto matchAny = [](const std::initializer_list<const std::string_view> &vec, const std::string &str) {
    return std::find(vec.begin(), vec.end(), std::string_view(str)) != vec.end();
  };

  if (matchAny(config::VALID_TRUE_STRING_REPRESENTATIONS, stringValue)) {
    return true;
  } else if (matchAny(config::VALID_FALSE_STRING_REPRESENTATIONS, stringValue)) {
    return false;
  } else {
    std::stringstream errorMessage;
    errorMessage << "Unable to convert \"" << stringValue << "\" into bool. \nHint: Valid values for true are: ";
    for (const auto &value : config::VALID_TRUE_STRING_REPRESENTATIONS)
      errorMessage << "\"" << value << "\"" << ',';
    errorMessage << " and valid values for false are: ";
    for (const auto &value : config::VALID_FALSE_STRING_REPRESENTATIONS)
      errorMessage << "\"" << value << "\"" << ',';
    throw std::invalid_argument(errorMessage.str().substr(0, errorMessage.str().size() - 1));
  }
}
