/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once

#include "workflow/Workflow.h"
#include "workflow/WorkflowExecutor.h"
#include <nlohmann/json.hpp>
#include <string>

using json = nlohmann::json;

class ProcessEngine {
public:
  ProcessEngine() = default;
  ~ProcessEngine() noexcept = default;

  void setPath(const std::string &executionFolder);

  bool runWorkflow(const std::string &workflowFile, bool dry, bool noColors);
  static bool continueWorkflow();
  static void printWorkflowStatus();

  static void printWorkflowLog();
  static void printInteractions();
  static bool printTree();

  static void printWorkflowShortcuts();
  static bool inputInteractionValue(const std::string &interactionId, const std::string &inputValue);

  static bool cancelWorkflow(unsigned int timeout_seconds);

  void signalHandler(int signal);

  static void printLogFilePath();
  static void printTreeFilePath();

private:
  bool createFolders();
  std::string path;
  std::unique_ptr<Workflow> workflow;
  std::unique_ptr<WorkflowExecution> currentExecution;
  WorkflowExecutor executor;

  static void setDefaultVariables(WorkflowExecution *execution);
  static json restoreStatusJson();
  static bool signalRunningEngine(int signal);
  static pid_t getPidOfRunningEngine();
  static void sendSignal(pid_t pid, int signal);
};
