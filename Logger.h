/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once

#include "config.h"
#include <fstream>
#include <iostream>
#include <mutex>
#include <sstream>
#include <string>

#define LOG_(Logger_, Context_, Message_)             \
  Logger_(Context_,                                   \
          dynamic_cast<std::ostringstream &>(         \
            std::ostringstream().flush() << Message_) \
            .str())
#define LOG(Message_) LOG_(GetLogger(), std::string(config::MAIN_LOG_CONTEXT), Message_)
#define LOG_CONTEXT(Context_, Message_) LOG_(GetLogger(), Context_, Message_)
#define LOG_NODE(Message_) LOG_(GetLogger(), this->getContext(), Message_)
#define LOG_VERBOSE(Context_, Message_) \
  if (GetLogger().getVerbose()) LOG_(GetLogger(), Context_, "[VERBOSE] " << Message_)
#define LOG_NODE_VERBOSE(Message_) LOG_VERBOSE(this->getContext(), Message_)

struct Color;

class Logger {
public:
  Logger();
  void operator()(const std::string &context, const std::string &message);
  void setPrintStdout(bool printStdout);
  void setPrintLog(bool printLog);
  void setVerbose(bool verbose);
  void setUseTerminalColors(bool useColors);
  void setContextWidth(size_t width);
  [[nodiscard]] bool getVerbose() const;

  [[nodiscard]] const std::string &getLogFilePath() const;

private:
  static void adjustBrightness(Color &color, int minBrightness);
  static void stringToRgb(const std::string &str, Color &color);
  template<typename T>
  void printLogLine(T &&stream, const Color &color, const std::string &context, const std::string &message);

  void log(const std::string &context, const std::string &message);

  std::string logFilePath;
  bool printStdout;
  bool printLog;
  bool verbose;
  bool useTerminalColors;
  size_t contextWidth;

  std::mutex logMutex;
};

Logger &GetLogger();
