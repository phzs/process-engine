/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "Logger.h"
#include "config.h"
#include "util.h"
#include <fstream>
#include <thread>
#include <unistd.h>

#include "ProcessEngine.h"
#include "workflow/WorkflowExecutor.h"
#include "workflow/parse/JsonParser.h"
#include "workflow/state/WorkflowExecution.h"

using json = nlohmann::json;


bool ProcessEngine::runWorkflow(const std::string &workflowFile, bool dry, bool noColors) {
  std::unordered_map<std::string, std::string> variables;
  try {
    JsonParser parser(workflowFile);
    workflow = parser.parseJsonWorkflow();
    GetLogger().setContextWidth(workflow->getLongestNodeContextSize());
    GetLogger().setUseTerminalColors(!noColors);
    variables = parser.parseVariables();
  } catch (const std::exception &e) {
    throw std::invalid_argument(std::string("Error while parsing given workflow: ") + e.what());
  }

  pid_t pid = getpid();

  if (!dry) {
    if (!createFolders()) {
      std::cerr << "Error: Unable to create the necessary folders for this workflow."
                   " No permission, invalid path or folder already existing?"
                << std::endl;
    }
  }

  currentExecution = std::make_unique<WorkflowExecution>(std::make_unique<FileInteractionManager>(std::string(config::INTERACTION_FILENAME)), workflow.get(), true);
  currentExecution->setVariables(variables);
  currentExecution->setFileName(workflowFile);
  currentExecution->setPid(pid);
  currentExecution->setState(ExecutionState::RUNNING);
  currentExecution->setStartDateTime(util::getCurrentDateTime());
  currentExecution->setDryRun(dry);

  setDefaultVariables(currentExecution.get());

  bool executionSuccess = false;
  try {
    executionSuccess = executor.execute(workflow.get(), currentExecution.get());
  } catch (std::exception &error) {
    LOG("Error while executing the workflow: " << error.what());
    currentExecution->setState(ExecutionState::ERROR);
  }
  LOG("");
  LOG(std::string(15, '-') << " EXECUTION RESULT " << std::string(15, '-'));
  if (executionSuccess) {
    LOG("OK: Workflow execution finished");
  } else {
    LOG("ERROR: Execution stopped on error");
  }
  LOG("New state: " << executionStateToString(currentExecution->getState()));
  LOG("Nodes to process: " << workflow->getNodesTotal());
  LOG("Nodes processed in Loops: " << workflow->getNodesProcessedInLoop());
  LOG("Nodes processed: " << workflow->getNodesProcessed());

  if (currentExecution->getState() == ExecutionState::FINISHED || currentExecution->getState() == ExecutionState::ERROR) {
    // workflow execution will not be continued
    if (!util::deleteFolder(std::string(config::TEMPORARY_FOLDER))) {
      LOG("WARNING: Unable to delete temp folder (" << config::TEMPORARY_FOLDER << ").");
    }
    currentExecution->setEndDateTime(util::getCurrentDateTime());
    currentExecution->persistInfo();
  }

  return executionSuccess;
}

bool ProcessEngine::continueWorkflow() {
  return signalRunningEngine(SIGUSR1);
}

void ProcessEngine::printWorkflowStatus() {
  // the infoFile is updated by the process which handles the execution
  // it can be read from the filesystem to get the requested information
  json currentStatus = restoreStatusJson(); // to get the pid

  if (currentStatus.contains(config::PID_KEY)) {
    pid_t pid_of_running_process_engine;
    pid_of_running_process_engine = currentStatus[std::string(config::PID_KEY)];
    ExecutionState state = stringToExecutionState(currentStatus["state"]);
    bool assume_process_running = (state == ExecutionState::NEEDS_INTERACTION || state == ExecutionState::RUNNING || state == ExecutionState::CANCELLING);
    if (assume_process_running && !util::processRunning(pid_of_running_process_engine)) {
      // no active process, set state to CANCELLED

      std::string cancelled = executionStateToString(ExecutionState::CANCELLED);
      currentStatus["state"] = cancelled;

      // since there is no other active process, it is safe to call LOG() and write to the status file
      GetLogger().setUseTerminalColors(false);
      LOG("** Process with PID " << pid_of_running_process_engine << " is not running, setting state to " << cancelled);
      std::string fileName = std::string(config::STATUS_FILENAME);
      std::ofstream out(fileName);
      out << std::setw(config::JSON_INDENT) << currentStatus << std::endl;
    }
  }

  std::cout << std::setw(config::JSON_INDENT) << currentStatus << std::endl;
}

bool ProcessEngine::createFolders() {
  bool success = true;

  if (!util::createDirectory(std::string(config::LOG_FOLDER))) {
    std::cerr << "Could not create log directory in path " << path << std::endl;
    success = false;
  }

  if (!util::createDirectory(std::string(config::TEMPORARY_FOLDER))) {
    std::cerr << "Could not create log directory in path " << path << std::endl;
    success = false;
  }

  return success;
}

void ProcessEngine::printWorkflowLog() {
  std::ostringstream stream;
  stream << std::string(config::LOG_FOLDER) << "/" << std::string(config::LOG_FILENAME);
  std::string logPath = stream.str();
  if (!logPath.empty() && util::fileExists(logPath)) {
    std::cout << util::readFile(logPath) << std::endl;
  }
}

void ProcessEngine::printWorkflowShortcuts() {
  if (!config::SHORTCUT_FILENAME.empty() && util::fileExists(std::string(config::SHORTCUT_FILENAME))) {
    std::cout << util::readFile(std::string(config::SHORTCUT_FILENAME)) << std::endl;
  }
}

void ProcessEngine::printInteractions() {
  std::string fileName = std::string(config::INTERACTION_FILENAME);
  if (!fileName.empty() && util::fileExists(fileName)) {
    std::cout << util::readFile(fileName) << std::endl;
  }
}

bool ProcessEngine::printTree() {
  std::string fileName = std::string(config::TREE_FILENAME);
  if (!fileName.empty() && util::fileExists(fileName)) {
    std::cout << util::readFile(fileName) << std::endl;
    return true;
  }
  return false;
}

bool ProcessEngine::inputInteractionValue(const std::string &interactionId, const std::string &inputValue) {
  bool valueWritten = false;
  std::string fileName = std::string(config::INTERACTION_FILENAME);
  std::ifstream in(fileName);
  json inJsonObject, outJsonObject;
  try {
    in >> inJsonObject;
    if (inJsonObject.contains(config::INTERACTION_ARRAY_KEY) && inJsonObject[std::string(config::INTERACTION_ARRAY_KEY)].is_array()) {
      // copy each interaction json object, insert the value in case the one matching id was found
      for (const auto &interactionObj : inJsonObject[std::string(config::INTERACTION_ARRAY_KEY)]) {
        if (interactionObj.contains("id") && interactionObj["id"].get<std::string>() == interactionId) {
          json interactionObjCopy = interactionObj;
          if (interactionObjCopy["type"] == "int") {
            interactionObjCopy["value"] = std::stoi(inputValue);
          } else if (interactionObjCopy["type"] == "float") {
            std::string value(inputValue);
            std::replace(value.begin(), value.end(), ',', '.');
            interactionObjCopy["value"] = std::stof(value);
          } else if (interactionObjCopy["type"] == "bool") {
            if (inputValue == "true" || inputValue == "True") {
              interactionObjCopy["value"] = true;
            } else {
              interactionObjCopy["value"] = false;
            }
          } else if (interactionObjCopy["type"] == "form") {
            json formJson = json::parse(inputValue);
            interactionObjCopy["value"] = formJson;
          } else {
            interactionObjCopy["value"] = inputValue;
          }
          if (valueWritten) {
            std::cerr << "Interaction id matched more than one entries" << std::endl;
            return false;
          }
          outJsonObject[std::string(config::INTERACTION_ARRAY_KEY)].push_back(interactionObjCopy);
          valueWritten = true;
        } else {
          outJsonObject[std::string(config::INTERACTION_ARRAY_KEY)].push_back(interactionObj);
        }
      }
    }
  } catch (json::exception &e) {
    std::cerr << "Error while parsing workflow interactions: " << e.what() << std::endl;
    return false;
  }
  in.close();
  if (valueWritten) {
    std::ofstream out(fileName);
    out << std::setw(config::JSON_INDENT) << outJsonObject << std::endl;
  } else {
    std::cerr << "Interaction with given id not found" << std::endl;
  }
  return valueWritten;
}

void ProcessEngine::setDefaultVariables(WorkflowExecution *execution) {
  if (!execution) throw std::invalid_argument("Execution not initialized properly");
  execution->setVariable("TMP_FOLDER", std::string(config::TEMPORARY_FOLDER), false);
}

bool ProcessEngine::cancelWorkflow(unsigned int timeoutSeconds) {
  json restoredStatusJson = restoreStatusJson();
  pid_t pid = getPidOfRunningEngine();
  if (pid <= 0) {
    std::cerr << "Warning: Invalid PID value."
              << " Unable to check if the process terminated and unable to "
                 "enforce termination."
              << std::endl;
    return false;
  }
  // SIGTERM tells the running process to stop starting new nodes, and to gracefully shutdown as soon as possible
  sendSignal(pid, SIGTERM);

  using namespace std::chrono_literals;
  std::chrono::duration elapsed = 0ms;
  std::chrono::duration timeout = std::chrono::seconds{timeoutSeconds};
  std::cout << "Waiting for the running process to shutdown gracefully... " << std::flush;
  bool killed = false;
  while (util::processRunning(pid)) {
    if (elapsed > timeout) {
      restoredStatusJson["state"] = executionStateToString(ExecutionState::CANCELLED);
      std::string fileName = std::string(config::STATUS_FILENAME);
      std::ofstream out(fileName);
      out << std::setw(config::JSON_INDENT) << restoredStatusJson << std::endl;
      killed = true;
      sendSignal(pid, SIGKILL);
      break;
    }
    std::chrono::duration sleepDuration = 100ms;
    std::this_thread::sleep_for(sleepDuration);
    elapsed += sleepDuration;
  }
  std::cout << "OK" << std::endl
            << (killed ? "Process was forcefully terminated due to timeout."
                       : "Process exited cleanly.")
            << std::endl;
  return true;
}

json ProcessEngine::restoreStatusJson() {
  json result;
  std::string fileName = std::string(config::STATUS_FILENAME);
  if (!util::fileExists(fileName)) {
    throw std::runtime_error("No workflow info available to restore (" + fileName + " not accessible)");
  }

  std::ifstream in(fileName);
  try {
    in >> result;
  } catch (json::exception &e) {
    throw std::runtime_error("Error while parsing workflow info to restore previous workflow state: " + std::string(e.what()));
  }
  return result;
}

bool ProcessEngine::signalRunningEngine(int signal) {
  try {
    sendSignal(getPidOfRunningEngine(), signal);
  } catch (const std::runtime_error &error) {
    std::cerr << "Unable to send signal to running process engine: " << error.what() << std::endl;
    return false;
  }
  return true;
}

pid_t ProcessEngine::getPidOfRunningEngine() {
  json restoredStatusJson;
  restoredStatusJson = restoreStatusJson();

  if (restoredStatusJson.contains(config::PID_KEY)) {
    return restoredStatusJson[std::string(config::PID_KEY)];
  } else {
    throw std::runtime_error("PID of running process engine not found in workflow info.");
  }
}

void ProcessEngine::sendSignal(pid_t pid, int signal) {
  int status = kill(pid, signal);
  if (status == -1) {     // kill syscall failed
    if (errno == ESRCH) { // no such process
      // the process engine process to send the signal to most likely died/crashed or was killed early
      std::stringstream stream;
      stream << "The process engine with PID " << pid << " does not exist (exited early, crashed or was killed).";
      throw std::runtime_error(stream.str());
    }
  }
}

void ProcessEngine::signalHandler(int signal) {
  if (signal == SIGTERM) {
    LOG("** SIGTERM received, attempting graceful shutdown of the workflow execution **");
    currentExecution->cancel();
  } else if (signal == SIGUSR1) {
    LOG("** SIGUSR1 received, notifying user interaction nodes to continue **");
    InteractionManager *interactionManager = currentExecution->getInteractionManager();
    interactionManager->pageFinished();
    executor.addNodesToExecute(interactionManager->updateAndNotify());
  }
}

void ProcessEngine::setPath(const std::string &executionFolder) {
  path = executionFolder;
}

void ProcessEngine::printLogFilePath() {
  std::cout << util::getAbsoluteFilePath(GetLogger().getLogFilePath()) << std::endl;
}

void ProcessEngine::printTreeFilePath() {
  std::string fileName = std::string(config::TREE_FILENAME);
  std::cout << util::getAbsoluteFilePath(fileName) << std::endl;
}
