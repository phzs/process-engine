/* Copyright 2021 Karlsruhe Institute of Technology
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License. */

#include "config.h"

namespace config {
  // name of the file to store the workflow status in (will be created in the execution directory)
  extern const std::string_view STATUS_FILENAME = ".workflow-status.json";
  extern const std::string_view TREE_FILENAME = ".workflow-tree.json";

  // output options
  extern const int JSON_INDENT = 4;

  // logging options
  extern const std::string_view LOG_FOLDER = "log"; // no '/' expected at the end
  extern const std::string_view LOG_FILENAME = "execution.log";
  extern const std::string_view COMMAND_LOG_FILENAME = "commands.log";
  extern const char LOG_SEPARATOR = '-';
  extern const int LOG_SEPARATOR_NUM_SHORT = 120;
  extern const int LOG_SEPARATOR_NUM_LONG = 200;
  extern const int LOG_COLOR_MIN_BRIGHTNESS = 100;
  extern const std::string_view MAIN_LOG_CONTEXT = "Process Engine";

  // shortcuts feature
  extern const std::string_view SHORTCUT_FILENAME = ".workflow-shortcuts.json";

  // interaction feature
  extern const std::string_view INTERACTION_FILENAME = ".workflow-interactions.json";
  extern const std::string_view INTERACTION_ARRAY_KEY = "interactions";

  // continuing workflows
  extern const std::string_view NODE_STATES_KEY = "node_states";
  extern const std::string_view VARIABLES_KEY = "variables";
  extern const std::string_view PID_KEY = "pid";

  // variable feature
  extern const std::string_view VARIABLE_REGEX = R"(\$\{([a-zA-Z0-9-_\.: ]+)\})";
  extern const std::string_view VARIABLE_VALUE_UNDEFINED("[UNDEFINED]");
  extern const int MAX_VAR_REPLACEMENTS = 100;
  extern const std::string_view VARIABLE_JSON_DEFAULT_KEY = "data";

  // temporary folder
  extern const std::string_view TEMPORARY_FOLDER = "tmp";

  // workflow analysis
  extern const int MAX_BRANCH_RECURSION_DEPTH = 10000;
  extern const int MAX_DEPENDENCY_SCAN_ROUNDS = 10000;

  // loops
  extern const std::string_view DEFAULT_LOOP_VARIABLE("index");

  // data and converting
  extern const std::string_view TRUE_STRING_REPR = "1";  // string used for bool value true internally
  extern const std::string_view FALSE_STRING_REPR = "0"; // string used for bool value false internally

  extern const std::initializer_list<const std::string_view> VALID_TRUE_STRING_REPRESENTATIONS = {
    TRUE_STRING_REPR,
    std::string_view("true"),
    std::string_view("True"),
    std::string_view("TRUE")};
  extern const std::initializer_list<const std::string_view> VALID_FALSE_STRING_REPRESENTATIONS = {
    FALSE_STRING_REPR,
    std::string_view("false"),
    std::string_view("False"),
    std::string_view("FALSE")};

  extern const unsigned int CANCEL_TIMEOUT_DEFAULT = 5;
} // namespace config
