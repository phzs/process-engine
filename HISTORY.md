# Release history

## 0.12.0 (2023-09-18)
* Changed to automatic parallel execution
* Added subcommand log_path to retrieve the path to the log file
* Added flag `--no-color` to disable colorized output
* Extended logging: Each line now contains context information and is colored based on the context
* Added subcommand `tree` to retrieve tree status information, which is useful for displaying the log output in a structured way
* Added subcommand `tree_path` similar to log_path, returning the path to the file containing the status tree information
* Added x position of nodes to the tree command as attribute "order" to allow sorting of the tree view in Kadistudio's log window

## 0.11.1 (2023-06-26)
* Fixed missing single quotes around parameter values which are not quoted but contain double quotes
* Fixed bug with LoopNode when using a variable as exit condition

## 0.11.0 (2023-06-16)
* Added subcommand log_path to retrieve the path to the log file
* Added multiline flag for UserInputText node

## 0.10.2 (2023-03-21)
* Fixed bug with UserInputFormNode

## 0.10.1 (2023-02-28)

* Fixed issue with user interactions shown multiple times when in nested branches
* Fixed some issues with interactions together with nested loops/branches

## 0.10.0 (2023-02-21)

* Fixed issues with packaging
* Implemented VariableJsonNode which allows setting multiple variables from json
* Updated debian packaging
* Updated CI config
* Added bugfixes

## 0.9.0 (2023-01-25)

* Added a value output port for ChooseNode
* Added VariableListNode that allows to set multiple variables at once
* Added UserInputFormNode, a special node to ask multiple inputs at once and is compatible with kadi templates
* Updated CI config

## 0.8.0 (2022-11-22)

* Removed option --detach for starting in the background
  * Detaching the process is now a responsibility of the Process Manager (or any other application or framework which calls the Process Engine).
* Added a timeout for cancelling workflows and a CLI option to control its length. After the timeout, the running process is killed.
  * Also set state to CANCELLED when PID is not running
* Added log message when state is set to Cancelled by status subcommand
* Added code refactorings
* Added parsing for variables in the .flow file
* Added UserInputSelectNode, a node to let the user select a value from a list of values
* Improved logging
* Removed restriction that values of workflow variables must not contain other variables
* Added bugfixes
* Updated code style

## 0.7.0 (2022-09-29)

* The node UserInputCropImagesNode is now deprecated
* Added new node UserInputSelectBounding which behaves like UserInputCropImageNode but has four separate int output ports
* Added new node FormatStringNode, allowing to create a formatted string in the workflow using a format string and multiple input values

## 0.6.0 (2022-09-21)

* Updated CLI commands / usage:
  * The subcommand to run workflows is now called 'start' to make the interface equal to the process manager. 'run' remains as an alias so that old process manager configurations still work
  * There is now a separate subcommand 'continue', the old --continue flag of the run command will be deprecated but still works
  * It is recommended to update the process manager configuration, see README.md to get the updated default configuration
* Fixed bug with interaction node in loop branch

## 0.5.1 (2022-08-23)

* Fixed bug with loop exit condition
* Improved usability regarding --xmlhelp
* Fixed exit code when cancel subcommand runs successfully

## 0.5.0 (2022-04-12)

* Implemented CLI options --commands and --xmlhelp
* Added functionality to gracefully cancel workflow execution
* Added signal/wait functionality for user interactions

## 0.4.0 (2021-11-09)

* Implemented interaction node to select elements from the periodic table
* Implemented support for the execution profile "detached"
* Implemented command log
* Improved logging for variables
* Added bugfixes

## 0.3.2 (2021-09-16)

* Introduced support for basic node execution profiles (supported profiles: default and skip)
* Added version flag to CLI

## 0.3.1 (2021-07-21)

* Added bugfix regarding branch selection
* Reworked environment feature
* Added checks against invalid port configurations

## 0.3.0 (2021-04-30)

* Implemented conversion from stdout outputs to string inputs
* Implemented functionality for default values for interaction nodes
* Implemented WebView interaction node
* Added support for relative file paths for CLI parameters
* Added various bug fixes
* Added updates for readme, CI configuration, code quality etc.

## 0.2.0 (2021-03-18)

* Implemented LoopNode
* Implemented UserInputChooseNode
* Implemented BranchSelectNode
* Variables are now supported in SourceNodes independently of their type
* Updated FileOutputNode
* Added various bug fixes

## 0.1.0 (2021-01-15)

* Basic execution engine for workflows implemented.
