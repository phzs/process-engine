/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "TestInteractionManager.h"
#include "../workflow/WorkflowExecutor.h"

using namespace std::chrono_literals;

TestInteractionManager::TestInteractionManager(WorkflowExecutor *executor)
    : InteractionManager(executor), runner([this] { executeRunner(); }) {
}

TestInteractionManager::~TestInteractionManager() {
  running = false;
  mutex.unlock();
  if (runner.joinable()) runner.join();
}

void TestInteractionManager::insertInteractionValue(Node *node, const json &value) {
  if (!node) throw std::invalid_argument("Issuing node must not be NULL, otherwise the notify mechanism won't work");
  std::unique_lock<std::mutex> lock(mutex);
  interactionValues[node].push(value);
}

std::vector<unsigned int> TestInteractionManager::updateValues() {
  if (interactionValues.empty()) return {};
  std::unique_lock<std::mutex> lock(mutex);
  std::vector<unsigned int> updatedInteractions;

  processInteractions([this, &updatedInteractions](AbstractInteraction *interaction) {
    Node *issuingNode = interaction->getIssuingNode();
    if (interactionValues.find(issuingNode) != interactionValues.end() && !interactionValues[issuingNode].empty()) {
      json value = interactionValues[issuingNode].front();
      interactionValues[issuingNode].pop();
      interaction->setValue(value);

      updatedInteractions.push_back(interaction->getId());
    }
  });
  return updatedInteractions;
}

void TestInteractionManager::interactionAdded(const AbstractInteraction *interaction) {
}

void TestInteractionManager::executeRunner() {
  while (running) {
    executor->addNodesToExecute(updateAndNotify());
    std::this_thread::sleep_for(50ms);
  }
}
