/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "../TerminalColors.h"
#include "../util.h"
#include <catch2/catch.hpp>

using namespace util;

TEST_CASE("format string") {
  REQUIRE(formatString("[%0, %1, %2, %3]", {"foo", "bar", "0", "10"}) == "[foo, bar, 0, 10]");
  REQUIRE(formatString("<<%0, %0 - %1, %1>>", {"abc", "dfe"}) == "<<abc, abc - dfe, dfe>>");
  REQUIRE(formatString("%1%0, %1-%1", {"bar", "foo"}) == "foobar, foo-foo");
  REQUIRE(formatString("%1", {"bar", "foo"}) == "foo");
  REQUIRE(formatString("String: %0%1!", {"Hello, ", "World"}) == "String: Hello, World!");
  REQUIRE(formatString("%0 %0 %0 %0 %0 nice", {"very"}) == "very very very very very nice");

  std::vector<std::string> values;
  values.resize(20, "nothing"); // fill with 0s
  values[10] = "something";
  REQUIRE(formatString("%10", values) == "something");

  SECTION("corner cases") {
    REQUIRE(formatString("", {"0", "1"}) == ""); // NOLINT
    REQUIRE(formatString("", {}) == "");         // NOLINT
    REQUIRE(formatString("", {""}) == "");       // NOLINT
    REQUIRE(formatString("%0 %1", {"", ""}) == " ");
    REQUIRE(formatString("%-1", {"0", "0", "0"}) == "%-1");

    REQUIRE(formatString("foobar", {"0", "1"}) == "foobar");
    REQUIRE(formatString("%a%1", {"0", "1"}) == "%a1");
    REQUIRE(formatString("%%%0%%%%1", {"foo", "bar"}) == "%%foo%%%bar");
    REQUIRE(formatString("%0%1", {"%1", "%0"}) == "%1%0");
    REQUIRE_THROWS(formatString("%2", {"a", "b"}));
    REQUIRE_THROWS(formatString("%10", {}));
  }
}

TEST_CASE("string replace") {
  std::string test = "foo baz";
  stringReplace(test, "foo", "foo bar");
  REQUIRE(test == "foo bar baz");
  stringReplace(test, "a", "");
  REQUIRE(test == "foo br bz");
  REQUIRE_THROWS(stringReplace(test, "", "a"));
  test = "";
  stringReplace(test, "b", "a");
  REQUIRE(test.empty());

  std::string no_color = std::string(ANSI_NO_COLOR);
  test = "test with " + beginANSIColor(Color{255, 0, 0}) + "COLOR" + no_color;
  stringReplace(test, no_color, "");
  REQUIRE(test.find(no_color) == std::string::npos);
}
