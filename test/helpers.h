/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once

#include <memory>
#include <string>

#include "../workflow/nodes/Node.h"

namespace helpers {
  /**
   * Creates a simple dummy ToolNode without any parameters, just dependency and pipe ports (optionally env ports)
   */
  std::unique_ptr<Node> createSimpleToolNode(const std::string &id, bool addEnvPorts = false);
  /**
   * Obtain a branch pointer from "outside" a workflow
   * @param workflow Target workflow
   * @param node_id Id of target node
   * @param portIndex Index of target branching port
   * @return const pointer to branch
   */
  const WorkflowBranch *obtainBranchFromNode(const Workflow *workflow, const std::string &node_id, int portIndex);
} // namespace helpers
