/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "../workflow/Workflow.h"
#include "../workflow/WorkflowAnalyzer.h"
#include "../workflow/nodes/Node.h"
#include "../workflow/nodes/SystemCommandNode.h"
#include "../workflow/nodes/ToolNode.h"
#include "TestExecution.h"
#include "helpers.h"
#include <catch2/catch.hpp>

TEST_CASE("Test detecting of dependencies for a coherent group of nodes") {
  Workflow workflow;

  /**
   * [A]           [F]
   *  \              \
   *  [B] @@@ [E] @@@ [G] @@@ [H]
   *         /
   * [C] - [D]
   *
   * -: dependency connection
   * @: pipe connection
   */
  auto nodeIndices = {"A", "B", "C", "D", "E", "F", "G", "H"};
  for (const auto &nodeIndex : nodeIndices) {
    workflow.addNode(helpers::createSimpleToolNode(nodeIndex, true));
  }
  int dependencyIndex = 0;
  workflow.connectPorts("A", dependencyIndex, "B", dependencyIndex);
  workflow.connectPorts("C", dependencyIndex, "D", dependencyIndex);
  workflow.connectPorts("D", dependencyIndex, "E", dependencyIndex);
  workflow.connectPorts("F", dependencyIndex, "G", dependencyIndex);

  int pipeIndex = 1;
  workflow.connectPorts("E", pipeIndex, "G", pipeIndex);
  workflow.connectPorts("G", pipeIndex, "H", pipeIndex);
  workflow.connectPorts("B", pipeIndex, "E", pipeIndex);

  auto workflowAnalyzer = WorkflowAnalyzer(&workflow);
  // make the coherentGroup known to the nodes
  auto nodes = workflow.getNodes();
  for (const auto &node : nodes) {
    auto sysNode = dynamic_cast<SystemCommandNode *>(node.second);
    REQUIRE(sysNode);
    sysNode->setCoherentGroup(workflowAnalyzer.findCoherentNodeGroup(sysNode->getId()));
  }


  SECTION("Members of the coherent group must return the dependencies of the group as a conjunction") {
    for (const auto &nodeIndex : {"B", "E", "G", "H"}) {
      auto node = nodes[nodeIndex];
      auto allDependencies = node->getAllDependencies();
      auto isIncluded = [&allDependencies](const std::string &id) {
        return allDependencies.find(id) != allDependencies.end();
      };

      for (const auto &requiredIndex : {"A", "D", "F"}) {
        REQUIRE(isIncluded(requiredIndex));
      }
      for (const auto &invalidIndex : {"B", "E", "G", "H"}) {
        // the result must not contain group members
        REQUIRE_FALSE(isIncluded(invalidIndex));
      }
    }
  }

  SECTION("Starting nodes must return no dependencies at all") {
    for (const auto &nodeIndex : {"A", "F", "C"}) {
      auto node = nodes[nodeIndex];
      REQUIRE(node->getAllDependencies().empty());
    }
  }

  SECTION("D must only have a dependency to C") {
    auto dependenciesOfB = nodes["D"]->getAllDependencies();
    REQUIRE(dependenciesOfB.find("C") != dependenciesOfB.end());
    REQUIRE(dependenciesOfB.size() == 1);
  }
}

TEST_CASE("Test command for coherent node groups") {
  Workflow workflow;
  auto nodeA = helpers::createSimpleToolNode("A");
  auto nodeB = helpers::createSimpleToolNode("B");
  auto nodeC = helpers::createSimpleToolNode("C");
  dynamic_cast<ToolNode *>(nodeA.get())->setToolPath("CommandA");
  dynamic_cast<ToolNode *>(nodeB.get())->setToolPath("CommandB");
  dynamic_cast<ToolNode *>(nodeC.get())->setToolPath("CommandC");
  workflow.addNode(std::move(nodeA));
  workflow.addNode(std::move(nodeB));
  workflow.addNode(std::move(nodeC));
  TestExecution execution(&workflow);

  workflow.connectPorts("A", 0, "B", 0); // dependency
  workflow.connectPorts("A", 1, "B", 1); // pipe
  workflow.connectPorts("B", 0, "C", 0); // dependency
  workflow.connectPorts("B", 1, "C", 1); // pipe

  workflow.initCoherentGroups();

  auto assertGroupsHaveCommands = [&execution, &workflow](const std::vector<std::string> &nodeIds, const std::string &expectedCommand) {
    for (std::string id : nodeIds) {
      auto systemCommandNode = dynamic_cast<const SystemCommandNode *>(workflow.getNode(id));
      REQUIRE(systemCommandNode);
      CoherentNodeGroup *nodeGroup = systemCommandNode->getCoherentGroup();
      REQUIRE(nodeGroup);
      REQUIRE(nodeGroup->getCommand(&execution) == expectedCommand);
    }
  };

  SECTION("Command of each node is the piped command of all three nodes") {
    assertGroupsHaveCommands({"A", "B", "C"}, "CommandA | CommandB | CommandC");
  }

  SECTION("First node of the group skipped") {
    execution.setNodeState("A", NodeState(NodeExecutionState::TO_BE_SKIPPED));
    assertGroupsHaveCommands({"A", "B", "C"}, "CommandB | CommandC");
  }

  SECTION("Middle node of the group skipped") {
    workflow.resetNodeStates();
    execution.setNodeState("B", NodeState(NodeExecutionState::TO_BE_SKIPPED));
    assertGroupsHaveCommands({"A", "B", "C"}, "CommandA | CommandC");
  }

  SECTION("End node of the group skipped") {
    workflow.resetNodeStates();
    workflow.setNodeState("C", NodeState(NodeExecutionState::TO_BE_SKIPPED));
    assertGroupsHaveCommands({"A", "B", "C"}, "CommandA | CommandB");
  }

  SECTION("Some mixed skipping") {
    workflow.resetNodeStates();
    workflow.setNodeState("A", NodeState(NodeExecutionState::TO_BE_SKIPPED));
    workflow.setNodeState("C", NodeState(NodeExecutionState::TO_BE_SKIPPED));
    assertGroupsHaveCommands({"A", "B", "C"}, "CommandB");

    workflow.resetNodeStates();
    workflow.setNodeState("B", NodeState(NodeExecutionState::TO_BE_SKIPPED));
    workflow.setNodeState("C", NodeState(NodeExecutionState::TO_BE_SKIPPED));
    assertGroupsHaveCommands({"A", "B", "C"}, "CommandA");
  }
}
