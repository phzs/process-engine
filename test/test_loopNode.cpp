/* Copyright 2021 Karlsruhe Institute of Technology
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License. */

#include "../config.h"
#include "../workflow/Workflow.h"
#include "../workflow/WorkflowExecutor.h"
#include "../workflow/nodes/control/IfNode.h"
#include "../workflow/nodes/control/LoopNode.h"
#include "../workflow/nodes/source/IntegerSourceNode.h"
#include "../workflow/nodes/source/StringSourceNode.h"
#include "TestExecution.h"
#include <catch2/catch.hpp>
#include <memory>
#include <utility>

class LoopTestNode : public Node {
public:
  explicit LoopTestNode(std::string id, std::vector<int> expectedIndices = {},
                        std::string indexVariableName = std::string(config::DEFAULT_LOOP_VARIABLE))
      : Node(std::move(id)), executionCounter(0), name("CountingTestNode"), expectedIndices(std::move(expectedIndices)),
        indexVariableName(std::move(indexVariableName)) {
    createPort<DependencyPort>(0, PortDirection::In);
    indexInPort = createPort<DataPort>(1, PortDirection::In);
  }
  bool execute(WorkflowExecution *execution) override {
    if (!processInputPortData(execution)) {
      return false;
    }
    executionCounter++;
    state.setCurrentState(NodeExecutionState::EXECUTED);
    if (!processOutputPortData(execution)) {
      return false;
    }
    return true;
  }
  unsigned int getExecutionCount() const {
    return executionCounter;
  }
  const std::string &getName() const override {
    return name;
  }

private:
  unsigned int executionCounter;
  DataPort *indexInPort;
  std::string name;
  std::vector<int> expectedIndices;
  std::string indexVariableName;
};

void prepareLoopWorkflow(Workflow &workflow, int startIndex, int endIndex, std::vector<int> expectedIndices = {}) {
  workflow.addNode(std::make_unique<LoopNode>("testLoopNode"));
  workflow.addNode(std::make_unique<LoopTestNode>("testCountingNode", std::move(expectedIndices)));
  workflow.connectPorts("testLoopNode", 1, "testCountingNode", 0);

  // to read the loop index
  workflow.connectPorts("testLoopNode", 2, "testCountingNode", 1);

  workflow.addNode(std::make_unique<IntegerSourceNode>("startInputNode", std::to_string(startIndex)));
  workflow.addNode(std::make_unique<IntegerSourceNode>("endInputNode", std::to_string(endIndex)));
  workflow.connectPorts("startInputNode", 0, "testLoopNode", 2);
  workflow.connectPorts("endInputNode", 0, "testLoopNode", 3);
}

TEST_CASE("Basic loop test") {
  Workflow testWorkflow;
  prepareLoopWorkflow(testWorkflow, 0, 3);
  TestExecution testExecution(&testWorkflow);
  WorkflowExecutor().execute(&testWorkflow, &testExecution);
  REQUIRE(testExecution.getState() == ExecutionState::FINISHED);

  SECTION("Test number of loop iterations") {
    auto testNode = dynamic_cast<const LoopTestNode *>(testWorkflow.getNodeConst("testCountingNode"));
    REQUIRE(testNode->getExecutionCount() == 4);
  }
  SECTION("Test loopIndex variable correctly set") {
    REQUIRE(testExecution.expandVariables("${" + std::string(config::DEFAULT_LOOP_VARIABLE) + "}") == std::to_string(4));
  }
}

TEST_CASE("Loops with start index = end index should run once") {
  Workflow testWorkflow;
  prepareLoopWorkflow(testWorkflow, 3, 3, {3});
  TestExecution testExecution(&testWorkflow);
  WorkflowExecutor().execute(&testWorkflow, &testExecution);
  REQUIRE(testExecution.getState() == ExecutionState::FINISHED);

  SECTION("Number of executions should be 1") {
    auto testNode = dynamic_cast<const LoopTestNode *>(testWorkflow.getNodeConst("testCountingNode"));
    REQUIRE(testNode->getExecutionCount() == 1);
  }
}

TEST_CASE("Loop within branch") {
  Workflow testWorkflow;
  prepareLoopWorkflow(testWorkflow, 0, 3);
  // put the whole test workflow into a branch (connect it to the false port, so that we don't need to set the condition to true)
  testWorkflow.addNode(std::make_unique<IfNode>("testIfNode"));
  testWorkflow.connectPorts("testIfNode", 2, "testLoopNode", 0);
  TestExecution testExecution(&testWorkflow);
  WorkflowExecutor().execute(&testWorkflow, &testExecution);
  REQUIRE(testExecution.getState() == ExecutionState::FINISHED);

  SECTION("Test number of loop iterations") {
    auto testNode = dynamic_cast<const LoopTestNode *>(testWorkflow.getNodeConst("testCountingNode"));
    REQUIRE(testNode->getExecutionCount() == 4);
  }
  SECTION("Test loopIndex variable correctly set") {
    REQUIRE(testExecution.expandVariables("${" + std::string(config::DEFAULT_LOOP_VARIABLE) + "}") == std::to_string(4));
  }
}

TEST_CASE("Loop within loop") {
  Workflow testWorkflow;
  prepareLoopWorkflow(testWorkflow, 0, 3); // this will be the inner loop

  // put the whole test workflow into a branch (connect it to the false port, so that we don't need to set the condition to true)
  testWorkflow.addNode(std::make_unique<LoopNode>("testOuterLoopNode"));
  testWorkflow.connectPorts("testOuterLoopNode", 1, "testLoopNode", 0);

  // parameterize outer loop node
  testWorkflow.addNode(std::make_unique<IntegerSourceNode>("outerStartInputNode", std::to_string(0)));
  testWorkflow.addNode(std::make_unique<IntegerSourceNode>("outerEndInputNode", std::to_string(8)));
  testWorkflow.addNode(std::make_unique<IntegerSourceNode>("outerStepInputNode", std::to_string(2)));
  testWorkflow.addNode(std::make_unique<StringSourceNode>("outerIndexVariableInputNode", std::to_string(2)));
  testWorkflow.addNode(std::make_unique<StringSourceNode>("outerIndexVariableInputNode", "outerIndex"));
  testWorkflow.connectPorts("outerStartInputNode", 0, "testOuterLoopNode", 2);
  testWorkflow.connectPorts("outerEndInputNode", 0, "testOuterLoopNode", 3);
  testWorkflow.connectPorts("outerStepInputNode", 0, "testOuterLoopNode", 4);
  testWorkflow.connectPorts("outerIndexVariableInputNode", 0, "testOuterLoopNode", 5);

  // add another counter node to the outer loop branch (next to the inner loop) in order to verify the number of iterations of the outer loop
  testWorkflow.addNode(std::make_unique<LoopTestNode>("outerCountingNode", std::vector<int>({0, 2, 4, 6, 8}), "outerIndex"));
  testWorkflow.connectPorts("testOuterLoopNode", 1, "outerCountingNode", 0);
  testWorkflow.connectPorts("testOuterLoopNode", 2, "outerCountingNode", 1);

  TestExecution testExecution(&testWorkflow);
  WorkflowExecutor().execute(&testWorkflow, &testExecution);
  REQUIRE(testExecution.getState() == ExecutionState::FINISHED);

  int expectedOuterCount = 5; // from 0 to 8 with step 2
  int expectedInnerCount = 4 * expectedOuterCount;
  SECTION("Correct number of inner loop iterations") {
    auto testNode = dynamic_cast<const LoopTestNode *>(testWorkflow.getNodeConst("testCountingNode"));
    REQUIRE(testNode->getExecutionCount() == expectedInnerCount);
  }

  SECTION("Correct number of outer loop iterations") {
    auto testNode = dynamic_cast<const LoopTestNode *>(testWorkflow.getNodeConst("outerCountingNode"));
    REQUIRE(testNode->getExecutionCount() == expectedOuterCount);
  }
  SECTION("LoopIndex variable correctly set for outer loop") {
    REQUIRE(testExecution.expandVariables("${outerIndex}") == std::to_string(2 * expectedOuterCount));
  }
}

TEST_CASE("Assure that LoopNodes can handle input with variables") {
  Workflow testWorkflow;
  testWorkflow.addNode(std::make_unique<LoopNode>("testLoopNode"));
  testWorkflow.addNode(std::make_unique<IntegerSourceNode>("testSource", "${testVar}"));
  testWorkflow.connectPorts("testSource", 0, "testLoopNode", 3);

  TestExecution testExecution(&testWorkflow);
  testExecution.setVariable("testVar", "1");
  REQUIRE(WorkflowExecutor().execute(&testWorkflow, &testExecution));
}

TEST_CASE("Test if step works correctly") {
  Workflow testWorkflow;
  prepareLoopWorkflow(testWorkflow, 5, 25, {5, 10, 15, 20, 25});
  testWorkflow.addNode(std::make_unique<IntegerSourceNode>("stepSourceNode", std::to_string(5)));
  testWorkflow.connectPorts("stepSourceNode", 0, "testLoopNode", 4);
  TestExecution testExecution(&testWorkflow);
  WorkflowExecutor().execute(&testWorkflow, &testExecution);
  REQUIRE(testExecution.getState() == ExecutionState::FINISHED);

  SECTION("Test number of loop iterations") {
    auto testNode = dynamic_cast<const LoopTestNode *>(testWorkflow.getNodeConst("testCountingNode"));
    REQUIRE(testNode->getExecutionCount() == 5);
  }
  SECTION("Test loopIndex variable correctly set") {
    REQUIRE(testExecution.expandVariables("${" + std::string(config::DEFAULT_LOOP_VARIABLE) + "}") == std::to_string(30));
  }
}
