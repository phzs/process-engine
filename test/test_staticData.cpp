/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "../config.h"
#include "helpers.h"
#include <catch2/catch.hpp>

TEST_CASE("Test data conversions using StaticData") {

  SECTION("Convert to string") {
    REQUIRE(StaticData(7).toString() == "7");
    REQUIRE(StaticData(true).toString() == config::TRUE_STRING_REPR);
    REQUIRE(StaticData(false).toString() == config::FALSE_STRING_REPR);
    REQUIRE(StaticData(1.0f).toString() == "1.000000");
  }

  SECTION("Convert from string") {
    REQUIRE(StaticData("42").toInt() == 42);
    REQUIRE(StaticData(std::string(config::FALSE_STRING_REPR)).toBoolean() == false);
    REQUIRE(StaticData(std::string(config::TRUE_STRING_REPR)).toBoolean() == true);
    REQUIRE(StaticData("7.35").toFloat() == 7.35f);
    REQUIRE(StaticData("3.").toFloat() == 3.0f);
    REQUIRE(StaticData("1337").toFloat() == 1337.0f);
  }

  SECTION("Convert to boolean") {
    REQUIRE(StaticData("true").toBoolean());
    REQUIRE(StaticData("True").toBoolean());
    REQUIRE(StaticData("TRUE").toBoolean());
    REQUIRE(!StaticData("false").toBoolean());
    REQUIRE(!StaticData("False").toBoolean());
    REQUIRE(!StaticData("FALSE").toBoolean());
  }

  SECTION("Erroneous conversions") {
    REQUIRE_THROWS(StaticData("abc").toInt());
    REQUIRE_THROWS(StaticData("4a").toInt());
    REQUIRE_THROWS(StaticData("13.").toInt());
    REQUIRE_THROWS(StaticData("1.7").toInt());

    REQUIRE_THROWS(StaticData("abc").toFloat());
    REQUIRE_THROWS(StaticData("3.5a").toFloat());

    REQUIRE_THROWS(StaticData("abc").toBoolean());
    REQUIRE_THROWS(StaticData("130").toBoolean());
    REQUIRE_THROWS(StaticData("0.0").toBoolean());

    REQUIRE_THROWS(StaticData(" ").toBoolean());
    REQUIRE_THROWS(StaticData(" ").toInt());
    REQUIRE_THROWS(StaticData(" ").toFloat());
  }
}
