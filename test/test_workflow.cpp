/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "../workflow/Workflow.h"
#include "../workflow/WorkflowAnalyzer.h"
#include "../workflow/nodes/control/IfNode.h"
#include "../workflow/ports/BranchingPort.h"
#include "TestExecution.h"
#include "helpers.h"
#include <catch2/catch.hpp>


TEST_CASE("Test if branch initialization works correctly") {

  /*
   *  [outer_if] -----------------------------------------------------  [node_outside]
   *            \ (false)
   *             \
   *               [inner_if] ---------------------- [node_in_       ]
   *                       \ (false)                 [outer_if_branch]
   *                        \
   *                         \
   *                           [node_in_       ]
   *                           [inner_if_branch]
   */

  SECTION("Test if nested branches will be initialized correctly") {
    Workflow workflow;
    workflow.addNode(std::make_unique<IfNode>("inner_if"));
    workflow.addNode(std::make_unique<IfNode>("outer_if"));
    workflow.addNode(helpers::createSimpleToolNode("node_in_inner_if_branch", false));
    workflow.addNode(helpers::createSimpleToolNode("node_in_outer_if_branch", false));
    workflow.addNode(helpers::createSimpleToolNode("node_outside", true));

    int branchOutIndex = 2; // use the "false" port
    workflow.connectPorts("outer_if", branchOutIndex, "inner_if", 0);
    workflow.connectPorts("inner_if", branchOutIndex, "node_in_inner_if_branch", 0);
    workflow.connectPorts("inner_if", 0, "node_in_outer_if_branch", 0);
    workflow.connectPorts("outer_if", 0, "node_outside", 0);

    // Let workflow initialize branches as usually
    TestExecution testExecution(&workflow);
    workflow.initBranches();

    SECTION("Test if the outer branch contain the correct nodes") {
      // Test the result by obtaining the branchNodes, ask them for their branches and test if they contain the expected
      // nodeIds
      const WorkflowBranch *outerBranch = helpers::obtainBranchFromNode(&workflow, "outer_if", branchOutIndex);
      auto outerBranchIds = outerBranch->getNodeIds();
      for (const std::string &expectedId : {"inner_if", "node_in_outer_if_branch"}) {
        REQUIRE_FALSE(outerBranchIds.find(expectedId) == outerBranchIds.end());
      }
      REQUIRE(outerBranchIds.size() == 2);
      const WorkflowBranch *innerBranch = helpers::obtainBranchFromNode(&workflow, "inner_if", branchOutIndex);
      auto innerBranchIds = innerBranch->getNodeIds();
      REQUIRE_FALSE(innerBranchIds.find("node_in_inner_if_branch") == innerBranchIds.end());
      REQUIRE(innerBranchIds.size() == 1);
    }

    SECTION("Test if the inner (nested) branch contain the correct nodes") {
      const WorkflowBranch *innerBranch = helpers::obtainBranchFromNode(&workflow, "inner_if", branchOutIndex);
      auto innerBranchIds = innerBranch->getNodeIds();
      REQUIRE_FALSE(innerBranchIds.find("node_in_inner_if_branch") == innerBranchIds.end());
      REQUIRE(innerBranchIds.size() == 1);
    }
  }
}
