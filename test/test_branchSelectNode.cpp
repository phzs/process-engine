/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "../workflow/nodes/control/BranchSelectNode.h"
#include <catch2/catch.hpp>

TEST_CASE("BranchingPorts initialized correctly") {
  int numberOfBranches = 10;
  BranchSelectNode branchSelectNode("testNode", numberOfBranches);

  for (int portIndex = 1; portIndex <= numberOfBranches; portIndex++) {
    auto port = branchSelectNode.getPort(PortDirection::Out, portIndex);
    REQUIRE(port != nullptr);
    REQUIRE(dynamic_cast<BranchingPort *>(port));
  }
}
