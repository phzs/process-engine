/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once

#include "../workflow/interaction/InteractionManager.h"
#include <queue>

class WorkflowExecutor;

/**
 * Instead of storing interactions in a file and checking the file for updates,
 * this implementation stores json values internally which can be added before
 * the interactions were registered by the nodes.
 * Once the interactions will be registered, it provides the value to them
 * instantly and reports the update.
 *
 * This allows to test the interaction mechanism programmatically.
 */
class TestInteractionManager : public InteractionManager {

public:
  // we need to know the executor and execution in order to automatically trigger the update for filled interaction values which normally happens when the SIGUSR1 was received
  explicit TestInteractionManager(WorkflowExecutor *executor);
  ~TestInteractionManager() override;

  /**
   * Add a new interaction value for a specific node.
   * If this node will register an interaction, the value will be consumed (removed from storage).
   * This way, multiple values can be provided for InteractionNodes which will register multiple
   * interactions (e.g. when in a loop).
   * @param node Node which will register the interaction for which the value should be added (issuingNode)
   * @param value value to be provided to the interaction once it is created
   */
  void insertInteractionValue(Node *node, const json &value);

protected:
  std::vector<unsigned int> updateValues() override;
  void interactionAdded(const AbstractInteraction *interaction) override;

private:
  void executeRunner();

  std::unordered_map<Node *, std::queue<json>> interactionValues;

  std::thread runner; // a runner to put the
  bool running = true;
};
