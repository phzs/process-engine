/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "../config.h"
#include "../workflow/Workflow.h"
#include "TestExecution.h"
#include <catch2/catch.hpp>


TEST_CASE("Variable regex") {
  std::cmatch match;
  std::string regex_string = std::string(config::VARIABLE_REGEX);
  std::regex variable_regex(regex_string);
  REQUIRE(std::regex_match("${test}", match, variable_regex));
  REQUIRE(match[1] == "test");
  REQUIRE(std::regex_match("${test123}", match, variable_regex));
  REQUIRE(match[1] == "test123");
  REQUIRE(std::regex_match("${TeSt123}", match, variable_regex));
  REQUIRE(match[1] == "TeSt123");
  REQUIRE(std::regex_search("${test}", variable_regex));
  REQUIRE(std::regex_search("This is a ${test}!", variable_regex));

  REQUIRE_FALSE(std::regex_match("${TeSt", match, variable_regex));
  REQUIRE_FALSE(std::regex_match("{test}", match, variable_regex));
  REQUIRE_FALSE(std::regex_match("$test", match, variable_regex));
  REQUIRE_FALSE(std::regex_match("$test$", match, variable_regex));
  REQUIRE_FALSE(std::regex_match("${te*st}", match, variable_regex));
}

TEST_CASE("Expanding of variables") {
  Workflow workflow;
  TestExecution execution(&workflow);
  execution.setVariable("myVar0", "test");
  execution.setVariable("standard_variable", "foobar baz 123");

  // test basic replacement
  REQUIRE(execution.expandVariables("This is a ${myVar0}!") == "This is a test!");

  // multiple variables in one string
  REQUIRE(execution.expandVariables("This is a ${myVar0} with two variables: ${standard_variable}!") == "This is a test with two variables: foobar baz 123!");

  // test updating of a variable
  execution.setVariable("myVar0", "updated test");
  REQUIRE(execution.expandVariables("${myVar0}") == "updated test");

  // test that missing variables are replaced like expected
  REQUIRE(execution.expandVariables("${missing}") == std::string(config::VARIABLE_VALUE_UNDEFINED));
  std::ostringstream expected;
  expected << "Some vars might be " << config::VARIABLE_VALUE_UNDEFINED << " after replacement";
  REQUIRE(execution.expandVariables("Some vars might be ${this_was_not_set} after replacement") == expected.str());
}

TEST_CASE("Nested variables") {
  Workflow workflow;
  TestExecution execution(&workflow);
  execution.setVariable("hello_world", "Hello${last_part}.");
  execution.setVariable("last_part", "${space}World");
  execution.setVariable("space", " ");
  REQUIRE(execution.expandVariables("${hello_world}") == "Hello World.");
}

TEST_CASE("Recursive variable expanding") {
  Workflow workflow;
  TestExecution execution(&workflow);

  // since nested variables are allowed now, assure that recursive lookups are catched
  execution.setVariable("self-contained", "Endless? ${self-contained}");
  REQUIRE_THROWS(execution.expandVariables("${self-contained}"));
  execution.setVariable("first_part", "Hello ${last_part}");
  execution.setVariable("last_part", "Hello ${first_part}");
  REQUIRE_THROWS(execution.expandVariables("${first_part}"));
  REQUIRE_THROWS(execution.expandVariables("${last_part}"));
}
