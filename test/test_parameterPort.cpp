/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "../workflow/Workflow.h"
#include "../workflow/ports/ParameterPort.h"
#include "TestExecution.h"
#include <catch2/catch.hpp>

TEST_CASE("Test if flags behave correctly with or without data ready to be obtained") {
  Workflow workflow;
  TestExecution testExecution(&workflow);
  ParameterPort paramPort(0, 0, "test", "t", "flag", PortDirection::In, false);
  DataPort connectedPort(0, PortDirection::Out);
  paramPort.addConnectedPort(&connectedPort);

  REQUIRE(paramPort.getCommandLineArgument(&testExecution) == "-t");
  // port connected but no data available!
  REQUIRE_FALSE(paramPort.isSet());

  connectedPort.setData(std::make_shared<StaticData>(false));
  paramPort.updateData();
  REQUIRE_FALSE(paramPort.isSet());

  connectedPort.setData(std::make_shared<StaticData>(true));
  paramPort.updateData();
  REQUIRE(paramPort.isSet());
}
