
/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once

#include "../workflow/state/WorkflowExecution.h"
#include "TestInteractionManager.h"

class TestExecution : public WorkflowExecution {
public:
  explicit TestExecution(AbstractWorkflow *workflow = nullptr, WorkflowExecutor *executor = nullptr)
      : WorkflowExecution(std::make_unique<TestInteractionManager>(executor), workflow, false, nullptr) {
  }

  void insertInteractionValue(Node *node, const json &value) {
    dynamic_cast<TestInteractionManager *>(getInteractionManager())->insertInteractionValue(node, value);
  }
};
