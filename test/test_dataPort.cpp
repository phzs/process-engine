/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "../workflow/Workflow.h"
#include "../workflow/ports/ParameterPort.h"
#include "TestExecution.h"
#include <catch2/catch.hpp>

class TestPort : public DataPort {
public:
  TestPort(int index, PortDirection direction)
      : DataPort(index, direction){};
  std::shared_ptr<StaticData> getDataRaw() {
    return data;
  }
};

TEST_CASE("Get data via interface") {
  Workflow workflow;
  TestExecution testExecution(&workflow);
  DataPort dataPort(0, PortDirection::In);
  DataPort connectedPort(0, PortDirection::Out);
  dataPort.addConnectedPort(&connectedPort);

  REQUIRE(dataPort.getData() == nullptr);

  connectedPort.setData(std::make_shared<StaticData>("test"));
  REQUIRE_FALSE(dataPort.getData() == nullptr);
}

TEST_CASE("Get/update data internally") {
  Workflow workflow;
  TestExecution testExecution(&workflow);
  TestPort dataPort(0, PortDirection::In);

  DataPort connectedPort(0, PortDirection::Out);
  dataPort.addConnectedPort(&connectedPort);
  REQUIRE(dataPort.getDataRaw().get() == nullptr);
  connectedPort.setData(std::make_shared<StaticData>("test"));
  REQUIRE(dataPort.getDataRaw().get() == nullptr);
  dataPort.updateData();
  REQUIRE_FALSE(dataPort.getDataRaw().get() == nullptr);
  REQUIRE(dataPort.getDataRaw()->toString() == "test");
}
