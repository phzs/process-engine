/* Copyright 2021 Karlsruhe Institute of Technology
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License. */

#include "../workflow/Workflow.h"
#include "../workflow/WorkflowBranch.h"
#include "../workflow/WorkflowExecutor.h"
#include "../workflow/nodes/control/IfNode.h"
#include "../workflow/nodes/source/BoolSourceNode.h"
#include "TestExecution.h"
#include "helpers.h"
#include <catch2/catch.hpp>

TEST_CASE("Basic if test") {
  Workflow workflow;

  auto ifNode = workflow.createNode<IfNode>("test");
  workflow.createNode<BoolSourceNode>("conditionInput", "true");
  workflow.addNode(helpers::createSimpleToolNode("trueNode"));
  workflow.addNode(helpers::createSimpleToolNode("falseNode"));

  workflow.connectPorts("conditionInput", 0, "test", 1);
  workflow.connectPorts("test", 1, "trueNode", 0);
  workflow.connectPorts("test", 2, "falseNode", 0);

  WorkflowExecutor executor;
  TestExecution testExecution(&workflow, &executor);
  REQUIRE(executor.execute(&workflow, &testExecution));
  REQUIRE(ifNode->getState().getActiveBranch()->getIdentifier() == "test,1");
  REQUIRE(workflow.getNode("falseNode")->getState().isSkipped());
}

TEST_CASE("Dependents of if node") {
  SECTION("Assure dependents will execute") {
    Workflow workflow;

    auto ifNode = workflow.createNode<IfNode>("test");
    workflow.createNode<BoolSourceNode>("conditionInput", "false");
    workflow.addNode(helpers::createSimpleToolNode("trueNode"));
    workflow.addNode(helpers::createSimpleToolNode("falseNode"));
    workflow.addNode(helpers::createSimpleToolNode("dependent"));

    workflow.connectPorts("conditionInput", 0, "test", 1);
    workflow.connectPorts("test", 1, "trueNode", 0);
    workflow.connectPorts("test", 2, "falseNode", 0);
    workflow.connectPorts("test", 0, "dependent", 0);

    WorkflowExecutor executor;
    TestExecution testExecution(&workflow, &executor);
    REQUIRE(executor.execute(&workflow, &testExecution));
    REQUIRE(ifNode->getState().getActiveBranch()->getIdentifier() == "test,2");
    REQUIRE(workflow.getNode("trueNode")->getState().isSkipped());

    REQUIRE(workflow.getNode("dependent")->getState().getCurrentState() == NodeExecutionState::EXECUTED);
  }
  SECTION("When active branch is empty") {
    Workflow workflow;

    auto ifNode = workflow.createNode<IfNode>("test");
    workflow.createNode<BoolSourceNode>("conditionInput", "true");
    workflow.addNode(helpers::createSimpleToolNode("falseNode"));
    workflow.addNode(helpers::createSimpleToolNode("dependent"));

    workflow.connectPorts("conditionInput", 0, "test", 1);
    workflow.connectPorts("test", 2, "falseNode", 0);
    workflow.connectPorts("test", 0, "dependent", 0);

    WorkflowExecutor executor;
    TestExecution testExecution(&workflow, &executor);
    REQUIRE(executor.execute(&workflow, &testExecution));
    REQUIRE(ifNode->getState().getActiveBranch()->getIdentifier() == "test,1");
    REQUIRE(workflow.getNode("falseNode")->getState().isSkipped());

    REQUIRE(workflow.getNode("dependent")->getState().getCurrentState() == NodeExecutionState::EXECUTED);
  }

  SECTION("When last node in active branch is skipped") {
    Workflow workflow;

    auto ifNode = workflow.createNode<IfNode>("test");
    workflow.createNode<BoolSourceNode>("conditionInput", "false");
    auto falseNode = workflow.addNode(helpers::createSimpleToolNode("falseNode"));
    dynamic_cast<ExecutableNode *>(falseNode)->setExecutionProfile(ExecutionProfile::SKIP);
    workflow.addNode(helpers::createSimpleToolNode("dependent"));

    workflow.connectPorts("conditionInput", 0, "test", 1);
    workflow.connectPorts("test", 2, "falseNode", 0);
    workflow.connectPorts("test", 0, "dependent", 0);

    WorkflowExecutor executor;
    TestExecution testExecution(&workflow, &executor);
    REQUIRE(executor.execute(&workflow, &testExecution));
    REQUIRE(ifNode->getState().getActiveBranch()->getIdentifier() == "test,2");
    REQUIRE(workflow.getNode("falseNode")->getState().isSkipped());

    REQUIRE(workflow.getNode("dependent")->getState().getCurrentState() == NodeExecutionState::EXECUTED);
  }
}
