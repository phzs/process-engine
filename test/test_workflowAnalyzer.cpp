/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "../workflow/Workflow.h"
#include "../workflow/nodes/FileInputNode.h"
#include "../workflow/nodes/FileOutputNode.h"
#include "../workflow/nodes/ToolNode.h"
#include "../workflow/nodes/control/VariableNode.h"
#include "../workflow/nodes/interaction/UserInputFileNode.h"
#include "../workflow/nodes/interaction/UserOutputTextNode.h"
#include <catch2/catch.hpp>

#include "../workflow/WorkflowAnalyzer.h"
#include "../workflow/WorkflowExecutor.h"
#include "../workflow/nodes/control/IfNode.h"
#include "../workflow/nodes/control/LoopNode.h"
#include "TestExecution.h"
#include "TestNode.h"
#include "helpers.h"


TEST_CASE("Implicit dependencies (connected with data ports only)") {
  // TODO
}


TEST_CASE("Test coherent node detection") {

  /**
  *                    ----- [E]
  *                   /
  * [A] ----------- [C] @ @ [D]
  *     \          @
  *      -- [B] @ @
  *
  *  (@: pipe connection)
  *
  */
  Workflow workflow;

  for (const auto &i : {"A", "B", "C", "D", "E"}) {
    // add env nodes only for B (needed for the second section of the test)
    workflow.addNode(helpers::createSimpleToolNode(i, std::string("B") == i));
  }
  auto nodes = workflow.getNodes();

  // dependency connections
  workflow.connectPorts("A", 0, "B", 0);
  workflow.connectPorts("A", 0, "C", 0);
  workflow.connectPorts("C", 0, "E", 0);

  // pipe connections
  workflow.connectPorts("B", 1, "C", 1);
  workflow.connectPorts("C", 1, "D", 1);
  SECTION("Detecting a pipe group") {

    auto workflowAnalyzer = WorkflowAnalyzer(&workflow);

    REQUIRE_FALSE(workflowAnalyzer.findCoherentNodeGroup("A"));
    REQUIRE_FALSE(workflowAnalyzer.findCoherentNodeGroup("E"));

    auto detectedPipeNodesB = workflowAnalyzer.findCoherentNodeGroup("B");
    auto detectedPipeNodesC = workflowAnalyzer.findCoherentNodeGroup("C");
    auto detectedPipeNodesD = workflowAnalyzer.findCoherentNodeGroup("D");

    // each node in the "piped" group BCD must know that it is piped together with the others of the group
    // the result of findCoherentNodeGroup must include exactly B, C and D for all three nodes
    int expectedGroupSize = 3;
    REQUIRE(detectedPipeNodesB->size() == expectedGroupSize);
    REQUIRE(detectedPipeNodesC->size() == expectedGroupSize);
    REQUIRE(detectedPipeNodesD->size() == expectedGroupSize);
    REQUIRE(*detectedPipeNodesB == *detectedPipeNodesC);
    REQUIRE(*detectedPipeNodesC == *detectedPipeNodesD);

    std::unordered_set<std::string> resultSet = detectedPipeNodesB->getNodeIds();
    for (const auto &expectedId : {"B", "C", "D"}) {
      REQUIRE(resultSet.find(expectedId) != resultSet.end());
    }
    auto coherentConnections = detectedPipeNodesC->getCoherentNodes();
    REQUIRE(coherentConnections[0].connectionType == CoherentConnectionType::NONE); // first must be NONE
    REQUIRE(coherentConnections[1].connectionType == CoherentConnectionType::PIPE);
    REQUIRE(coherentConnections[2].connectionType == CoherentConnectionType::PIPE);
  }
}

TEST_CASE("Conversion with outgoing Pipe Port") {
  Workflow workflow;

  workflow.addNode(std::make_unique<FileInputNode>("fileIn"));
  auto customTestNode = std::make_unique<ToolNode>("test1");
  customTestNode->addPort(std::make_unique<DataPort>(0, PortDirection::In));
  workflow.addNode(std::move(customTestNode));
  workflow.connectPorts("fileIn", 1, "test1", 0);

  SECTION("The converter connection (pipe to non-Pipe) should not be interpreted as a normal pipe connection") {
    WorkflowAnalyzer analyzer(&workflow);
    REQUIRE(!analyzer.findCoherentNodeGroup("fileIn")); // fail if the algorithm detects a coherent group
  }

  TestExecution testExecution(&workflow);
  FileInputNode *fileInputNode = dynamic_cast<FileInputNode *>(workflow.getNode("fileIn"));

  SECTION("FileInputNode respects outgoing connections from it's pipe output port that are connected to non-pipe ports") {
    REQUIRE(fileInputNode->getCommand(&testExecution).empty());
  }
}

TEST_CASE("Ensure that invalid topologies will be detected") {

  SECTION("Connecting pipe AND env ports should result in an error") {
    Workflow workflow;
    workflow.addNode(helpers::createSimpleToolNode("test1", true));
    workflow.addNode(helpers::createSimpleToolNode("test2", true));
    workflow.connectPorts("test1", 1, "test2", 1); // env
    workflow.connectPorts("test1", 1, "test2", 2); // pipe
    auto workflowAnalyzer = WorkflowAnalyzer(&workflow);
    REQUIRE_THROWS_AS(workflowAnalyzer.findCoherentNodeGroup("test1"), std::logic_error);
    REQUIRE_THROWS_AS(workflowAnalyzer.findCoherentNodeGroup("test2"), std::logic_error);
  }

  SECTION("Connecting pipe to multiple targets should result in an error"
          " (this is not possible in the editor") {
    Workflow workflow;
    workflow.addNode(helpers::createSimpleToolNode("test1", true));
    workflow.addNode(helpers::createSimpleToolNode("test2", true));
    workflow.addNode(helpers::createSimpleToolNode("test3", true));
    workflow.connectPorts("test1", 1, "test2", 1);
    workflow.connectPorts("test1", 1, "test3", 1);
    auto workflowAnalyzer = WorkflowAnalyzer(&workflow);
    REQUIRE_THROWS_AS(workflowAnalyzer.findCoherentNodeGroup("test1"), std::logic_error);
    REQUIRE_THROWS_AS(workflowAnalyzer.findCoherentNodeGroup("test2"), std::logic_error);
    REQUIRE_THROWS_AS(workflowAnalyzer.findCoherentNodeGroup("test3"), std::logic_error);
  }
}

TEST_CASE("Test finding of branches ") {

  SECTION("Test if nested branches will are found correctly") {
    Workflow workflow;
    auto innerIf = workflow.createNode<IfNode>("inner_if");
    auto outerIf = workflow.createNode<IfNode>("outer_if");
    workflow.addNode(helpers::createSimpleToolNode("node_in_inner_if_branch", false));
    workflow.addNode(helpers::createSimpleToolNode("node_in_outer_if_branch", false));
    workflow.addNode(helpers::createSimpleToolNode("node_outside", true));

    int branchOutIndex = 2; // use the "false" port
    workflow.connectPorts("outer_if", branchOutIndex, "inner_if", 0);
    workflow.connectPorts("inner_if", branchOutIndex, "node_in_inner_if_branch", 0);
    workflow.connectPorts("inner_if", 0, "node_in_outer_if_branch", 0);
    workflow.connectPorts("outer_if", 0, "node_outside", 0);

    auto workflowAnalyzer = WorkflowAnalyzer(&workflow);

    SECTION("Running on a branch with nested branch") {

      /* when run on the inner IfNode, findBranch should return only the contained node plus the closing EndIfNode */
      SECTION("Running on an inner branch") {
        auto innerBranch = workflowAnalyzer.findBranch(&workflow, innerIf, 2);
        auto innerBranchIds = innerBranch->getNodeIds();
        REQUIRE_FALSE(innerBranchIds.find("node_in_inner_if_branch") == innerBranchIds.end());
        REQUIRE(innerBranchIds.size() == 1);
      }

      SECTION("Running on an BranchingPort that is not connected") {
        auto emptyBranch = workflowAnalyzer.findBranch(&workflow, outerIf, 1);
        auto emptyBranchIds = emptyBranch->getNodeIds();
        REQUIRE(emptyBranchIds.empty());
        REQUIRE(emptyBranch->isEmpty());
      }
    }

    SECTION("Check if branchId is set correctly for each node") {
      Workflow testWorkflow;
      TestExecution testExecution(&testWorkflow);
      workflow.initBranches();

      REQUIRE(workflow.getNodeConst("outer_if")->getBranchId() == "root");
      REQUIRE(workflow.getNodeConst("inner_if")->getBranchId() == "outer_if," + std::to_string(branchOutIndex));
      REQUIRE(workflow.getNodeConst("node_in_inner_if_branch")->getBranchId() == "inner_if," + std::to_string(branchOutIndex));
      REQUIRE(workflow.getNodeConst("node_in_outer_if_branch")->getBranchId() == "outer_if," + std::to_string(branchOutIndex));
    }

    SECTION("Check if branchId is set correctly when nested branches are executed") {
      TestExecution testExecution(&workflow);
      workflow.initBranches();
      WorkflowExecutor().execute(&workflow, &testExecution);

      REQUIRE(workflow.getNodeConst("outer_if")->getBranchId() == "root");
      REQUIRE(workflow.getNodeConst("inner_if")->getBranchId() == "outer_if," + std::to_string(branchOutIndex));
      REQUIRE(workflow.getNodeConst("node_in_inner_if_branch")->getBranchId() == "inner_if," + std::to_string(branchOutIndex));
      REQUIRE(workflow.getNodeConst("node_in_outer_if_branch")->getBranchId() == "outer_if," + std::to_string(branchOutIndex));
    }
  }
}

TEST_CASE("Branch parents and branchTokens") {
  SECTION("Finding the parents in a single branch") {
    Workflow workflow;
    auto branchNode = workflow.createNode<LoopNode>("branch");
    auto inBranch = workflow.createNode<TestNode>("inBranch");
    auto branchEnd = workflow.createNode<TestNode>("branchEnd");
    workflow.connectPorts("branch", 1, "inBranch", 0);
    workflow.connectPorts("inBranch", 0, "branchEnd", 0);
    workflow.initBranches();
    TestExecution execution(&workflow);

    REQUIRE_FALSE(branchNode->getBranchParent());
    REQUIRE(inBranch->getBranchParent() == branchNode);
    REQUIRE(branchEnd->getBranchParent());
    REQUIRE(branchEnd->getBranchParent() == branchNode);

    REQUIRE(branchNode->getInitialState().branchTokenCount() == 1);
  }

  SECTION("Finding the parents for nested branches") {
    Workflow workflow;
    auto branchNode = workflow.createNode<LoopNode>("branch");
    auto innerBranchNode = workflow.createNode<LoopNode>("innerBranch");
    auto inBranch = workflow.createNode<TestNode>("inBranch");
    auto branchEnd = workflow.createNode<TestNode>("branchEnd");
    workflow.connectPorts("branch", 1, "innerBranch", 0);
    workflow.connectPorts("innerBranch", 1, "inBranch", 0);
    workflow.connectPorts("inBranch", 0, "branchEnd", 0);
    workflow.initBranches();
    TestExecution execution(&workflow);

    REQUIRE_FALSE(branchNode->getBranchParent());
    REQUIRE(innerBranchNode->getBranchParent() == branchNode);
    REQUIRE(inBranch->getBranchParent() == innerBranchNode);
    REQUIRE(branchEnd->getBranchParent() == innerBranchNode);

    REQUIRE(branchNode->getInitialState().branchTokenCount() == 1);
    REQUIRE(innerBranchNode->getInitialState().branchTokenCount() == 1);
  }

  SECTION("Finding the parents for nested branches with value connection") {
    Workflow workflow;
    auto branchNode = workflow.createNode<LoopNode>("branch");
    auto innerBranchNode = workflow.createNode<LoopNode>("innerBranch");
    auto inBranch = workflow.createNode<TestNode>("inBranch");
    auto branchEnd = workflow.createNode<TestNode>("branchEnd");
    workflow.connectPorts("branch", 1, "innerBranch", 0);
    workflow.connectPorts("innerBranch", 1, "inBranch", 0);
    workflow.connectPorts("innerBranch", 2, "inBranch", 1);
    workflow.connectPorts("inBranch", 0, "branchEnd", 0);
    workflow.initBranches();
    TestExecution execution(&workflow);

    REQUIRE_FALSE(branchNode->getBranchParent());
    REQUIRE(innerBranchNode->getBranchParent() == branchNode); // the inner branch is the end on the level of the outer branch
    REQUIRE(inBranch->getBranchParent() == innerBranchNode);
    REQUIRE(branchEnd->getBranchParent() == innerBranchNode);

    REQUIRE(branchNode->getInitialState().branchTokenCount() == 1);
    REQUIRE(innerBranchNode->getInitialState().branchTokenCount() == 1);
  }
}
