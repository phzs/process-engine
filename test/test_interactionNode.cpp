/* Copyright 2021 Karlsruhe Institute of Technology
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License. */

#include "../workflow/Workflow.h"
#include "../workflow/WorkflowExecutor.h"
#include "../workflow/nodes/interaction/UserInputTextNode.h"
#include "TestExecution.h"
#include "TestInteractionManager.h"
#include "TestNode.h"
#include <catch2/catch.hpp>

TEST_CASE("Basic interaction test") {
  Workflow testWorkflow;
  testWorkflow.addNode(std::make_unique<TestNode>("before"));
  auto *uiNode = testWorkflow.createNode<UserInputTextNode>("uiNode", "test description", "default value", 0);
  auto *nodeAfter = testWorkflow.createNode<TestNode>("after");

  testWorkflow.connectPorts("before", 0, "uiNode", 0);
  testWorkflow.connectPorts("uiNode", 0, "after", 0);
  testWorkflow.connectPorts("uiNode", 1, "after", 1);

  WorkflowExecutor executor;
  TestExecution testExecution(&testWorkflow, &executor);
  testExecution.insertInteractionValue(uiNode, "some value");

  REQUIRE(WorkflowExecutor().execute(&testWorkflow, &testExecution));
  REQUIRE(nodeAfter->getInputData());
  REQUIRE(nodeAfter->getInputData()->toString() == "some value");
}
