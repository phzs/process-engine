/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "../workflow/Workflow.h"
#include "../workflow/nodes/Node.h"
#include "../workflow/nodes/ToolNode.h"
#include "../workflow/nodes/control/BranchingNode.h"
#include "../workflow/ports/BranchingPort.h"
#include "../workflow/ports/DependencyPort.h"
#include "../workflow/ports/EnvPort.h"
#include "../workflow/ports/PipePort.h"
#include <memory>
#include <string>

namespace helpers {
  // creates a simple dummy ToolNode without any parameters, just dependency and pipe ports (optionally env ports)
  std::unique_ptr<Node> createSimpleToolNode(const std::string &id, bool addEnvPorts = false) {
    auto result = std::make_unique<ToolNode>(id);
    result->addPort(std::make_unique<DependencyPort>(0, PortDirection::In));
    result->addPort(std::make_unique<DependencyPort>(0, PortDirection::Out));

    result->addPort(std::make_unique<PipePort>(1, PortDirection::In));
    result->addPort(std::make_unique<PipePort>(1, PortDirection::Out));

    // note that the editor would generate different indexes (pipe always at the last position)
    if (addEnvPorts) {
      result->addPort(std::make_unique<EnvPort>(2, PortDirection::In));
      result->addPort(std::make_unique<EnvPort>(2, PortDirection::Out));
    }

    return result;
  }

  const WorkflowBranch *obtainBranchFromNode(const Workflow *workflow, const std::string &node_id, int portIndex) {
    auto branchingNode = dynamic_cast<const BranchingNode *>(workflow->getNodeConst(node_id));
    auto branchingPort = dynamic_cast<const BranchingPort *>(branchingNode->getPort(PortDirection::Out, portIndex));
    return branchingPort->getBranch();
  }
} // namespace helpers
