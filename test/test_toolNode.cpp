/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "../workflow/Workflow.h"
#include "../workflow/WorkflowAnalyzer.h"
#include "../workflow/nodes/EnvNode.h"
#include "../workflow/nodes/interaction/UserInputTextNode.h"
#include "../workflow/ports/ParameterPort.h"
#include "helpers.h"
#include <catch2/catch.hpp>

const int ENV_PARAM_INDEX = 0;

TEST_CASE("Test ToolNode::getConnectedEnvNode()") {
  Workflow workflow;
  workflow.addNode(helpers::createSimpleToolNode("ToolWithEnv", true));
  workflow.addNode(helpers::createSimpleToolNode("ToolWithoutEnv", true));
  auto envNode = std::make_unique<EnvNode>("Env");
  envNode->createPort<EnvPort>(ENV_PARAM_INDEX, PortDirection::Out);
  workflow.addNode(std::move(envNode));
  workflow.connectPorts("Env", 0, "ToolWithEnv", 2);

  ToolNode *toolWithEnv = dynamic_cast<ToolNode *>(workflow.getNode("ToolWithEnv"));
  ToolNode *toolWithoutEnv = dynamic_cast<ToolNode *>(workflow.getNode("ToolWithoutEnv"));

  const EnvNode *foundEnvNode = toolWithEnv->getConnectedEnvNode();
  REQUIRE(foundEnvNode != nullptr);
  REQUIRE(foundEnvNode->getId() == "Env");

  REQUIRE(toolWithoutEnv->getConnectedEnvNode() == nullptr);
}

TEST_CASE("Environment dependencies detected correctly") {
  Workflow workflow;
  workflow.addNode(helpers::createSimpleToolNode("Tool", true));

  const int PIPE_INDEX = 1;
  auto envNode = std::make_unique<EnvNode>("Env");
  envNode->createPort<ParameterPort>(0, 0, "dummy", "d", "string", PortDirection::In,
                                     true); // this dummy argument can be used to produce a dependency for the EnvNode
  envNode->createPort<EnvPort>(ENV_PARAM_INDEX, PortDirection::Out);
  workflow.addNode(std::move(envNode));
  workflow.addNode(helpers::createSimpleToolNode("DependencyTool"));
  workflow.addNode(std::make_unique<UserInputTextNode>("DependencyEnv", "", "", 0));
  workflow.connectPorts("DependencyEnv", 1, "Env", ENV_PARAM_INDEX);
  workflow.connectPorts("DependencyTool", 0, "Tool", 0);
  workflow.connectPorts("Env", 0, "Tool", 2);

  SECTION("Dependencies of the environment must be included in the dependencies of connected tool nodes") {
    // sanity check for the test: Env node should depend on DependencyEnv
    std::unordered_set<std::string> dependenciesEnv = workflow.getNode("Env")->getAllDependencies();
    REQUIRE(dependenciesEnv.find("DependencyEnv") != dependenciesEnv.end());

    std::unordered_set<std::string> dependenciesTool = workflow.getNode("Tool")->getAllDependencies();
    REQUIRE(dependenciesTool.find("DependencyTool") != dependenciesTool.end());
    REQUIRE(dependenciesTool.find("DependencyEnv") != dependenciesTool.end());
  }

  SECTION("Indirect env dependencies must be included in the shared dependencies of groups") {
    workflow.addNode(helpers::createSimpleToolNode("PipeTool"));
    workflow.addNode(helpers::createSimpleToolNode("DependencyPipeTool"));
    workflow.connectPorts("DependencyPipeTool", 0, "PipeTool", 0);
    workflow.connectPorts("Tool", PIPE_INDEX, "PipeTool", PIPE_INDEX);

    // Initialize coherent groups
    auto workflowAnalyzer = WorkflowAnalyzer(&workflow);
    auto nodes = workflow.getNodes();
    for (const auto &node : nodes) {
      auto sysNode = dynamic_cast<SystemCommandNode *>(node.second);
      if (sysNode) {
        sysNode->setCoherentGroup(workflowAnalyzer.findCoherentNodeGroup(sysNode->getId()));
      }
    }

    std::unordered_set<std::string> dependenciesTool = workflow.getNode("Tool")->getAllDependencies();
    REQUIRE(dependenciesTool.find("DependencyTool") != dependenciesTool.end());
    REQUIRE(dependenciesTool.find("DependencyEnv") != dependenciesTool.end());
    REQUIRE(dependenciesTool.find("DependencyPipeTool") != dependenciesTool.end());

    std::unordered_set<std::string> dependenciesPipeTool = workflow.getNode("Tool")->getAllDependencies();
    REQUIRE(dependenciesPipeTool.find("DependencyTool") != dependenciesPipeTool.end());
    REQUIRE(dependenciesPipeTool.find("DependencyEnv") != dependenciesPipeTool.end());
    REQUIRE(dependenciesTool.find("DependencyPipeTool") != dependenciesTool.end());
  }
}
