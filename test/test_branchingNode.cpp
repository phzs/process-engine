/* Copyright 2021 Karlsruhe Institute of Technology
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License. */

#include "../workflow/Workflow.h"
#include "../workflow/nodes/control/IfNode.h"
#include "TestExecution.h"
#include <catch2/catch.hpp>

TEST_CASE("Skipping a BranchingNode should also mark nodes in it's branches and sub-brunches to be skipped") {
  Workflow workflow;
  auto outer = workflow.createNode<IfNode>("outer");
  auto middle = workflow.createNode<IfNode>("middle");
  auto inner = workflow.createNode<IfNode>("inner");
  workflow.connectPorts("outer", 1, "middle", 0);
  workflow.connectPorts("middle", 1, "inner", 0);
  TestExecution execution(&workflow);

  outer->skip();
  REQUIRE(outer->getState().isSkipped());
  REQUIRE(middle->getState().isSkipped());
  REQUIRE(inner->getState().isSkipped());
}
