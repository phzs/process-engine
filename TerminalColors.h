/* Copyright 2021 Karlsruhe Institute of Technology
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License. */

#pragma once
#include <string>

static constexpr std::string_view ANSI_COLOR = "\x1b[38;2;";
static constexpr std::string_view ANSI_NO_COLOR = "\033[0m";

struct Color {
  unsigned int r;
  unsigned int g;
  unsigned int b;
};

std::string beginANSIColor(const Color &color);
