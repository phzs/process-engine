/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once
#include <algorithm>
#include <fstream>
#include <regex>
#include <string>
#include <unordered_map>
#include <unordered_set>

class Node;

namespace util {

  /**
   * Return current date and time as string
   * @return string containing current date & time
   */
  std::string getCurrentDateTime();

  /**
   * Creates the specified directory if it does not exist
   * @param path Path of the directory to create
   * @return true if the directory exists afterwards (was created or existed before)
   */
  bool createDirectory(const std::string &path);

  /**
   * Given an absolute or relative path, return the directory without the file name.
   * e.g. "output/test.txt" -> "output/"
   * @return Path to the directory without filename
   */
  std::string getDirectoryPath(const std::string &path);


  /** Check if a path is absolute
   * @param path path to be checked
   * @return true if path is absolute
   */
  bool isAbsolutePath(const std::string &path);

  /**
   * Get absolute path for a filename in the same directory (adding cwd)
   * @param fileName
   * @return absolute path
   */
  std::string getAbsolutePath(const std::string &fileName);

  /**
   * Check if given file exists on the file system
   * @param fileName Name (incl. path) to check
   * @return true if the file exists
   */
  bool fileExists(const std::string &fileName);

  /**
   * Get absolute path for a relative path (relative to the current directory).
   * If @param expand is true, also perform shell-like expansion to replace leading '~' or variables like '$HOME'.
   * @param relativePath relative path
   * @param expand perform shell-like expansion
   * @return absolute path
   */
  std::string getAbsoluteFilePath(const std::string &relativePath, bool expand = true);

  void deleteFile(const std::string &basicString);

  bool deleteFolder(const std::string &path);

  /**
   * Read a file and return its content as string. Throws invalid_argument if file does not exists or the path is empty.
   * @param path relative or absolute path
   * @return file content as string
   */
  std::string readFile(const std::string &path);

  /**
   * Perform a shell-like expansion of a string.
   * For example, '~' will be replaced with the user's home directory.
   *    ~/test.txt --> /home/user/test.txt
   *    $HOME/test.txt --> /home/user/test.txt
   * See https://man7.org/linux/man-pages/man3/wordexp.3.html.
   * @param string
   * @return expanded string
   */
  std::string expandString(const std::string &string);

  /**
   * Remove all whitespace characters from the left side of a string
   * @param s target string
   * @return modified copy of target string
   */
  std::string ltrim(const std::string &string);

  /**
   * Remove all whitespace characters from the right side of a string
   * @param s target string
   * @return modified copy of target string
   */
  std::string rtrim(const std::string &string);

  /**
   * Remove all whitespace characters from the left and right side of a string
   * @param s target string
   * @return modified copy of target string
   */
  std::string trim(const std::string &string);

  /**
   * Create a formatted string with a given number of reusable values.
   * In the format string, %0 refers to the first value and will be replaced accordingly, %1 refers to the second value and so on.
   * For example, the values "abc" and "01" with the format "[%0, %1]" will become "[abc, 01]".
   * Values tokens can be used more than once in the format. If the token requests a index not in the specified vector 'values',
   * an exception will be thrown.
   *
   * @param format A format string which can contain tokens (%0, %1, ...) representing a value from @param 'values'
   * @param values A vector of string values
   * @return Formatted string with tokens replaced if possible
   */
  std::string formatString(const std::string &format, const std::vector<std::string> &values);

  /**
   * Splits a string at a given delimiter. The default delimiter is a comma ','.
   * @param string A string which may be delimited by `delimiter`
   * @param delimiter A string which defines the delimiter. The delimiter won't be included in the resulting tokens.
   * @return A standard vector containing the resulting tokens. If `delimiter` does not occur in `string`, then a vector
   * with one element containing the input string is returned.
   */
  std::vector<std::string> splitString(const std::string &string, const std::string &delimiter = ",");

  /**
   * Return true if the process with given pid is running.
   * @param pid operating systems process id
   * @return true if the process is running, false otherwise
   */
  bool processRunning(pid_t pid);

  template<typename T, typename _values>
  std::unordered_set<T> keys(const std::unordered_map<T, _values> &map) {
    std::unordered_set<T> keys;
    keys.reserve(map.size());
    for (const auto &entry : map) keys.insert(entry.first);
    return keys;
  }

  template<typename T>
  void joinVector(std::vector<T *> &targetVector, const std::vector<T *> &vectorToJoin) {
    targetVector.reserve(targetVector.size() + vectorToJoin.size());
    for (T *element : vectorToJoin) {
      targetVector.push_back(element);
    }
  }

  void stringReplace(std::string &target, const std::string &toReplace, const std::string &replaceWith);
} // namespace util
